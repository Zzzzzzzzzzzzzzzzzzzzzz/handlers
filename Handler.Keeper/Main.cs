﻿using AOSharp.Core;
using System;
using AOSharp.Core.UI;
using AOSharp.Core.Combat;

namespace Handler.Keeper
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Keeper Handler Loaded! Version: 0.9.9.96");
                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new KeeperHandler(pluginDir));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
