﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using System;
using AOSharp.Core.IPC;
using System.Collections.Generic;
using Handler.Generic;
using static Handler.Generic.Extensions;

namespace Handler.Trader
{
    public class TraderHandler : GenericHandler
    {
        private static string PluginDirectory;

        private static double _mainUpdate;

        private static SimpleChar _mobRansack;
        private static SimpleChar _mobDeprive;
        private static SimpleChar _mobDamage;
        private static SimpleChar _mobAAO;
        private static SimpleChar _mobAAD;
        private static SimpleChar _mobAC;

        private static bool _purpleReady = false;

        public TraderHandler(string pluginDir) : base(pluginDir)
        {
            IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Perks, OnPerksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Buffing, OnBuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Composites, OnCompositesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Debuffing, OnDebuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sickness, OnSicknessMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Invites, OnInvitesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Level, OnLevelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Trading, OnTradingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Channel, OnChannelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.DisbandAll, OnDisbandAllMessage);

            Game.TeleportEnded += OnZoned;

            _settings.AddVariable("DamageDrain", true);
            _settings.AddVariable("HealthDrain", false);

            _settings.AddVariable("EvadesTeam", false);
            _settings.AddVariable("Umbral", false);

            _settings.AddVariable("ProcType1Selection", (int)ProcType1Selection.None);
            _settings.AddVariable("ProcType2Selection", (int)ProcType2Selection.None);

            _settings.AddVariable("LegShot", false);

            _settings.AddVariable("PerkSelection", (int)PerkSelection.None);

            _settings.AddVariable("HealSelection", (int)HealSelection.None);

            _settings.AddVariable("DepriveSelection", (int)DepriveSelection.Target);
            _settings.AddVariable("RansackSelection", (int)RansackSelection.Target);
            _settings.AddVariable("NanoDrainSelection", (int)NanoDrainSelection.None);
            _settings.AddVariable("ACDrainSelection", (int)ACDrainSelection.None);
            _settings.AddVariable("NanoResistSelection", (int)NanoResistSelection.None);
            _settings.AddVariable("DamageDrainSelection", (int)DamageDrainSelection.None);
            _settings.AddVariable("AADDrainSelection", (int)AADDrainSelection.None);
            _settings.AddVariable("AAODrainSelection", (int)AAODrainSelection.None);
            _settings.AddVariable("GrandTheftHumiditySelection", (int)GrandTheftHumiditySelection.Target);
            _settings.AddVariable("MyEnemySelection", (int)MyEnemySelection.Target);
            _settings.AddVariable("NanoHealSelection", (int)NanoHealSelection.Self);

            _settings.AddVariable("CalmingModeSelection", (int)CalmingModeSelection.None);

            _settings.AddVariable("Root", false);

            RegisterSettingsWindow("Handler", "TraderSettingsView.xml");

            //LE Proc
            RegisterPerkProcessor(PerkHash.LEProcTraderDebtCollection, DebtCollection, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderAccumulatedInterest, AccumulatedInterest, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderExchangeProduct, ExchangeProduct, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderUnforgivenDebts, UnforgivenDebts, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderUnexpectedBonus, UnexpectedBonus, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderRebate, Rebate, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.LEProcTraderUnopenedLetter, UnopenedLetter, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderRigidLiquidation, RigidLiquidation, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderDepleteAssets, DepleteAssets, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderEscrow, Escrow, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderRefinanceLoans, RefinanceLoans, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcTraderPaymentPlan, PaymentPlan, (CombatActionPriority)20);

            //Perks
            RegisterPerkProcessor(PerkHash.Sacrifice, Sacrifice);
            RegisterPerkProcessor(PerkHash.PurpleHeart, PurpleHeart);

            //Heals
            RegisterSpellProcessor(RelevantNanos.Heal, Healing, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.TeamHeal, Healing, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.HealthDrain, HealthDrain, (CombatActionPriority)4);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DrainHeal).OrderByStackingOrder(), LEHeal, (CombatActionPriority)3);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoDrain_LineA).OrderByStackingOrder(), RKNanoDrain, (CombatActionPriority)5);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SLNanopointDrain).OrderByStackingOrder(), SLNanoDrain, (CombatActionPriority)4);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoPointHeals).OrderByStackingOrder(), NanoHeal, (CombatActionPriority)3);

            //Buffs
            RegisterSpellProcessor(RelevantNanos.ImprovedQuantumUncertanity, ImprovedQuantumUncertanity);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DamageBuff_LineC).OrderByStackingOrder(), GenericBuff);


            //Team Buffs
            RegisterSpellProcessor(RelevantNanos.QuantumUncertanity, EvadesTeam);
            RegisterSpellProcessor(RelevantNanos.Umbrals, Umbral);

            //Debuffs
            RegisterSpellProcessor(RelevantNanos.GrandThefts, GrandTheftHumidity, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.MyEnemiesEnemyIsMyFriend, MyEnemy, (CombatActionPriority)8);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderAADDrain).OrderByStackingOrder(), AADDrain, (CombatActionPriority)7);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderAAODrain).OrderByStackingOrder(), AAODrain, (CombatActionPriority)5);
            
            RegisterSpellProcessor(RelevantNanos.DivestDamage, DamageDrain, (CombatActionPriority)6);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderShutdownSkillDebuff).OrderByStackingOrder(), DamageDrain, (CombatActionPriority)12);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoResistanceDebuff_LineA).OrderByStackingOrder(), NanoResistA, (CombatActionPriority)11);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DebuffNanoACHeavy).OrderByStackingOrder(), NanoResistB, (CombatActionPriority)11);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderSkillTransferTargetDebuff_Deprive).OrderByStackingOrder(), DepriveDrain, (CombatActionPriority)9);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderSkillTransferTargetDebuff_Ransack).OrderByStackingOrder(), RansackDrain, (CombatActionPriority)9);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderDebuffACNanos).OrderByStackingOrder(), ACDrain, (CombatActionPriority)10);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderACTransferTargetDebuff_Draw).OrderByStackingOrder(), ACDrain, (CombatActionPriority)10);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderACTransferTargetDebuff_Siphon).OrderByStackingOrder(), ACDrain, (CombatActionPriority)10);

            RegisterSpellProcessor(RelevantNanos.FlowOfTime, Root, (CombatActionPriority)4);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.Mezz).OrderByStackingOrder(), Calm, (CombatActionPriority)1);

            #region Commands

            Chat.RegisterCommand("handlerchannel", ChannelCommand);
            Chat.RegisterCommand("disbandall", DisbandAllCommand);

            #endregion

            PluginDirectory = pluginDir;
        }

        #region Callbacks

        public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || Time.NormalTime < _lastZonedTime + 2f) { return; }

            if (!DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
            SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        }

        private void OnPerksMessage(int sender, IPCMessage msg)
        {
            PerkMessage perkMsg = (PerkMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Perks"] = perkMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnBuffingMessage(int sender, IPCMessage msg)
        {
            BuffingMessage buffMsg = (BuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Buffing"] = buffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnCompositesMessage(int sender, IPCMessage msg)
        {
            CompositesMessage compMsg = (CompositesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Composites"] = compMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnDebuffingMessage(int sender, IPCMessage msg)
        {
            DebuffingMessage debuffMsg = (DebuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Debuffing"] = debuffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnChannelMessage(int sender, IPCMessage msg)
        {
            ChannelMessage channelMsg = (ChannelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(channelMsg.Id);
            Config.Save();

            var window = SettingsController.settingsWindow;
            if (window != null && window.IsValid && window.IsVisible)
            {
                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                Config.Save();
            }

            IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
            Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
            SettingsController.RemainingNCU.Clear();
        }
        private void OnDisbandAllMessage(int sender, IPCMessage msg)
        {
            DisbandAllMessage disbandallMsg = (DisbandAllMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Team.Leave();
        }

        private void OnSicknessMessage(int sender, IPCMessage msg)
        {
            SicknessMessage sicknessMsg = (SicknessMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Sickness"] = sicknessMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnInvitesMessage(int sender, IPCMessage msg)
        {
            InvitesMessage invitesMsg = (InvitesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Invites"] = invitesMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnTradingMessage(int sender, IPCMessage msg)
        {
            TradingMessage tradingMsg = (TradingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Trading"] = tradingMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnLevelMessage(int sender, IPCMessage msg)
        {
            LevelMessage levelMsg = (LevelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].Level = levelMsg.Level;

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid && window.IsVisible) { levelInput.Text = $"{levelMsg.Level}"; }
            else
            {
                _settings[$"Level"] = levelMsg.Switch;
                Level = levelMsg.Level;
                SettingsController.CleanUp();
            }
        }

        #endregion

        #region Handles
        private void HandleGenericRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new CompositesMessage()
            {
                Switch = _settings["Composites"].AsBool()
            });
            IPCChannel.Broadcast(new BuffingMessage()
            {
                Switch = _settings["Buffing"].AsBool()
            });
            IPCChannel.Broadcast(new PerkMessage()
            {
                Switch = _settings["Perks"].AsBool()
            });
            IPCChannel.Broadcast(new DebuffingMessage()
            {
                Switch = _settings["Debuffing"].AsBool()
            });
            IPCChannel.Broadcast(new SicknessMessage()
            {
                Switch = _settings["Sickness"].AsBool()
            });
            IPCChannel.Broadcast(new InvitesMessage()
            {
                Switch = _settings["Invites"].AsBool()
            });
            IPCChannel.Broadcast(new TradingMessage()
            {
                Switch = _settings["Trading"].AsBool()
            });
            IPCChannel.Broadcast(new LevelMessage()
            {
                Switch = _settings["Level"].AsBool(),
                Level = Level
            });
        }
        private void HandleBuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_buffView)) { return; }

                _buffView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderBuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Buffs", XmlViewName = "TraderBuffsView" }, _buffView);

                InitSettings(window);
            }
            else if (_buffWindow == null || (_buffWindow != null && !_buffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_buffWindow, PluginDir, new WindowOptions() { Name = "Buffs", XmlViewName = "TraderBuffsView" }, _buffView, out var container);
                _buffWindow = container;

                InitSettings(container);
            }
        }

        private void HandleDebuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_debuffView)) { return; }

                _debuffView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderDebuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Debuffs", XmlViewName = "TraderDebuffsView" }, _debuffView);

                InitSettings(window);
            }
            else if (_debuffWindow == null || (_debuffWindow != null && !_debuffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_debuffWindow, PluginDir, new WindowOptions() { Name = "Debuffs", XmlViewName = "TraderDebuffsView" }, _debuffView, out var container);
                _debuffWindow = container;

                InitSettings(container);
            }
        }
        private void HandleCalmingViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_calmView)) { return; }

                _calmView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderCalmingView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Calming", XmlViewName = "TraderCalmingView" }, _calmView);

                InitSettings(window);
            }
            else if (_calmWindow == null || (_calmWindow != null && !_calmWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_calmWindow, PluginDir, new WindowOptions() { Name = "Calming", XmlViewName = "TraderCalmingView" }, _calmView, out var container);
                _calmWindow = container;

                InitSettings(container);
            }
        }
        private void HandleHealingViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                _healingView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderHealingView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Healing", XmlViewName = "TraderHealingView" }, _healingView);

                InitSettings(window);
            }
            else if (_healingWindow == null || (_healingWindow != null && !_healingWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_healingWindow, PluginDir, new WindowOptions() { Name = "Healing", XmlViewName = "TraderHealingView" }, _healingView, out var container);
                _healingWindow = container;

                InitSettings(container);
            }
        }

        private void HandlePerkViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_perkView)) { return; }

                _perkView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderPerksView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Perks", XmlViewName = "TraderPerksView" }, _perkView);

                InitSettings(window);
            }
            else if (_perkWindow == null || (_perkWindow != null && !_perkWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_perkWindow, PluginDir, new WindowOptions() { Name = "Perks", XmlViewName = "TraderPerksView" }, _perkView, out var container);
                _perkWindow = container;

                InitSettings(container);
            }
        }
        private void HandleItemViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_itemView)) { return; }

                _itemView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderItemsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Items", XmlViewName = "TraderItemsView" }, _itemView);

                InitSettings(window);
            }
            else if (_itemWindow == null || (_itemWindow != null && !_itemWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_itemWindow, PluginDir, new WindowOptions() { Name = "Items", XmlViewName = "TraderItemsView" }, _itemView, out var container);
                _itemWindow = container;

                InitSettings(container);
            }
        }
        private void HandleGenericViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_genericView)) { return; }

                _genericView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderGenericView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Generic", XmlViewName = "TraderGenericView" }, _genericView);

                InitSettings(window);
            }
            else if (_genericWindow == null || (_genericWindow != null && !_genericWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_genericWindow, PluginDir, new WindowOptions() { Name = "Generic", XmlViewName = "TraderGenericView" }, _genericView, out var container);
                _genericWindow = container;

                InitSettings(container);
            }
        }
        private void HandleProcViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_procView)) { return; }

                _procView = View.CreateFromXml(PluginDirectory + "\\UI\\TraderProcsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Procs", XmlViewName = "TraderProcsView" }, _procView);

                InitSettings(window);
            }
            else if (_procWindow == null || (_procWindow != null && !_procWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_procWindow, PluginDir, new WindowOptions() { Name = "Procs", XmlViewName = "TraderProcsView" }, _procView, out var container);
                _procWindow = container;

                InitSettings(container);
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            if (Time.NormalTime > _mainUpdate + Tick)
            {
                RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

                IPCChannel.Broadcast(ncuMessage);

                SettingsController.RemainingNCU[DynelManager.LocalPlayer.Identity] = DynelManager.LocalPlayer.RemainingNCU;

                base.OnUpdate(deltaTime);
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            var window = SettingsController.FindValidWindow(_windows);

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (window != null && window.IsValid && window.IsVisible)
            {
                if (window.FindView("GenericRelay", out Button genericRelay))
                {
                    genericRelay.Tag = window;
                    genericRelay.Clicked = HandleGenericRelayClick;
                }

                if (levelInput != null && !string.IsNullOrEmpty(levelInput.Text))
                    if (int.TryParse(levelInput.Text, out int levelValue))
                        if (Config.CharSettings[Game.ClientInst].Level != levelValue)
                            Config.CharSettings[Game.ClientInst].Level = levelValue;
                if (fountainOfLifeInput != null && !string.IsNullOrEmpty(fountainOfLifeInput.Text))
                    if (int.TryParse(fountainOfLifeInput.Text, out int fountainOfLifeValue))
                        if (Config.CharSettings[Game.ClientInst].FountainOfLifePercentage != fountainOfLifeValue)
                            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = fountainOfLifeValue;
                if (healInput != null && !string.IsNullOrEmpty(healInput.Text))
                    if (int.TryParse(healInput.Text, out int healValue))
                        if (Config.CharSettings[Game.ClientInst].HealPercentage != healValue)
                            Config.CharSettings[Game.ClientInst].HealPercentage = healValue;
                if (stimTargetInput != null && !string.IsNullOrEmpty(stimTargetInput.Text))
                    if (Config.CharSettings[Game.ClientInst].StimTargetName != stimTargetInput.Text)
                        Config.CharSettings[Game.ClientInst].StimTargetName = stimTargetInput.Text;
                if (stimHealthInput != null && !string.IsNullOrEmpty(stimHealthInput.Text))
                    if (int.TryParse(stimHealthInput.Text, out int stimHealthValue))
                        if (Config.CharSettings[Game.ClientInst].StimHealthPercentage != stimHealthValue)
                            Config.CharSettings[Game.ClientInst].StimHealthPercentage = stimHealthValue;
                if (stimNanoInput != null && !string.IsNullOrEmpty(stimNanoInput.Text))
                    if (int.TryParse(stimNanoInput.Text, out int stimNanoValue))
                        if (Config.CharSettings[Game.ClientInst].StimNanoPercentage != stimNanoValue)
                            Config.CharSettings[Game.ClientInst].StimNanoPercentage = stimNanoValue;
                if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        if (Config.CharSettings[Game.ClientInst].KitHealthPercentage != kitHealthValue)
                            Config.CharSettings[Game.ClientInst].KitHealthPercentage = kitHealthValue;
                if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        if (Config.CharSettings[Game.ClientInst].KitNanoPercentage != kitNanoValue)
                            Config.CharSettings[Game.ClientInst].KitNanoPercentage = kitNanoValue;
                if (areaDebuffRangeInput != null && !string.IsNullOrEmpty(areaDebuffRangeInput.Text))
                    if (float.TryParse(areaDebuffRangeInput.Text, out float areaDebuffRangeValue))
                        if (Config.CharSettings[Game.ClientInst].AreaDebuffRange != areaDebuffRangeValue)
                            Config.CharSettings[Game.ClientInst].AreaDebuffRange = areaDebuffRangeValue;
                if (calmRangeInput != null && !string.IsNullOrEmpty(calmRangeInput.Text))
                    if (float.TryParse(calmRangeInput.Text, out float calmRangeValue))
                        if (Config.CharSettings[Game.ClientInst].CalmRange != calmRangeValue)
                            Config.CharSettings[Game.ClientInst].CalmRange = calmRangeValue;
                if (cleanseInput != null && !string.IsNullOrEmpty(cleanseInput.Text))
                    if (int.TryParse(cleanseInput.Text, out int cleanseValue))
                        if (Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay != cleanseValue)
                            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = cleanseValue;
                if (survivalInput != null && !string.IsNullOrEmpty(survivalInput.Text))
                    if (int.TryParse(survivalInput.Text, out int survivalValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay != survivalValue)
                            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = survivalValue;
                if (sphereInput != null && !string.IsNullOrEmpty(sphereInput.Text))
                    if (int.TryParse(sphereInput.Text, out int sphereValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay != sphereValue)
                            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = sphereValue;
                if (witOfTheAtroxInput != null && !string.IsNullOrEmpty(witOfTheAtroxInput.Text))
                    if (int.TryParse(witOfTheAtroxInput.Text, out int witOfTheAtroxValue))
                        if (Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay != witOfTheAtroxValue)
                            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = witOfTheAtroxValue;
                if (selfHealInput != null && !string.IsNullOrEmpty(selfHealInput.Text))
                    if (int.TryParse(selfHealInput.Text, out int selfHealValue))
                        if (Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage != selfHealValue)
                            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = selfHealValue;
                if (selfNanoInput != null && !string.IsNullOrEmpty(selfNanoInput.Text))
                    if (int.TryParse(selfNanoInput.Text, out int selfNanoValue))
                        if (Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage != selfNanoValue)
                            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = selfNanoValue;
                if (teamHealInput != null && !string.IsNullOrEmpty(teamHealInput.Text))
                    if (int.TryParse(teamHealInput.Text, out int teamHealValue))
                        if (Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage != teamHealValue)
                            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = teamHealValue;
                if (teamNanoInput != null && !string.IsNullOrEmpty(teamNanoInput.Text))
                    if (int.TryParse(teamNanoInput.Text, out int teamNanoValue))
                        if (Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage != teamNanoValue)
                            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = teamNanoValue;
                if (bodyDevInput != null && !string.IsNullOrEmpty(bodyDevInput.Text))
                    if (int.TryParse(bodyDevInput.Text, out int bodyDevValue))
                        if (Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage != bodyDevValue)
                            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = bodyDevValue;
                if (strengthInput != null && !string.IsNullOrEmpty(strengthInput.Text))
                    if (int.TryParse(strengthInput.Text, out int strengthValue))
                        if (Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage != strengthValue)
                            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = strengthValue;
                if (healthDrainInput != null && !string.IsNullOrEmpty(healthDrainInput.Text))
                    if (int.TryParse(healthDrainInput.Text, out int heallthDrainValue))
                        if (Config.CharSettings[Game.ClientInst].HealthDrainPercentage != heallthDrainValue)
                            Config.CharSettings[Game.ClientInst].HealthDrainPercentage = heallthDrainValue;

                SettingsController.CleanUp();
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid && SettingsController.settingsWindow.IsVisible)
            {
                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("ItemsView", out Button itemView))
                {
                    itemView.Tag = SettingsController.settingsWindow;
                    itemView.Clicked = HandleItemViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PerksView", out Button perkView))
                {
                    perkView.Tag = SettingsController.settingsWindow;
                    perkView.Clicked = HandlePerkViewClick;
                }

                if (SettingsController.settingsWindow.FindView("HealingView", out Button healingView))
                {
                    healingView.Tag = SettingsController.settingsWindow;
                    healingView.Clicked = HandleHealingViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffsView", out Button buffView))
                {
                    buffView.Tag = SettingsController.settingsWindow;
                    buffView.Clicked = HandleBuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("CalmingView", out Button calmView))
                {
                    calmView.Tag = SettingsController.settingsWindow;
                    calmView.Clicked = HandleCalmingViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DebuffsView", out Button debuffView))
                {
                    debuffView.Tag = SettingsController.settingsWindow;
                    debuffView.Clicked = HandleDebuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ProcsView", out Button procView))
                {
                    procView.Tag = SettingsController.settingsWindow;
                    procView.Clicked = HandleProcViewClick;
                }

                if (SettingsController.settingsWindow.FindView("GenericView", out Button genericView))
                {
                    genericView.Tag = SettingsController.settingsWindow;
                    genericView.Clicked = HandleGenericViewClick;
                }

                SettingsController.CleanUp();
            }

            #endregion
        }

        #region LE Procs

        private bool AccumulatedInterest(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.AccumulatedInterest != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool DebtCollection(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.DebtCollection != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool ExchangeProduct(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.ExchangeProduct != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool Rebate(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.Rebate != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool UnexpectedBonus(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.UnexpectedBonus != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool UnforgivenDebts(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.UnforgivenDebts != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool DepleteAssets(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.DepleteAssets != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool Escrow(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.Escrow != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool RefinanceLoans(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.RefinanceLoans != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool RigidLiquidation(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.RigidLiquidation != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool UnopenedLetter(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.UnopenedLetter != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool PaymentPlan(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.PaymentPlan != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Perks

        private bool Sacrifice(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkSelection.Sacrifice != (PerkSelection)_settings["PerkSelection"].AsInt32()
                || PerkSelection.None == (PerkSelection)_settings["PerkSelection"].AsInt32()
                || fightingTarget == null) { return false; }

            return Volunteer(perk, fightingTarget, ref actionTarget);
        }

        private bool PurpleHeart(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkSelection.PurpleHeart != (PerkSelection)_settings["PerkSelection"].AsInt32()
                || PerkSelection.None == (PerkSelection)_settings["PerkSelection"].AsInt32()
                || fightingTarget == null) { return false; }

            return Volunteer(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Healing

        private bool LEHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return FindMemberWithHealthBelow(60, spell, ref actionTarget);
        }

        private bool Healing(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (HealPercentage == 0) { return false; }

            if (HealSelection.SingleTeam == (HealSelection)_settings["HealSelection"].AsInt32())
                return FindMemberWithHealthBelow(HealPercentage, spell, ref actionTarget);
            else if (HealSelection.SingleArea == (HealSelection)_settings["HealSelection"].AsInt32())
                return FindPlayerWithHealthBelow(HealPercentage, spell, ref actionTarget);
            else if (HealSelection.Team == (HealSelection)_settings["HealSelection"].AsInt32())
            {
                if (DynelManager.LocalPlayer.IsInTeam())
                {
                    List<SimpleChar> dyingTeamMember = DynelManager.Characters
                        .Where(c => Team.Members
                            .Where(m => m.TeamIndex == Team.Members.FirstOrDefault(n => n.Identity == DynelManager.LocalPlayer.Identity).TeamIndex)
                                .Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                        .Where(c => c.HealthPercent <= 85 && c.HealthPercent >= 50)
                        .ToList();

                    if (dyingTeamMember.Count >= 4) { return false; }
                }

                return FindMemberWithHealthBelow(HealPercentage, spell, ref actionTarget);
            }

            return false;
        }

        private bool HealthDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || HealthDrainPercentage == 0 || !_settings["HealthDrain"].AsBool() || !_settings["Debuffing"].AsBool()) { return false; }

            if (DynelManager.LocalPlayer.HealthPercent <= HealthDrainPercentage 
                && OtherSpellChecks(spell, spell.Nanoline, fightingTarget))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        private bool NanoHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (NanoHealSelection.Combat == (NanoHealSelection)_settings["NanoHealSelection"].AsInt32()
                && InCombatTeam())
                return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            else if (NanoHealSelection.Self == (NanoHealSelection)_settings["NanoHealSelection"].AsInt32())
                return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return false;
        }

        #endregion

        #region Calms

        private bool Calm(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell)) { return false; }

            if (CalmingModeSelection.All == (CalmingModeSelection)_settings["CalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < CalmRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .ThenByDescending(c => c.Name == "Corrupted Hiisi Berserker")
                    .ThenByDescending(c => c.Name == "Corrupted Xan-Cur")
                    .ThenByDescending(c => c.Name == "Corrupted Xan-Kuir")
                    .ThenByDescending(c => c.Name == "Corrupted Xan-Len")
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }
            else if (CalmingModeSelection.Adds == (CalmingModeSelection)_settings["CalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && c.FightingTarget != null
                        && !DynelManager.Characters.Any(m => m.FightingTarget != null && m.FightingTarget.Identity == c.Identity)
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < CalmRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .ThenByDescending(c => c.Name == "Corrupted Hiisi Berserker")
                    .ThenByDescending(c => c.Name == "Corrupted Xan-Cur")
                    .ThenByDescending(c => c.Name == "Corrupted Xan-Kuir")
                    .ThenByDescending(c => c.Name == "Corrupted Xan-Len")
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Buffs

        protected bool ImprovedQuantumUncertanity(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Team Buffs

        protected bool EvadesTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["EvadesTeam"].AsBool()) { return false; }

            return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected bool Umbral(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Umbral"].AsBool()) { return false; }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Debuffs

        private bool Root(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool()
                || !_settings["Root"].AsBool() || !CanCast(spell)) { return false; }

            SimpleChar target = DynelManager.Characters
                    .Where(c => c.IsInLineOfSight
                        && IsMoving(c)
                        && !c.Buffs.Contains(NanoLine.Root)
                        && (c.Name == "Flaming Vengeance"
                            || c.Name == "Hand of the Colonel"
                            || c.Name == "Alien Seeker"))
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

            if (target != null)
            {
                actionTarget.Target = target;
                actionTarget.ShouldSetTarget = true;
                return true;
            }

            return false;
        }

        private bool SLNanoDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.NanoPercent >= 75) { return false; }

            if (NanoDrainSelection.Target == (NanoDrainSelection)_settings["NanoDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (NanoDrainSelection.Boss == (NanoDrainSelection)_settings["NanoDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool RKNanoDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.NanoPercent >= 75) { return false; }

            if (NanoDrainSelection.Target == (NanoDrainSelection)_settings["NanoDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (NanoDrainSelection.Boss == (NanoDrainSelection)_settings["NanoDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool MyEnemy(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (MyEnemySelection.Target == (MyEnemySelection)_settings["MyEnemySelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (MyEnemySelection.Boss == (MyEnemySelection)_settings["MyEnemySelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool GrandTheftHumidity(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (GrandTheftHumiditySelection.Target == (GrandTheftHumiditySelection)_settings["GrandTheftHumiditySelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (GrandTheftHumiditySelection.Boss == (GrandTheftHumiditySelection)_settings["GrandTheftHumiditySelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool RansackDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (RansackSelection.Area == (RansackSelection)_settings["RansackSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Ransack, AreaDebuffRange, ref actionTarget);
            else if (RansackSelection.ReplenishArea == (RansackSelection)_settings["RansackSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Ransack, AreaDebuffRange, ref actionTarget);
            else if (RansackSelection.Target == (RansackSelection)_settings["RansackSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Ransack, fightingTarget, ref actionTarget);
            }
            else if (RansackSelection.ReplenishTarget == (RansackSelection)_settings["RansackSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return ReplenishTargetDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Ransack, fightingTarget, ref actionTarget);
            }
            else if (RansackSelection.Boss == (RansackSelection)_settings["RansackSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool DepriveDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DepriveSelection.Area == (DepriveSelection)_settings["DepriveSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Deprive, AreaDebuffRange, ref actionTarget);
            else if (DepriveSelection.ReplenishArea == (DepriveSelection)_settings["DepriveSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Deprive, AreaDebuffRange, ref actionTarget);
            else if (DepriveSelection.Target == (DepriveSelection)_settings["DepriveSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Deprive, fightingTarget, ref actionTarget);
            }
            else if (DepriveSelection.ReplenishTarget == (DepriveSelection)_settings["DepriveSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return ReplenishTargetDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Ransack, fightingTarget, ref actionTarget);
            }
            else if (DepriveSelection.Boss == (DepriveSelection)_settings["DepriveSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool DamageDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DamageDrainSelection.Area == (DamageDrainSelection)_settings["DamageDrainSelection"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);
            else if (DamageDrainSelection.ReplenishArea == (DamageDrainSelection)_settings["DamageDrainSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.DamageBuffs_LineA, AreaDebuffRange, ref actionTarget);
            else if (DamageDrainSelection.Target == (DamageDrainSelection)_settings["DamageDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (DamageDrainSelection.ReplenishTarget == (DamageDrainSelection)_settings["DamageDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return ReplenishTargetDebuff(spell, NanoLine.DamageBuffs_LineA, fightingTarget, ref actionTarget);
            }
            else if (DamageDrainSelection.Boss == (DamageDrainSelection)_settings["DamageDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool AAODrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (AAODrainSelection.Area == (AAODrainSelection)_settings["AAODrainSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderNanoTheft1, AreaDebuffRange, ref actionTarget);
            else if (AAODrainSelection.ReplenishArea == (AAODrainSelection)_settings["AAODrainSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.AAOBuffs, AreaDebuffRange, ref actionTarget);
            else if (AAODrainSelection.Target == (AAODrainSelection)_settings["AAODrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, NanoLine.TraderNanoTheft1, fightingTarget, ref actionTarget);
            }
            else if (AAODrainSelection.ReplenishTarget == (AAODrainSelection)_settings["AAODrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return ReplenishTargetDebuff(spell, NanoLine.AAOBuffs, fightingTarget, ref actionTarget);
            }
            else if (AAODrainSelection.Boss == (AAODrainSelection)_settings["AAODrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool AADDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (AADDrainSelection.Area == (AADDrainSelection)_settings["AADDrainSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderNanoTheft2, AreaDebuffRange, ref actionTarget);
            else if (AADDrainSelection.ReplenishArea == (AADDrainSelection)_settings["AADDrainSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.AAOBuffs, AreaDebuffRange, ref actionTarget);
            else if (AADDrainSelection.Target == (AADDrainSelection)_settings["AADDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, NanoLine.TraderNanoTheft2, fightingTarget, ref actionTarget);
            }
            else if (AADDrainSelection.ReplenishTarget == (AADDrainSelection)_settings["AADDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return ReplenishTargetDebuff(spell, NanoLine.AADBuffs, fightingTarget, ref actionTarget);
            }
            else if (AADDrainSelection.Boss == (AADDrainSelection)_settings["AADDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool NanoResistA(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (NanoResistSelection.Target == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (NanoResistSelection.Boss == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool NanoResistB(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (NanoResistSelection.Target == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (NanoResistSelection.Boss == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }
        private bool ACDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ACDrainSelection.Area == (ACDrainSelection)_settings["ACDrainSelection"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);
            else if (ACDrainSelection.ReplenishArea == (ACDrainSelection)_settings["ACDrainSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);
            else if (ACDrainSelection.Target == (ACDrainSelection)_settings["ACDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (ACDrainSelection.ReplenishTarget == (ACDrainSelection)_settings["ACDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return ReplenishTargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (ACDrainSelection.Boss == (ACDrainSelection)_settings["ACDrainSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        #endregion

        #region Misc

        public static void DisbandAllCommand(string command, string[] param, ChatWindow chatWindow)
        {
            IPCChannel.Broadcast(new DisbandAllMessage());
        }

        public static void ChannelCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length > 0)
            {
                if (param.Length > 1 && param[0].ToLower() == "all")
                {
                    if (param[1] != null && Convert.ToByte(param[1]) > 0)
                    {
                        IPCChannel.Broadcast(new ChannelMessage()
                        {
                            Id = Convert.ToByte(param[1])
                        });

                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[1]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
                else if (param.Length == 1 && Convert.ToByte(param[0]) > 0)
                {
                    if (param[0] != null && Convert.ToByte(param[0]) > 0)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[0]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
            }
        }

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }

        private static bool IsMoving(SimpleChar target)
        {
            if (Playfield.Identity.Instance == 4021)
            {
                return true;
            }

            return target.IsMoving;
        }

        private static class RelevantNanos
        {
            public const int QuantumUncertanity = 30745;
            public const int ImprovedQuantumUncertanity = 270808;
            public const int UnstoppableKiller = 275846;
            public const int DivestDamage = 273407;
            public const int MyEnemiesEnemyIsMyFriend = 270714;
            public const int FlowOfTime = 30719;
            public static int[] Umbrals = { 235291, 235289, 235287, 235285, 235283, 235281,
                235279, 235277, 235275, 235273, 235271 };
            public static int[] GrandThefts = { 269842, 280050 };
            public static int[] HealthDrain = { 270357, 77195, 76478, 76475, 76487, 76481,
                76484, 76491, 76494, 76499, 76571, 76503, 76651, 76614, 76656,
                76653, 76679, 76681, 76684, 76686, 76691, 76688, 76717, 76715,
                76720, 76722, 76724, 76727, 76729, 76732, 76742 };
            public static int[] Heal = { 273410, 252155, 121496, 121500, 121501, 121499,
                121502, 121495, 121492, 121506, 121494, 121493, 121504, 121498, 121503,
                76653, 76679, 76681, 76684, 76686, 76691, 76688, 76717, 76715,
                121497, 121505 };
            public static int[] TeamHeal = { 118245, 118230, 118232, 118231, 118235, 118233,
                118234, 118238, 118236, 118237, 118241, 118239, 118240, 118243, 118244,
                118242, 43374 };
        }

        public enum PerkSelection
        {
           None, Sacrifice, PurpleHeart
        }
        public enum HealSelection
        {
            None, SingleTeam, SingleArea, Team
        }
        public enum NanoHealSelection
        {
            None, Self, Combat
        }
        public enum NanoDrainSelection
        {
            None, Target, Boss
        }
        public enum AADDrainSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum AAODrainSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum ACDrainSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum NanoResistSelection
        {
            None, Target, Boss
        }
        public enum DamageDrainSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum RansackSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum DepriveSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum GrandTheftHumiditySelection
        {
            None, Target, Boss
        }
        public enum MyEnemySelection
        {
            None, Target, Boss
        }
        public enum CalmingModeSelection
        {
            None, All, Adds
        }

        public enum ProcType1Selection
        {
            DebtCollection, AccumulatedInterest, ExchangeProduct, UnforgivenDebts, UnexpectedBonus, Rebate, None
        }

        public enum ProcType2Selection
        {
            UnopenedLetter, RigidLiquidation, DepleteAssets, Escrow, RefinanceLoans, PaymentPlan, None
        }

        #endregion
    }
}
