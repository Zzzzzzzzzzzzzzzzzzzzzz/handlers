﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Core.IPC;
using AOSharp.Core.Inventory;
using Handler.Generic;
using static Handler.Generic.Extensions;
using System;

namespace Handler.NT
{
    public class NTHandler : GenericHandler
    {
        private static string PluginDirectory;

        private static double _absorbs;

        private static double _mainUpdate;

        public NTHandler(string pluginDir) : base(pluginDir)
        {
            IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Perks, OnPerksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Buffing, OnBuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Composites, OnCompositesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Debuffing, OnDebuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sickness, OnSicknessMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Invites, OnInvitesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Level, OnLevelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Trading, OnTradingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Channel, OnChannelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.DisbandAll, OnDisbandAllMessage);

            Game.TeleportEnded += OnZoned;

            _settings.AddVariable("Illusionist", false);
            _settings.AddVariable("NotumGraft", false);

            _settings.AddVariable("CycleAbsorbs", false);

            _settings.AddVariable("AIDot", false);

            _settings.AddVariable("Pierce", false);
            _settings.AddVariable("FlimFocus", false);

            _settings.AddVariable("NanoHOTTeam", false);
            _settings.AddVariable("CostTeam", false);
            _settings.AddVariable("AbsorbACTeam", false);
            _settings.AddVariable("EvadesTeam", false);
            _settings.AddVariable("NFRangeTeam", false);


            _settings.AddVariable("ProcType1Selection", (int)ProcType1Selection.None);
            _settings.AddVariable("ProcType2Selection", (int)ProcType2Selection.None);

            _settings.AddVariable("BlindSelection", (int)BlindSelection.None);
            _settings.AddVariable("HaloSelection", (int)HaloSelection.None);
            _settings.AddVariable("NanoResistSelection", (int)NanoResistSelection.None);

            _settings.AddVariable("CalmingSelection", (int)CalmingSelection.SL);
            _settings.AddVariable("CalmingModeSelection", (int)CalmingModeSelection.None);

            _settings.AddVariable("AOENukeSelection", (int)AOENukeSelection.None);

            RegisterSettingsWindow("Handler", "NTSettingsView.xml");

            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianThermalReprieve, ThermalReprieve, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianHarvestEnergy, HarvestEnergy, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianLayeredAmnesty, LayeredAmnesty, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianSourceTap, SourceTap, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianCircularLogic, CircularLogic, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianOptimizedLibrary, OptimizedLibrary, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianAcceleratedReality, AcceleratedReality, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianLoopingService, LoopingService, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianPoweredNanoFortress, PoweredNanoFortress, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianIncreaseMomentum, IncreaseMomentum, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcNanoTechnicianUnstableLibrary, UnstableLibrary, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.FlimFocus, FlimFocus, (CombatActionPriority)20);

            //Buffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NullitySphereNano).OrderByStackingOrder(), NullitySphere, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.NanobotAegis, NanobotAegis, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.IzgimmersWealth, IzgimmersWealth, (CombatActionPriority)3);

            RegisterSpellProcessor(RelevantNanos.NanobotShelter, GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.Psy_IntBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoDamageMultiplierBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MatCreaBuff).OrderByStackingOrder(), MatCreaBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MajorEvasionBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.Fortify).OrderByStackingOrder(), GenericBuff);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AbsorbACBuff).OrderByStackingOrder(), CycleAbsorbs);

            //Team Buffs
            RegisterSpellProcessor(RelevantNanos.AbsorbACBuff, AbsorbACTeam);
            RegisterSpellProcessor(RelevantNanos.DarkMovement, EvadesTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NFRangeBuff).OrderByStackingOrder(), NFRangeTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoOverTime_LineA).OrderByStackingOrder(), NanoHOTTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NPCostBuff).OrderByStackingOrder(), CostTeam);

            //Nukes and DoTs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DOTNanotechnicianStrainA).OrderByStackingOrder(), AIDOT, (CombatActionPriority)6);
            RegisterSpellProcessor(RelevantNanos.Garuk, SingleTargetNuke, (CombatActionPriority)7);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DOTNanotechnicianStrainB).OrderByStackingOrder(), PierceNuke, (CombatActionPriority)8);
            RegisterSpellProcessor(RelevantNanos.SingleTargetNukes, SingleTargetNuke, (CombatActionPriority)9);
            RegisterSpellProcessor(RelevantNanos.AOENukes, AOENuke);
            RegisterSpellProcessor(RelevantNanos.VolcanicEruption, VolcanicEruption);

            //Debuffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AAODebuffs).OrderByStackingOrder(), SingleBlind, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.AOEBlinds, AOEBlind, (CombatActionPriority)5);

            RegisterSpellProcessor(RelevantNanos.SLCalms, SLCalm, (CombatActionPriority)4);
            RegisterSpellProcessor(RelevantNanos.RKCalms, RKCalm, (CombatActionPriority)4);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.HaloNanoDebuff).OrderByStackingOrder(), Halo, (CombatActionPriority)20);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoResistanceDebuff_LineA).OrderByStackingOrder(), NanoResist, (CombatActionPriority)20);

            //Items
            RegisterItemProcessor(RelevantItems.NotumItem, NotumItem);
            RegisterItemProcessor(RelevantItems.WilloftheIllusionist, RelevantItems.WilloftheIllusionist, Illusionist);

            #region Commands

            Chat.RegisterCommand("handlerchannel", ChannelCommand);
            Chat.RegisterCommand("disbandall", DisbandAllCommand);

            #endregion

            PluginDirectory = pluginDir;
        }

        #region Callbacks

        public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || Time.NormalTime < _lastZonedTime + 2f) { return; }

            if (!DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
            SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        }

        private void OnPerksMessage(int sender, IPCMessage msg)
        {
            PerkMessage perkMsg = (PerkMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Perks"] = perkMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnBuffingMessage(int sender, IPCMessage msg)
        {
            BuffingMessage buffMsg = (BuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Buffing"] = buffMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnCompositesMessage(int sender, IPCMessage msg)
        {
            CompositesMessage compMsg = (CompositesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Composites"] = compMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnDebuffingMessage(int sender, IPCMessage msg)
        {
            DebuffingMessage debuffMsg = (DebuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Debuffing"] = debuffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnChannelMessage(int sender, IPCMessage msg)
        {
            ChannelMessage channelMsg = (ChannelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(channelMsg.Id);
            Config.Save();

            var window = SettingsController.settingsWindow;
            if (window != null && window.IsValid && window.IsVisible)
            {
                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                Config.Save();
            }

            IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
            Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
            SettingsController.RemainingNCU.Clear();
        }
        private void OnDisbandAllMessage(int sender, IPCMessage msg)
        {
            DisbandAllMessage disbandallMsg = (DisbandAllMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Team.Leave();
        }

        private void OnSicknessMessage(int sender, IPCMessage msg)
        {
            SicknessMessage sicknessMsg = (SicknessMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Sickness"] = sicknessMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnInvitesMessage(int sender, IPCMessage msg)
        {
            InvitesMessage invitesMsg = (InvitesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Invites"] = invitesMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnTradingMessage(int sender, IPCMessage msg)
        {
            TradingMessage tradingMsg = (TradingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Trading"] = tradingMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnLevelMessage(int sender, IPCMessage msg)
        {
            LevelMessage levelMsg = (LevelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].Level = levelMsg.Level;

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid && window.IsVisible) { levelInput.Text = $"{levelMsg.Level}"; }
            else
            {
                _settings[$"Level"] = levelMsg.Switch;
                Level = levelMsg.Level;
                SettingsController.CleanUp();
            }
        }

        #endregion

        #region Handles

        private void HandleGenericRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new CompositesMessage()
            {
                Switch = _settings["Composites"].AsBool()
            });
            IPCChannel.Broadcast(new BuffingMessage()
            {
                Switch = _settings["Buffing"].AsBool()
            });
            IPCChannel.Broadcast(new PerkMessage()
            {
                Switch = _settings["Perks"].AsBool()
            });
            IPCChannel.Broadcast(new DebuffingMessage()
            {
                Switch = _settings["Debuffing"].AsBool()
            });
            IPCChannel.Broadcast(new SicknessMessage()
            {
                Switch = _settings["Sickness"].AsBool()
            });
            IPCChannel.Broadcast(new InvitesMessage()
            {
                Switch = _settings["Invites"].AsBool()
            });
            IPCChannel.Broadcast(new TradingMessage()
            {
                Switch = _settings["Trading"].AsBool()
            });
            IPCChannel.Broadcast(new LevelMessage()
            {
                Switch = _settings["Level"].AsBool(),
                Level = Level
            });
        }
        private void HandleDebuffsViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_debuffView)) { return; }

                _debuffView = View.CreateFromXml(PluginDirectory + "\\UI\\NTDebuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Debuffs", XmlViewName = "NTDebuffsView" }, _debuffView);

                InitSettings(window);
            }
            else if (_debuffWindow == null || (_debuffWindow != null && !_debuffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_debuffWindow, PluginDir, new WindowOptions() { Name = "Debuffs", XmlViewName = "NTDebuffsView" }, _debuffView, out var container);
                _debuffWindow = container;

                InitSettings(container);
            }
        }
        private void HandlePerkViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_perkView)) { return; }

                _perkView = View.CreateFromXml(PluginDirectory + "\\UI\\NTPerksView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Perks", XmlViewName = "NTPerksView" }, _perkView);

                InitSettings(window);
            }
            else if (_perkWindow == null || (_perkWindow != null && !_perkWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_perkWindow, PluginDir, new WindowOptions() { Name = "Perks", XmlViewName = "NTPerksView" }, _perkView, out var container);
                _perkWindow = container;

                InitSettings(container);
            }
        }
        private void HandleCalmingViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_calmView)) { return; }

                _calmView = View.CreateFromXml(PluginDirectory + "\\UI\\NTCalmingView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Calming", XmlViewName = "NTCalmingView" }, _calmView);

                InitSettings(window);
            }
            else if (_calmWindow == null || (_calmWindow != null && !_calmWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_calmWindow, PluginDir, new WindowOptions() { Name = "Calming", XmlViewName = "NTCalmingView" }, _calmView, out var container);
                _calmWindow = container;

                InitSettings(container);
            }
        }

        private void HandleNukesViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_nukeView)) { return; }

                _nukeView = View.CreateFromXml(PluginDirectory + "\\UI\\NTNukesView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Nukes", XmlViewName = "NTNukesView" }, _nukeView);

                InitSettings(window);
            }
            else if (_nukeWindow == null || (_nukeWindow != null && !_nukeWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_nukeWindow, PluginDir, new WindowOptions() { Name = "Nukes", XmlViewName = "NTNukesView" }, _nukeView, out var container);
                _nukeWindow = container;

                InitSettings(container);
            }
        }
        private void HandleBuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_buffView)) { return; }

                _buffView = View.CreateFromXml(PluginDirectory + "\\UI\\NTBuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Buffs", XmlViewName = "NTBuffsView" }, _buffView);

                InitSettings(window);
            }
            else if (_buffWindow == null || (_buffWindow != null && !_buffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_buffWindow, PluginDir, new WindowOptions() { Name = "Buffs", XmlViewName = "NTBuffsView" }, _buffView, out var container);
                _buffWindow = container;

                InitSettings(container);
            }
        }
        private void HandleItemViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_itemView)) { return; }

                _itemView = View.CreateFromXml(PluginDirectory + "\\UI\\NTItemsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Items", XmlViewName = "NTItemsView" }, _itemView);

                InitSettings(window);
            }
            else if (_itemWindow == null || (_itemWindow != null && !_itemWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_itemWindow, PluginDir, new WindowOptions() { Name = "Items", XmlViewName = "NTItemsView" }, _itemView, out var container);
                _itemWindow = container;

                InitSettings(container);
            }
        }

        private void HandleGenericViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_genericView)) { return; }

                _genericView = View.CreateFromXml(PluginDirectory + "\\UI\\NTGenericView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Generic", XmlViewName = "NTGenericView" }, _genericView);

                InitSettings(window);
            }
            else if (_genericWindow == null || (_genericWindow != null && !_genericWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_genericWindow, PluginDir, new WindowOptions() { Name = "Generic", XmlViewName = "NTGenericView" }, _genericView, out var container);
                _genericWindow = container;

                InitSettings(container);
            }
        }

        private void HandleProcViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_procView)) { return; }

                _procView = View.CreateFromXml(PluginDirectory + "\\UI\\NTProcsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Procs", XmlViewName = "NTProcsView" }, _procView);

                InitSettings(window);
            }
            else if (_procWindow == null || (_procWindow != null && !_procWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_procWindow, PluginDir, new WindowOptions() { Name = "Procs", XmlViewName = "NTProcsView" }, _procView, out var container);
                _procWindow = container;

                InitSettings(container);
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            if (Time.NormalTime > _mainUpdate + Tick)
            {
                RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

                IPCChannel.Broadcast(ncuMessage);

                SettingsController.RemainingNCU[DynelManager.LocalPlayer.Identity] = DynelManager.LocalPlayer.RemainingNCU;

                base.OnUpdate(deltaTime);
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            var window = SettingsController.FindValidWindow(_windows);

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (window != null && window.IsValid && window.IsVisible)
            {
                if (window.FindView("GenericRelay", out Button genericRelay))
                {
                    genericRelay.Tag = window;
                    genericRelay.Clicked = HandleGenericRelayClick;
                }

                if (levelInput != null && !string.IsNullOrEmpty(levelInput.Text))
                    if (int.TryParse(levelInput.Text, out int levelValue))
                        if (Config.CharSettings[Game.ClientInst].Level != levelValue)
                            Config.CharSettings[Game.ClientInst].Level = levelValue;
                if (fountainOfLifeInput != null && !string.IsNullOrEmpty(fountainOfLifeInput.Text))
                    if (int.TryParse(fountainOfLifeInput.Text, out int fountainOfLifeValue))
                        if (Config.CharSettings[Game.ClientInst].FountainOfLifePercentage != fountainOfLifeValue)
                            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = fountainOfLifeValue;
                if (stimTargetInput != null && !string.IsNullOrEmpty(stimTargetInput.Text))
                    if (Config.CharSettings[Game.ClientInst].StimTargetName != stimTargetInput.Text)
                        Config.CharSettings[Game.ClientInst].StimTargetName = stimTargetInput.Text;
                if (stimHealthInput != null && !string.IsNullOrEmpty(stimHealthInput.Text))
                    if (int.TryParse(stimHealthInput.Text, out int stimHealthValue))
                        if (Config.CharSettings[Game.ClientInst].StimHealthPercentage != stimHealthValue)
                            Config.CharSettings[Game.ClientInst].StimHealthPercentage = stimHealthValue;
                if (stimNanoInput != null && !string.IsNullOrEmpty(stimNanoInput.Text))
                    if (int.TryParse(stimNanoInput.Text, out int stimNanoValue))
                        if (Config.CharSettings[Game.ClientInst].StimNanoPercentage != stimNanoValue)
                            Config.CharSettings[Game.ClientInst].StimNanoPercentage = stimNanoValue;
                if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        if (Config.CharSettings[Game.ClientInst].KitHealthPercentage != kitHealthValue)
                            Config.CharSettings[Game.ClientInst].KitHealthPercentage = kitHealthValue;
                if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        if (Config.CharSettings[Game.ClientInst].KitNanoPercentage != kitNanoValue)
                            Config.CharSettings[Game.ClientInst].KitNanoPercentage = kitNanoValue;
                if (areaDebuffRangeInput != null && !string.IsNullOrEmpty(areaDebuffRangeInput.Text))
                    if (float.TryParse(areaDebuffRangeInput.Text, out float areaDebuffRangeValue))
                        if (Config.CharSettings[Game.ClientInst].AreaDebuffRange != areaDebuffRangeValue)
                            Config.CharSettings[Game.ClientInst].AreaDebuffRange = areaDebuffRangeValue;
                if (calmRangeInput != null && !string.IsNullOrEmpty(calmRangeInput.Text))
                    if (float.TryParse(calmRangeInput.Text, out float calmRangeValue))
                        if (Config.CharSettings[Game.ClientInst].CalmRange != calmRangeValue)
                            Config.CharSettings[Game.ClientInst].CalmRange = calmRangeValue;
                if (absorbsInput != null && !string.IsNullOrEmpty(absorbsInput.Text))
                    if (int.TryParse(absorbsInput.Text, out int absorbsValue))
                        if (Config.CharSettings[Game.ClientInst].CycleAbsorbsDelay != absorbsValue)
                            Config.CharSettings[Game.ClientInst].CycleAbsorbsDelay = absorbsValue;
                if (cleanseInput != null && !string.IsNullOrEmpty(cleanseInput.Text))
                    if (int.TryParse(cleanseInput.Text, out int cleanseValue))
                        if (Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay != cleanseValue)
                            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = cleanseValue;
                if (survivalInput != null && !string.IsNullOrEmpty(survivalInput.Text))
                    if (int.TryParse(survivalInput.Text, out int survivalValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay != survivalValue)
                            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = survivalValue;
                if (sphereInput != null && !string.IsNullOrEmpty(sphereInput.Text))
                    if (int.TryParse(sphereInput.Text, out int sphereValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay != sphereValue)
                            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = sphereValue;
                if (witOfTheAtroxInput != null && !string.IsNullOrEmpty(witOfTheAtroxInput.Text))
                    if (int.TryParse(witOfTheAtroxInput.Text, out int witOfTheAtroxValue))
                        if (Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay != witOfTheAtroxValue)
                            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = witOfTheAtroxValue;
                if (selfHealInput != null && !string.IsNullOrEmpty(selfHealInput.Text))
                    if (int.TryParse(selfHealInput.Text, out int selfHealValue))
                        if (Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage != selfHealValue)
                            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = selfHealValue;
                if (selfNanoInput != null && !string.IsNullOrEmpty(selfNanoInput.Text))
                    if (int.TryParse(selfNanoInput.Text, out int selfNanoValue))
                        if (Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage != selfNanoValue)
                            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = selfNanoValue;
                if (teamHealInput != null && !string.IsNullOrEmpty(teamHealInput.Text))
                    if (int.TryParse(teamHealInput.Text, out int teamHealValue))
                        if (Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage != teamHealValue)
                            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = teamHealValue;
                if (teamNanoInput != null && !string.IsNullOrEmpty(teamNanoInput.Text))
                    if (int.TryParse(teamNanoInput.Text, out int teamNanoValue))
                        if (Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage != teamNanoValue)
                            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = teamNanoValue;
                if (bodyDevInput != null && !string.IsNullOrEmpty(bodyDevInput.Text))
                    if (int.TryParse(bodyDevInput.Text, out int bodyDevValue))
                        if (Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage != bodyDevValue)
                            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = bodyDevValue;
                if (strengthInput != null && !string.IsNullOrEmpty(strengthInput.Text))
                    if (int.TryParse(strengthInput.Text, out int strengthValue))
                        if (Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage != strengthValue)
                            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = strengthValue;
                if (nanoAegisInput != null && !string.IsNullOrEmpty(nanoAegisInput.Text))
                    if (int.TryParse(nanoAegisInput.Text, out int nanoAegisValue))
                        if (Config.CharSettings[Game.ClientInst].NanoAegisPercentage != nanoAegisValue)
                            Config.CharSettings[Game.ClientInst].NanoAegisPercentage = nanoAegisValue;
                if (nullSphereInput != null && !string.IsNullOrEmpty(nullSphereInput.Text))
                    if (int.TryParse(nullSphereInput.Text, out int nullSphereValue))
                        if (Config.CharSettings[Game.ClientInst].NullitySpherePercentage != nullSphereValue)
                            Config.CharSettings[Game.ClientInst].NullitySpherePercentage = nullSphereValue;
                if (izWealthInput != null && !string.IsNullOrEmpty(izWealthInput.Text))
                    if (int.TryParse(izWealthInput.Text, out int izWealthValue))
                        if (Config.CharSettings[Game.ClientInst].IzgimmersWealthPercentage != izWealthValue)
                            Config.CharSettings[Game.ClientInst].IzgimmersWealthPercentage = izWealthValue;

                SettingsController.CleanUp();
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid && SettingsController.settingsWindow.IsVisible)
            {
                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("ItemsView", out Button itemView))
                {
                    itemView.Tag = SettingsController.settingsWindow;
                    itemView.Clicked = HandleItemViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PerksView", out Button perkView))
                {
                    perkView.Tag = SettingsController.settingsWindow;
                    perkView.Clicked = HandlePerkViewClick;
                }

                if (SettingsController.settingsWindow.FindView("NukesView", out Button nukeView))
                {
                    nukeView.Tag = SettingsController.settingsWindow;
                    nukeView.Clicked = HandleNukesViewClick;
                }

                if (SettingsController.settingsWindow.FindView("CalmingView", out Button calmView))
                {
                    calmView.Tag = SettingsController.settingsWindow;
                    calmView.Clicked = HandleCalmingViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffsView", out Button buffView))
                {
                    buffView.Tag = SettingsController.settingsWindow;
                    buffView.Clicked = HandleBuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DebuffsView", out Button debuffView))
                {
                    debuffView.Tag = SettingsController.settingsWindow;
                    debuffView.Clicked = HandleDebuffsViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ProcsView", out Button procView))
                {
                    procView.Tag = SettingsController.settingsWindow;
                    procView.Clicked = HandleProcViewClick;
                }

                if (SettingsController.settingsWindow.FindView("GenericView", out Button genericView))
                {
                    genericView.Tag = SettingsController.settingsWindow;
                    genericView.Clicked = HandleGenericViewClick;
                }

                SettingsController.CleanUp();
            }

            #endregion
        }

        #region LE Procs

        private bool CircularLogic(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.CircularLogic != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool HarvestEnergy(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.HarvestEnergy != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool LayeredAmnesty(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.LayeredAmnesty != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool SourceTap(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.SourceTap != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool ThermalReprieve(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.ThermalReprieve != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool AcceleratedReality(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.AcceleratedReality != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool IncreaseMomentum(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.IncreaseMomentum != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool UnstableLibrary(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.UnstableLibrary != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool LoopingService(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.LoopingService != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool PoweredNanoFortress(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.PoweredNanoFortress != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool OptimizedLibrary(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.OptimizedLibrary != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Nukes

        private bool VolcanicEruption(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !CanCast(spell) || AOENukeSelection.VE != (AOENukeSelection)_settings["AOENukeSelection"].AsInt32()) { return false; }

            actionTarget.ShouldSetTarget = true;
            actionTarget.Target = fightingTarget;
            return true;
        }

        private bool PierceNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Pierce"].AsBool() || fightingTarget == null || !CanCast(spell)) { return false; }

            actionTarget.ShouldSetTarget = true;
            actionTarget.Target = fightingTarget;
            return true;
        }

        private bool AOENuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !CanCast(spell) || AOENukeSelection.Normal != (AOENukeSelection)_settings["AOENukeSelection"].AsInt32()) { return false; }

            actionTarget.ShouldSetTarget = true;
            actionTarget.Target = fightingTarget;
            return true;
        }

        private bool SingleTargetNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !CanCast(spell) || AOENukeSelection.None != (AOENukeSelection)_settings["AOENukeSelection"].AsInt32()) { return false; }

            //Task.Factory.StartNew(
            //    async () =>
            //    {
            //        DynelManager.LocalPlayer.SetStat(Stat.AggDef, 100);
            //        await Task.Delay(444);
            //        DynelManager.LocalPlayer.SetStat(Stat.AggDef, -100);
            //    });

            //if (DynelManager.LocalPlayer.GetStat(Stat.AggDef) == 100)
            //{
            //    return true;
            //}

            actionTarget.ShouldSetTarget = true;
            actionTarget.Target = fightingTarget;
            return true;
        }

        private bool AIDOT(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["AIDot"].AsBool() || fightingTarget == null || !CanCast(spell)
                || fightingTarget.Health < 80000 || (fightingTarget.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 5)) { return false; }

            actionTarget.ShouldSetTarget = true;
            actionTarget.Target = fightingTarget;
            return true;
        }

        #endregion

        #region Debuffs

        private bool AOEBlind(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (BlindSelection.AOE != (BlindSelection)_settings["BlindSelection"].AsInt32()) { return false; }

            return TargetDebuff(spell, NanoLine.AAODebuffs, fightingTarget, ref actionTarget);
        }

        private bool SingleBlind(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (BlindSelection.Target != (BlindSelection)_settings["BlindSelection"].AsInt32()) { return false; }

            return TargetDebuff(spell, NanoLine.AAODebuffs, fightingTarget, ref actionTarget);
        }

        private bool Halo(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (HaloSelection.Area == (HaloSelection)_settings["HaloSelection"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);
            else if (HaloSelection.Target == (HaloSelection)_settings["HaloSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (HaloSelection.Boss == (HaloSelection)_settings["HaloSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;

        }

        private bool NanoResist(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (NanoResistSelection.Area == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);
            else if (NanoResistSelection.Target == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }
            else if (NanoResistSelection.Boss == (NanoResistSelection)_settings["NanoResistSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget?.MaxHealth < 1000000 || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        #endregion

        #region Perks

        private bool FlimFocus(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["FlimFocus"].AsBool()) { return false; }

            return CyclePerks(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Calms

        private bool SLCalm(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell) || CalmingSelection.SL != (CalmingSelection)_settings["CalmingSelection"].AsInt32()) { return false; }

            if (CalmingModeSelection.All == (CalmingModeSelection)_settings["CalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < CalmRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }
            else if (CalmingModeSelection.Adds == (CalmingModeSelection)_settings["CalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && c.FightingTarget != null
                        && !DynelManager.Characters.Any(m => m.FightingTarget != null && m.FightingTarget.Identity == c.Identity)
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < CalmRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }

            return false;
        }

        private bool RKCalm(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell) || CalmingSelection.RK != (CalmingSelection)_settings["CalmingSelection"].AsInt32()) { return false; }

            if (CalmingModeSelection.All == (CalmingModeSelection)_settings["CalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < CalmRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }
            else if (CalmingModeSelection.Adds == (CalmingModeSelection)_settings["CalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && c.FightingTarget != null
                        && !DynelManager.Characters.Any(m => m.FightingTarget != null && m.FightingTarget.Identity == c.Identity)
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < CalmRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Buffs

        private bool MatCreaBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(RelevantNanos.MPCompositeNano)) { return false; }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool NanobotAegis(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent > NanoAegisPercentage
                || DynelManager.LocalPlayer.NanoPercent < 50
                || !InCombatSelf()) { return false; }

            return CombatBuff(spell, NanoLine.NullitySphereNano, fightingTarget, ref actionTarget);
        }

        private bool NullitySphere(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent > NullitySpherePercentage 
                || DynelManager.LocalPlayer.Buffs.Contains(RelevantNanos.NanobotAegis)
                || !InCombatSelf()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool CycleAbsorbs(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["CycleAbsorbs"].AsBool() && Time.NormalTime > _absorbs + CycleAbsorbsDelay)
            {
                _absorbs = Time.NormalTime;

                if (!_settings["Buffing"].AsBool() || !CanCast(spell) || DynelManager.LocalPlayer.Buffs.Any(Buff => Buff.Id == RelevantNanos.BioCocoon)) { return false; }

                if (InCombatSelf()) 
                {
                    actionTarget.ShouldSetTarget = false;
                    return true;
                }
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool IzgimmersWealth(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.NanoPercent > IzgimmersWealthPercentage || !InCombatSelf()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Team Buffs

        private bool NanoHOTTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["NanoHOTTeam"].AsBool())
                return TeamBuffExclusion(spell, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool CostTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["CostTeam"].AsBool())
                return TeamBuffExclusion(spell, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected bool AbsorbACTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["AbsorbACTeam"].AsBool())
                return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected bool EvadesTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["EvadesTeam"].AsBool())
                return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool NFRangeTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["NFRangeTeam"].AsBool())
                return CombatTeamBuffExclusion(spell, fightingTarget, ref actionTarget);

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Items

        protected bool NotumItem(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["NotumGraft"].AsBool() || DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.MaxNanoEnergy) || DynelManager.LocalPlayer.NanoPercent > 50) { return false; }

            return true;
        }

        private bool Illusionist(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Illusionist"].AsBool() || DynelManager.LocalPlayer.Buffs.Contains(274736) || fightingTarget?.MaxHealth > 1000000) { return false; }

            return true;

        }

        #endregion

        #region Misc

        public static void DisbandAllCommand(string command, string[] param, ChatWindow chatWindow)
        {
            IPCChannel.Broadcast(new DisbandAllMessage());
        }

        public static void ChannelCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length > 0)
            {
                if (param.Length > 1 && param[0].ToLower() == "all")
                {
                    if (param[1] != null && Convert.ToByte(param[1]) > 0)
                    {
                        IPCChannel.Broadcast(new ChannelMessage()
                        {
                            Id = Convert.ToByte(param[1])
                        });

                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[1]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
                else if (param.Length == 1 && Convert.ToByte(param[0]) > 0)
                {
                    if (param[0] != null && Convert.ToByte(param[0]) > 0)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[0]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
            }
        }

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }

        public enum ProcType1Selection
        {
            ThermalReprieve, HarvestEnergy, LayeredAmnesty, SourceTap, CircularLogic, None
        }

        public enum ProcType2Selection
        {
            OptimizedLibrary, AcceleratedReality, LoopingService, PoweredNanoFortress, IncreaseMomentum, UnstableLibrary, None
        }
        public enum BlindSelection
        {
            None, Target, AOE
        }
        public enum HaloSelection
        {
            None, Target, Area, Boss
        }
        public enum NanoResistSelection
        {
            None, Target, Area, Boss
        }
        public enum AOENukeSelection
        {
            None, Normal, VE
        }
        public enum CalmingSelection
        {
            SL, RK
        }

        public enum CalmingModeSelection
        {
            None, All, Adds
        }

        private static class RelevantNanos
        {
            public static readonly int[] MPCompositeNano = { 220343, 220341, 220339, 220337, 220335, 220333, 220331 };
            public const int NanobotAegis = 302074;
            public const int IzgimmersWealth = 275024;
            public const int IzgimmersUltimatum = 218168;
            public const int Garuk = 275692;
            public const int PierceReflect = 266287;
            public const int VolcanicEruption = 28638;
            public const int DarkMovement = 28603;
            public const int BioCocoon = 209802;
            public static readonly int[] AOENukes = { 266293, 28638,
                266297, 28637, 28594, 45922, 45906, 45884, 28635, 266298, 28593, 45925, 45940, 45900,28629,
                45917, 45937, 28599, 45894, 45943, 28633, 28631 };
            public const int SuperiorFleetingImmunity = 273386;
            public static readonly int[] SLCalms = { 259364, 259365, 259362, 259363, 259366, 259367, 259335, 259336 };
            public static readonly int[] RKCalms = { 100443, 100441, 100442, 100440 };
            public static readonly int[] AbsorbACBuff = { 270356, 117676, 117675, 117677, 117678, 117679 };
            public static readonly int[] AOEBlinds = { 83959, 83960, 83961, 83962, 83963, 83964 };
            public static readonly int[] SingleTargetNukes = { 218168, 218164, 218162, 218160, 218158, 218156, 218154, 218152, 218150,
                218148, 218146, 218144, 218142, 218140, 218138, 218136, 269473, 218134, 201935, 202262, 201933, 218132, 28618, 218124, 218130,
                218122, 218120, 218128, 218118, 218126, 45226, 45192, 28619, 45230, 28623, 28604, 28616, 218116, 28597, 45210, 45236, 45197,
                45233, 45247, 45199, 45235, 45234, 218114, 45258, 45217, 28600, 45198, 28613, 45919, 45195, 45225, 45260, 45891, 45254, 45890,
                45213, 218112, 45215, 45915, 218104, 45252, 45214, 45251, 45929, 45220, 45920, 45222, 218102, 28598, 45911, 45237, 45216,
                218110, 45913, 45901, 45212, 45206, 45912, 45883, 45245, 45140, 45904, 45218, 28626, 218108, 45261, 218100, 45909, 45203,
                45228, 45903, 45200, 45939, 28592, 45242, 218098, 218106, 45885, 45926, 45241, 44538, 45908, 45250, 45934, 45138, 45932,
                28632, 45205, 28609, 45209, 45246, 45935, 45921, 45227, 45207, 45942, 45191, 45924, 218096, 28610, 45914, 45208, 45893,
                28621, 45211, 45916, 45933, 218094, 45240, 45259, 45941, 45910, 45253, 28614, 218092, 45221, 45204, 28634, 45196, 45886,
                45201, 45928, 45193, 45323, 45244, 45889, 45895, 28605, 45219, 45223, 45938, 28628, 45232, 45248, 45898, 45202, 45923,
                45229, 45907, 45139, 45887, 45231, 45882, 28627, 45936, 45194, 28639, 45243, 45931, 28630, 45137, 28607, 45257, 45880,
                45256, 45249, 45888, 45255, 45881, 42543, 45927, 45902, 42540, 42541, 45899, 45905, 28611, 45897, 28601, 42542, 28608,
                45918, 42539, 45892, 45930, 45879, 45896, 28612 };
            public static readonly int[] NanobotShelter = { 273388, 263265 };
            public static readonly int CompositeAttribute = 223372;
            public static readonly int CompositeNano = 223380;
        }

        private static class RelevantItems
        {
            public static readonly int[] NotumItem = { 204649, 305513 };
            public const int WilloftheIllusionist = 274717;
        }

        #endregion
    }
}
