﻿using AOSharp.Core;
using System;
using AOSharp.Core.Combat;
using AOSharp.Core.UI;

namespace Handler.Agent
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Agent Handler Loaded! Version: 0.9.9.96");
                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new AgentHandler(pluginDir));
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
