﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Core.IPC;
using Handler.Generic;
using static Handler.Generic.Extensions;
using System;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using System.Collections.Generic;

namespace Handler.Agent
{
    public class AgentHandler : GenericHandler
    {
        private static string PluginDirectory;

        private double _petHealDelay = 0;

        private static SimpleChar _mobRansack;
        private static SimpleChar _mobDeprive;
        private static SimpleChar _mobAAO;
        private static SimpleChar _mobAAD;

        private static double _sitPetUsedTimer;

        private static bool _initSit = false;

        private static double _mainUpdate;

        public AgentHandler(string pluginDir) : base(pluginDir)
        {
            IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Perks, OnPerksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Buffing, OnBuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Composites, OnCompositesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Debuffing, OnDebuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sickness, OnSicknessMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Invites, OnInvitesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Level, OnLevelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Trading, OnTradingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Channel, OnChannelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.DisbandAll, OnDisbandAllMessage);

            Game.TeleportEnded += OnZoned;

            _settings.AddVariable("DOTA", false);
            _settings.AddVariable("Evasion", false);

            _settings.AddVariable("CritTeam", false);

            _settings.AddVariable("FalseProfSelection", (int)FalseProfSelection.None);

            _settings.AddVariable("SyncPets", true);
            _settings.AddVariable("BuffPets", true);
            _settings.AddVariable("WarpPets", false);
            _settings.AddVariable("HealPets", false);

            _settings.AddVariable("AttackPet", false);
            _settings.AddVariable("HealPet", false);

            _settings.AddVariable("PetHealingSelection", (int)PetHealingSelection.None);

            _settings.AddVariable("FalseProfsDoctorInitDebuffSelection", (int)FalseProfsDoctorInitDebuffSelection.None);
            _settings.AddVariable("FalseProfsDoctorHealSelection", (int)FalseProfsDoctorHealSelection.None);

            _settings.AddVariable("FalseProfsTraderCalmingModeSelection", (int)FalseProfsTraderCalmingModeSelection.None);

            _settings.AddVariable("FalseProfsTraderRansackSelection", (int)FalseProfsTraderRansackSelection.None);
            _settings.AddVariable("FalseProfsTraderDepriveSelection", (int)FalseProfsTraderDepriveSelection.None);
            _settings.AddVariable("FalseProfsTraderAAODrainSelection", (int)FalseProfsTraderAAODrainSelection.None);
            _settings.AddVariable("FalseProfsTraderAADDrainSelection", (int)FalseProfsTraderAADDrainSelection.None);

            _settings.AddVariable("ProcType1Selection", (int)ProcType1Selection.None);
            _settings.AddVariable("ProcType2Selection", (int)ProcType2Selection.None);


            _settings.AddVariable("CH", false);

            _settings.AddVariable("ProcSelection", (int)ProcSelection.Damage);

            _settings.AddVariable("Concentration", false);

            RegisterSettingsWindow("Handler", "AgentSettingsView.xml");

            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcAgentGrimReaper, GrimReaper, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentDisableCuffs, DisableCuffs, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentNoEscape, NoEscape, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentIntenseMetabolism, IntenseMetabolism, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentMinorNanobotEnhance, MinorNanobotEnhance, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.LEProcAgentNotumChargedRounds, NotumChargedRounds, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentLaserAim, LaserAim, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentNanoEnhancedTargeting, NanoEnhancedTargeting, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentPlasteelPiercingRounds, PlasteelPiercingRounds, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentCellKiller, CellKiller, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentImprovedFocus, ImprovedFocus, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcAgentBrokenAnkle, BrokenAnkle, (CombatActionPriority)20);

            //Buffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AgilityBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AimedShotBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ConcealmentBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CritBuffs, GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ExecutionerBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.RifleBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AgentProcBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ConcentrationCriticalLine).OrderByStackingOrder(), Concentration, (CombatActionPriority)4);

            RegisterSpellProcessor(RelevantNanos.DetauntProc, DetauntProc);
            RegisterSpellProcessor(RelevantNanos.DamageProc, DamageProc);
            RegisterSpellProcessor(RelevantNanos.ShockProc, ShockProc);

            //Team Buffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DamageBuffs_LineA).OrderByStackingOrder(), GenericTeamBuff);
            RegisterSpellProcessor(RelevantNanos.TeamCritBuffs, CritTeam);

            //Debuffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DOTAgentStrainA).OrderByStackingOrder(), DOTA, (CombatActionPriority)8);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.EvasionDebuffs_Agent), EvasionDecrease, (CombatActionPriority)9);

            //False Profs
            RegisterSpellProcessor(RelevantNanos.FalseProfDoc, FalseProfDoctor, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfAdv, FalseProfAdventurer, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfCrat, FalseProfBeauracrat, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfEnf, FalseProfEnforcer, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfEng, FalseProfEngineer, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfFixer, FalseProfFixer, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfMa, FalseProfMartialArtist, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfMp, FalseProfMetaphysicist, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfNt, FalseProfNanoTechnician, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfSol, FalseProfSoldier, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.FalseProfTrader, FalseProfTrader, (CombatActionPriority)1);

            //Trader
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderAADDrain).OrderByStackingOrder(), AADDrain, (CombatActionPriority)6);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderAAODrain).OrderByStackingOrder(), AAODrain, (CombatActionPriority)7);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderSkillTransferTargetDebuff_Deprive).OrderByStackingOrder(), DepriveDrain, (CombatActionPriority)5);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderSkillTransferTargetDebuff_Ransack).OrderByStackingOrder(), RansackDrain, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.RKCalms, RKCalm, (CombatActionPriority)3);

            //Doctor
            RegisterSpellProcessor(RelevantNanos.Healing, Healing, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.CompleteHealing, CompleteHealing, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.InitDebuffs, InitDebuffs, CombatActionPriority.Medium);
            RegisterSpellProcessor(RelevantNanos.HPBuffs, MaxHealth);

            //Metaphysicist
            RegisterSpellProcessor(RelevantNanos.AttackPets, AttackPetSpawner);
            RegisterSpellProcessor(RelevantNanos.HealPets, HealPetSpawner);

            //Soldier
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ReflectShield).Where(c => c.Name.Contains("Mirror")).OrderByStackingOrder(), MirrorShield, (CombatActionPriority)3);

            #region Commands

            Chat.RegisterCommand("handlerchannel", ChannelCommand);
            Chat.RegisterCommand("disbandall", DisbandAllCommand);

            #endregion

            PluginDirectory = pluginDir;
        }

        #region Callbacks

        public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || Time.NormalTime < _lastZonedTime + 2f) { return; }

            if (!DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
            SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        }
        private void OnPerksMessage(int sender, IPCMessage msg)
        {
            PerkMessage perkMsg = (PerkMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Perks"] = perkMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnBuffingMessage(int sender, IPCMessage msg)
        {
            BuffingMessage buffMsg = (BuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Buffing"] = buffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnCompositesMessage(int sender, IPCMessage msg)
        {
            CompositesMessage compMsg = (CompositesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Composites"] = compMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnDebuffingMessage(int sender, IPCMessage msg)
        {
            DebuffingMessage debuffMsg = (DebuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Debuffing"] = debuffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnChannelMessage(int sender, IPCMessage msg)
        {
            ChannelMessage channelMsg = (ChannelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].IPCChannel = channelMsg.Id;
            Config.Save();

            var window = SettingsController.settingsWindow;
            if (window != null && window.IsValid && window.IsVisible)
            {
                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                Config.Save();
            }

            IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
            Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
            SettingsController.RemainingNCU.Clear();
        }

        private void OnDisbandAllMessage(int sender, IPCMessage msg)
        {
            DisbandAllMessage disbandallMsg = (DisbandAllMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Team.Leave();
        }

        private void OnSicknessMessage(int sender, IPCMessage msg)
        {
            SicknessMessage sicknessMsg = (SicknessMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Sickness"] = sicknessMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnInvitesMessage(int sender, IPCMessage msg)
        {
            InvitesMessage invitesMsg = (InvitesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Invites"] = invitesMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnTradingMessage(int sender, IPCMessage msg)
        {
            TradingMessage tradingMsg = (TradingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Trading"] = tradingMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnLevelMessage(int sender, IPCMessage msg)
        {
            LevelMessage levelMsg = (LevelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].Level = levelMsg.Level;

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid && window.IsVisible) { levelInput.Text = $"{levelMsg.Level}"; }
            else
            {
                _settings[$"Level"] = levelMsg.Switch;
                Level = levelMsg.Level;
                SettingsController.CleanUp();
            }
        }

        #endregion

        #region Handles

        private void HandleGenericRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new CompositesMessage()
            {
                Switch = _settings["Composites"].AsBool()
            });
            IPCChannel.Broadcast(new BuffingMessage()
            {
                Switch = _settings["Buffing"].AsBool()
            });
            IPCChannel.Broadcast(new PerkMessage()
            {
                Switch = _settings["Perks"].AsBool()
            });
            IPCChannel.Broadcast(new DebuffingMessage()
            {
                Switch = _settings["Debuffing"].AsBool()
            });
            IPCChannel.Broadcast(new SicknessMessage()
            {
                Switch = _settings["Sickness"].AsBool()
            });
            IPCChannel.Broadcast(new InvitesMessage()
            {
                Switch = _settings["Invites"].AsBool()
            });
            IPCChannel.Broadcast(new TradingMessage()
            {
                Switch = _settings["Trading"].AsBool()
            });
            IPCChannel.Broadcast(new LevelMessage()
            {
                Switch = _settings["Level"].AsBool(),
                Level = Level
            });
        }
        private void HandleItemViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_itemView)) { return; }

                _itemView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentItemsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Items", XmlViewName = "AgentItemsView" }, _itemView);

                InitSettings(window);
            }
            else if (_itemWindow == null || (_itemWindow != null && !_itemWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_itemWindow, PluginDir, new WindowOptions() { Name = "Items", XmlViewName = "AgentItemsView" }, _itemView, out var container);
                _itemWindow = container;

                InitSettings(container);
            }
        }
        private void HandleGenericViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_genericView)) { return; }

                _genericView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentGenericView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Generic", XmlViewName = "AgentGenericView" }, _genericView);

                InitSettings(window);
            }
            else if (_genericWindow == null || (_genericWindow != null && !_genericWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_genericWindow, PluginDir, new WindowOptions() { Name = "Generic", XmlViewName = "AgentGenericView" }, _genericView, out var container);
                _genericWindow = container;

                InitSettings(container);
            }
        }
        private void HandlePerkViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_perkView)) { return; }

                _perkView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentPerksView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Perks", XmlViewName = "AgentPerksView" }, _perkView);

                InitSettings(window);
            }
            else if (_perkWindow == null || (_perkWindow != null && !_perkWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_perkWindow, PluginDir, new WindowOptions() { Name = "Perks", XmlViewName = "AgentPerksView" }, _perkView, out var container);
                _perkWindow = container;

                InitSettings(container);
            }
        }
        private void HandleProcViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_procView)) { return; }

                _procView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentProcsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Procs", XmlViewName = "AgentProcsView" }, _procView);

                InitSettings(window);
            }
            else if (_procWindow == null || (_procWindow != null && !_procWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_procWindow, PluginDir, new WindowOptions() { Name = "Procs", XmlViewName = "AgentProcsView" }, _procView, out var container);
                _procWindow = container;

                InitSettings(container);
            }
        }
        private void HandlePetViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_petView)) { return; }

                _petView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentPetsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Pets", XmlViewName = "AgentPetsView" }, _petView);

                InitSettings(window);
            }
            else if (_petWindow == null || (_petWindow != null && !_petWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_petWindow, PluginDir, new WindowOptions() { Name = "Pets", XmlViewName = "AgentPetsView" }, _petView, out var container);
                _petWindow = container;

                InitSettings(container);
            }
        }
        private void HandleFalseProfViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_falseProfView)) { return; }

                _falseProfView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentFalseProfsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "False Professions", XmlViewName = "AgentFalseProfsView" }, _falseProfView);

                InitSettings(window);
            }
            else if (_falseProfWindow == null || (_falseProfWindow != null && !_falseProfWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_falseProfWindow, PluginDir, new WindowOptions() { Name = "False Professions", XmlViewName = "AgentFalseProfsView" }, _falseProfView, out var container);
                _falseProfWindow = container;

                InitSettings(container);
            }
        }

        private void HandleBuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_buffView)) { return; }

                _buffView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentBuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Buffs", XmlViewName = "AgentBuffsView" }, _buffView);

                InitSettings(window);
            }
            else if (_buffWindow == null || (_buffWindow != null && !_buffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_buffWindow, PluginDir, new WindowOptions() { Name = "Buffs", XmlViewName = "AgentBuffsView" }, _buffView, out var container);
                _buffWindow = container;

                InitSettings(container);
            }
        }

        private void HandleDebuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_debuffView)) { return; }

                _debuffView = View.CreateFromXml(PluginDirectory + "\\UI\\AgentDebuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Debuffs", XmlViewName = "AgentDebuffsView" }, _debuffView);

                InitSettings(window);
            }
            else if (_debuffWindow == null || (_debuffWindow != null && !_debuffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_debuffWindow, PluginDir, new WindowOptions() { Name = "Debuffs", XmlViewName = "AgentDebuffsView" }, _debuffView, out var container);
                _debuffWindow = container;

                InitSettings(container);
            }
        }

        private void HandleFalseProfsOptionViewClick(object s, ButtonBase button)
        {
            Profession prof = (Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession);

            if (prof == Profession.Agent)
            {
                Chat.WriteLine("Not in false profession.");
                return;
            }

            if (prof == Profession.Doctor || prof == Profession.Trader) 
            {
                if (_falseProfsSettingsWindow == null || (_falseProfsSettingsWindow != null && !_falseProfsSettingsWindow.IsValid))
                {
                    SettingsController.CreateSettingsTab(_falseProfsSettingsWindow, PluginDir, new WindowOptions() { Name = $"{prof} Settings", XmlViewName = $"AgentFalseProfs{prof}SettingsView" }, _falseProfsSettingsView, out var container);
                    _falseProfsSettingsWindow = container;

                    InitSettings(container);
                }
            }
            else
            {
                Chat.WriteLine("No Settings for this profession yet.");
                return;
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            #region Pets

            if (Time.NormalTime > _lastPetSyncTime + 0.9f)
            {
                if (DynelManager.LocalPlayer.Pets.Count() >= 1 && CanLookupPetsAfterZone())
                {
                    if (_settings["HealPets"].AsBool())
                        ListenerPetSit();

                    AssignTargetToHealPet();

                    if (_settings["SyncPets"].AsBool())
                    {
                        foreach (Pet _pet in DynelManager.LocalPlayer.Pets.Where(c => c.Type == PetType.Attack || c.Type == PetType.Support))
                        {
                            if (!DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending
                                && (bool)_pet?.Character.IsAttacking) { _pet?.Follow(); }

                            if (DynelManager.LocalPlayer.IsAttacking)
                            {
                                if (_pet?.Character.IsAttacking == false)
                                    _pet?.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                                else if (_pet?.Character.IsAttacking == true && _pet?.Character.FightingTarget != null
                                    && _pet?.Character.FightingTarget.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                                    _pet?.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                            }
                        }
                    }
                }

                _lastPetSyncTime = Time.NormalTime;
            }

            #endregion

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

                IPCChannel.Broadcast(ncuMessage);

                SettingsController.RemainingNCU[DynelManager.LocalPlayer.Identity] = DynelManager.LocalPlayer.RemainingNCU;

                base.OnUpdate(deltaTime);
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            if (_falseProfsSettingsWindow != null && _falseProfsSettingsWindow.IsValid && _falseProfsSettingsWindow.IsVisible)
            {
                if (healInput != null && !string.IsNullOrEmpty(healInput.Text))
                    if (int.TryParse(healInput.Text, out int healValue))
                        if (Config.CharSettings[Game.ClientInst].HealPercentage != healValue)
                        {
                            Config.CharSettings[Game.ClientInst].HealPercentage = healValue;
                        }
                if (completeHealInput != null && !string.IsNullOrEmpty(completeHealInput.Text))
                    if (int.TryParse(completeHealInput.Text, out int completeHealValue))
                        if (Config.CharSettings[Game.ClientInst].CompleteHealPercentage != completeHealValue)
                        {
                            Config.CharSettings[Game.ClientInst].CompleteHealPercentage = completeHealValue;
                        }
                if (falseProfsDoctorAreaRangeInput != null && !string.IsNullOrEmpty(falseProfsDoctorAreaRangeInput.Text))
                    if (float.TryParse(falseProfsDoctorAreaRangeInput.Text, out float doctorRangeValue))
                        if (Config.CharSettings[Game.ClientInst].FalseProfsDoctorAreaRange != doctorRangeValue)
                        {
                            Config.CharSettings[Game.ClientInst].FalseProfsDoctorAreaRange = doctorRangeValue;
                        }
                if (falseProfsTraderAreaRangeInput != null && !string.IsNullOrEmpty(falseProfsTraderAreaRangeInput.Text))
                    if (float.TryParse(falseProfsTraderAreaRangeInput.Text, out float traderRangeValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].FalseProfsTraderAreaRange != traderRangeValue)
                        {
                            Config.CharSettings[Game.ClientInst].FalseProfsTraderAreaRange = traderRangeValue;
                        }
                    }

                SettingsController.CleanUp();
            }

            var window = SettingsController.FindValidWindow(_windows);

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (window != null && window.IsValid && window.IsVisible)
            {
                if (window.FindView("GenericRelay", out Button genericRelay))
                {
                    genericRelay.Tag = window;
                    genericRelay.Clicked = HandleGenericRelayClick;
                }

                if (window.FindView("FalseProfsSettingsView", out Button falseProfsOptionButton))
                {
                    falseProfsOptionButton.Tag = window;
                    falseProfsOptionButton.Clicked = HandleFalseProfsOptionViewClick;
                }

                if (levelInput != null && !string.IsNullOrEmpty(levelInput.Text))
                    if (int.TryParse(levelInput.Text, out int levelValue))
                        if (Config.CharSettings[Game.ClientInst].Level != levelValue)
                            Config.CharSettings[Game.ClientInst].Level = levelValue;
                if (fountainOfLifeInput != null && !string.IsNullOrEmpty(fountainOfLifeInput.Text))
                    if (int.TryParse(fountainOfLifeInput.Text, out int fountainOfLifeValue))
                        if (Config.CharSettings[Game.ClientInst].FountainOfLifePercentage != fountainOfLifeValue)
                            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = fountainOfLifeValue;
                if (stimTargetInput != null && !string.IsNullOrEmpty(stimTargetInput.Text))
                    if (Config.CharSettings[Game.ClientInst].StimTargetName != stimTargetInput.Text)
                        Config.CharSettings[Game.ClientInst].StimTargetName = stimTargetInput.Text;
                if (stimHealthInput != null && !string.IsNullOrEmpty(stimHealthInput.Text))
                    if (int.TryParse(stimHealthInput.Text, out int stimHealthValue))
                        if (Config.CharSettings[Game.ClientInst].StimHealthPercentage != stimHealthValue)
                            Config.CharSettings[Game.ClientInst].StimHealthPercentage = stimHealthValue;
                if (stimNanoInput != null && !string.IsNullOrEmpty(stimNanoInput.Text))
                    if (int.TryParse(stimNanoInput.Text, out int stimNanoValue))
                        if (Config.CharSettings[Game.ClientInst].StimNanoPercentage != stimNanoValue)
                            Config.CharSettings[Game.ClientInst].StimNanoPercentage = stimNanoValue;
                if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        if (Config.CharSettings[Game.ClientInst].KitHealthPercentage != kitHealthValue)
                            Config.CharSettings[Game.ClientInst].KitHealthPercentage = kitHealthValue;
                if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        if (Config.CharSettings[Game.ClientInst].KitNanoPercentage != kitNanoValue)
                            Config.CharSettings[Game.ClientInst].KitNanoPercentage = kitNanoValue;
                if (cleanseInput != null && !string.IsNullOrEmpty(cleanseInput.Text))
                    if (int.TryParse(cleanseInput.Text, out int cleanseValue))
                        if (Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay != cleanseValue)
                            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = cleanseValue;
                if (survivalInput != null && !string.IsNullOrEmpty(survivalInput.Text))
                    if (int.TryParse(survivalInput.Text, out int survivalValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay != survivalValue)
                            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = survivalValue;
                if (sphereInput != null && !string.IsNullOrEmpty(sphereInput.Text))
                    if (int.TryParse(sphereInput.Text, out int sphereValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay != sphereValue)
                            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = sphereValue;
                if (witOfTheAtroxInput != null && !string.IsNullOrEmpty(witOfTheAtroxInput.Text))
                    if (int.TryParse(witOfTheAtroxInput.Text, out int witOfTheAtroxValue))
                        if (Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay != witOfTheAtroxValue)
                            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = witOfTheAtroxValue;
                if (selfHealInput != null && !string.IsNullOrEmpty(selfHealInput.Text))
                    if (int.TryParse(selfHealInput.Text, out int selfHealValue))
                        if (Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage != selfHealValue)
                            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = selfHealValue;
                if (selfNanoInput != null && !string.IsNullOrEmpty(selfNanoInput.Text))
                    if (int.TryParse(selfNanoInput.Text, out int selfNanoValue))
                        if (Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage != selfNanoValue)
                            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = selfNanoValue;
                if (teamHealInput != null && !string.IsNullOrEmpty(teamHealInput.Text))
                    if (int.TryParse(teamHealInput.Text, out int teamHealValue))
                        if (Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage != teamHealValue)
                            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = teamHealValue;
                if (teamNanoInput != null && !string.IsNullOrEmpty(teamNanoInput.Text))
                    if (int.TryParse(teamNanoInput.Text, out int teamNanoValue))
                        if (Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage != teamNanoValue)
                            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = teamNanoValue;
                if (bodyDevInput != null && !string.IsNullOrEmpty(bodyDevInput.Text))
                    if (int.TryParse(bodyDevInput.Text, out int bodyDevValue))
                        if (Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage != bodyDevValue)
                            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = bodyDevValue;
                if (strengthInput != null && !string.IsNullOrEmpty(strengthInput.Text))
                    if (int.TryParse(strengthInput.Text, out int strengthValue))
                        if (Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage != strengthValue)
                            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = strengthValue;

                SettingsController.CleanUp();
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid && SettingsController.settingsWindow.IsVisible)
            {
                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("ItemsView", out Button itemView))
                {
                    itemView.Tag = SettingsController.settingsWindow;
                    itemView.Clicked = HandleItemViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PerksView", out Button perkView))
                {
                    perkView.Tag = SettingsController.settingsWindow;
                    perkView.Clicked = HandlePerkViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffsView", out Button buffView))
                {
                    buffView.Tag = SettingsController.settingsWindow;
                    buffView.Clicked = HandleBuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DebuffsView", out Button debuffView))
                {
                    debuffView.Tag = SettingsController.settingsWindow;
                    debuffView.Clicked = HandleDebuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PetsView", out Button petView))
                {
                    petView.Tag = SettingsController.settingsWindow;
                    petView.Clicked = HandlePetViewClick;
                }

                if (SettingsController.settingsWindow.FindView("FalseProfsView", out Button falseProfView))
                {
                    falseProfView.Tag = SettingsController.settingsWindow;
                    falseProfView.Clicked = HandleFalseProfViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ProcView", out Button procView))
                {
                    procView.Tag = SettingsController.settingsWindow;
                    procView.Clicked = HandleProcViewClick;
                }

                if (SettingsController.settingsWindow.FindView("GenericView", out Button genericView))
                {
                    genericView.Tag = SettingsController.settingsWindow;
                    genericView.Clicked = HandleGenericViewClick;
                }

                SettingsController.CleanUp();
            }

            #endregion
        }

        #region LE Procs

        private bool DisableCuffs(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.DisableCuffs != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool GrimReaper(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.GrimReaper != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool IntenseMetabolism(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.IntenseMetabolism != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool MinorNanobotEnhance(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.MinorNanobotEnhance != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool NoEscape(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.NoEscape != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool BrokenAnkle(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.BrokenAnkle != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool CellKiller(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.CellKiller != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool ImprovedFocus(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.ImprovedFocus != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool LaserAim(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.LaserAim != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool NanoEnhancedTargeting(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.NanoEnhancedTargeting != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool NotumChargedRounds(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.NotumChargedRounds != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool PlasteelPiercingRounds(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.PlasteelPiercingRounds != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region False Profs

        #region Metaphysicist

        private bool AttackPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["AttackPet"].AsBool()) { return false; }

            if (AttackPetSpawn(spell, fightingTarget, ref actionTarget))
            {
                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }

            return false;
        }

        private bool HealPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["HealPet"].AsBool()) { return false; }

            if (HealPetSpawn(spell, fightingTarget, ref actionTarget))
            {
                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }

            return false;
        }

        #endregion

        #region Soldier

        private bool MirrorShield(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (!DynelManager.LocalPlayer.Buffs.Any(c => RelevantNanos.FalseProfSol.Contains(c.Id)) || !_settings["Buffing"].AsBool()
                || (Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Soldier
                || fightingtarget == null || !CanCast(spell)) { return false; }

            return DynelManager.LocalPlayer.HealthPercent <= 85;
        }

        #endregion

        #region Trader

        #region AAO/AAD Drains

        private bool AAODrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Trader || !_settings["Debuffing"].AsBool()) { return false; }

            if (FalseProfsTraderAAODrainSelection.Target == (FalseProfsTraderAAODrainSelection)_settings["FalseProfsTraderAAODrainSelection"].AsInt32())
                return TargetDebuff(spell, NanoLine.TraderNanoTheft1, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderAAODrainSelection.ReplenishTarget == (FalseProfsTraderAAODrainSelection)_settings["FalseProfsTraderAAODrainSelection"].AsInt32())
                return ReplenishTargetDebuff(spell, NanoLine.AAOBuffs, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderAAODrainSelection.Boss == (FalseProfsTraderAAODrainSelection)_settings["FalseProfsTraderAAODrainSelection"].AsInt32())
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                return TargetDebuff(spell, NanoLine.TraderNanoTheft1, fightingTarget, ref actionTarget);
            }
            else if (FalseProfsTraderAAODrainSelection.Area == (FalseProfsTraderAAODrainSelection)_settings["FalseProfsTraderAAODrainSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderNanoTheft1, FalseProfsTraderAreaRange, ref actionTarget);
            else if (FalseProfsTraderAAODrainSelection.ReplenishArea == (FalseProfsTraderAAODrainSelection)_settings["FalseProfsTraderAAODrainSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.AAOBuffs, FalseProfsTraderAreaRange, ref actionTarget);

            return false;
        }

        private bool AADDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Trader || !_settings["Debuffing"].AsBool()) { return false; }

            if (FalseProfsTraderAADDrainSelection.Target == (FalseProfsTraderAADDrainSelection)_settings["FalseProfsTraderAADDrainSelection"].AsInt32())
                return TargetDebuff(spell, NanoLine.TraderNanoTheft2, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderAADDrainSelection.ReplenishTarget == (FalseProfsTraderAADDrainSelection)_settings["FalseProfsTraderAADDrainSelection"].AsInt32())
                return ReplenishTargetDebuff(spell, NanoLine.AADBuffs, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderAADDrainSelection.Boss == (FalseProfsTraderAADDrainSelection)_settings["FalseProfsTraderAADDrainSelection"].AsInt32())
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                return TargetDebuff(spell, NanoLine.TraderNanoTheft2, fightingTarget, ref actionTarget);
            }
            else if (FalseProfsTraderAADDrainSelection.Area == (FalseProfsTraderAADDrainSelection)_settings["FalseProfsTraderAADDrainSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderNanoTheft2, FalseProfsTraderAreaRange, ref actionTarget);
            else if (FalseProfsTraderAADDrainSelection.ReplenishArea == (FalseProfsTraderAADDrainSelection)_settings["FalseProfsTraderAADDrainSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.AADBuffs, FalseProfsTraderAreaRange, ref actionTarget);

            return false;
        }

        #endregion

        #region Ransack/Deprive

        private bool RansackDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Trader || !_settings["Debuffing"].AsBool()) { return false; }

            if (FalseProfsTraderRansackSelection.Target == (FalseProfsTraderRansackSelection)_settings["FalseProfsTraderRansackSelection"].AsInt32())
                return TargetDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Ransack, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderRansackSelection.ReplenishTarget == (FalseProfsTraderRansackSelection)_settings["FalseProfsTraderRansackSelection"].AsInt32())
                return ReplenishTargetDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Ransack, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderRansackSelection.Boss == (FalseProfsTraderRansackSelection)_settings["FalseProfsTraderRansackSelection"].AsInt32())
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                return TargetDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Ransack, fightingTarget, ref actionTarget);
            }
            else if (FalseProfsTraderRansackSelection.Area == (FalseProfsTraderRansackSelection)_settings["FalseProfsTraderRansackSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Ransack, FalseProfsTraderAreaRange, ref actionTarget);
            else if (FalseProfsTraderRansackSelection.ReplenishArea == (FalseProfsTraderRansackSelection)_settings["FalseProfsTraderRansackSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Ransack, FalseProfsTraderAreaRange, ref actionTarget);

            return false;
        }

        private bool DepriveDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Trader || !_settings["Debuffing"].AsBool()) { return false; }

            if (FalseProfsTraderDepriveSelection.Target == (FalseProfsTraderDepriveSelection)_settings["FalseProfsTraderDepriveSelection"].AsInt32())
                return TargetDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Deprive, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderDepriveSelection.ReplenishTarget == (FalseProfsTraderDepriveSelection)_settings["FalseProfsTraderDepriveSelection"].AsInt32())
                return ReplenishTargetDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Deprive, fightingTarget, ref actionTarget);
            else if (FalseProfsTraderDepriveSelection.Boss == (FalseProfsTraderDepriveSelection)_settings["FalseProfsTraderDepriveSelection"].AsInt32())
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                return TargetDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Deprive, fightingTarget, ref actionTarget);
            }
            else if (FalseProfsTraderDepriveSelection.Area == (FalseProfsTraderDepriveSelection)_settings["FalseProfsTraderDepriveSelection"].AsInt32())
                return AreaDebuff(spell, NanoLine.TraderSkillTransferTargetDebuff_Deprive, FalseProfsTraderAreaRange, ref actionTarget);
            else if (FalseProfsTraderDepriveSelection.ReplenishArea == (FalseProfsTraderDepriveSelection)_settings["FalseProfsTraderDepriveSelection"].AsInt32())
                return ReplenishAreaDebuff(spell, NanoLine.TraderSkillTransferCasterBuff_Deprive, FalseProfsTraderAreaRange, ref actionTarget);

            return false;
        }

        #endregion

        #region Calming

        private bool RKCalm(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Trader || !_settings["Debuffing"].AsBool()
                || !CanCast(spell)) { return false; }

            if (FalseProfsTraderCalmingModeSelection.All == (FalseProfsTraderCalmingModeSelection)_settings["FalseProfsTraderCalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < FalseProfsTraderAreaRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }
            else if (FalseProfsTraderCalmingModeSelection.Adds == (FalseProfsTraderCalmingModeSelection)_settings["FalseProfsTraderCalmingModeSelection"].AsInt32())
            {
                _mobCalm = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.MaxHealth < 1000000
                        && c.FightingTarget != null
                        && !DynelManager.Characters.Any(m => m.FightingTarget != null && m.FightingTarget.Identity == c.Identity)
                        && !c.Buffs.Contains(NanoLine.Mezz) && !c.Buffs.Contains(NanoLine.AOEMezz)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < FalseProfsTraderAreaRange)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

                if (_mobCalm != null)
                {
                    if (_mobCalm.Buffs.Find(spell.Id, out Buff buff) && buff.RemainingTime > 10) { return false; }

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _mobCalm;
                    return true;
                }
            }

            return false;
        }

        #endregion

        #endregion

        #region Doctor

        #region Buffs

        private bool MaxHealth(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Healing

        private bool Healing(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell) || HealPercentage == 0 
                || (Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Doctor) { return false; }

            if (FalseProfsDoctorHealSelection.SingleTeam == (FalseProfsDoctorHealSelection)_settings["FalseProfsDoctorHealSelection"].AsInt32())
                return FindMemberWithHealthBelow(HealPercentage, spell, ref actionTarget);
            else if (FalseProfsDoctorHealSelection.SingleArea == (FalseProfsDoctorHealSelection)_settings["FalseProfsDoctorHealSelection"].AsInt32())
                return FindPlayerWithHealthBelow(HealPercentage, spell, ref actionTarget);

            return false;
        }

        private bool CompleteHealing(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !_settings["CH"].AsBool() || CompleteHealPercentage == 0
                || (Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Doctor) { return false; }

            return FindMemberWithHealthBelow(CompleteHealPercentage, spell, ref actionTarget);
        }

        #endregion

        #region Init Debuffs

        private bool InitDebuffs(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((Profession)DynelManager.LocalPlayer.GetStat(Stat.VisualProfession) != Profession.Doctor) { return false; }

            if (FalseProfsDoctorInitDebuffSelection.Area == (FalseProfsDoctorInitDebuffSelection)_settings["FalseProfsDoctorInitDebuffSelection"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, FalseProfsDoctorAreaRange, ref actionTarget);
            else if (FalseProfsDoctorInitDebuffSelection.Target == (FalseProfsDoctorInitDebuffSelection)_settings["FalseProfsDoctorInitDebuffSelection"].AsInt32())
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name) || fightingTarget == null) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        #endregion


        #endregion

        #endregion

        #region Buffs

        #region False Profs

        private bool FalseProfEnforcer(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Enforcer != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfEngineer(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Engineer != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfNanoTechnician(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.NanoTechnician != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfTrader(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Trader != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfBeauracrat(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Beauracrat != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfMartialArtist(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.MartialArtist != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfFixer(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Fixer != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfAdventurer(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Adventurer != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfDoctor(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Doctor != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool FalseProfSoldier(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Soldier != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }
        private bool FalseProfMetaphysicist(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (FalseProfSelection.Metaphysicist != (FalseProfSelection)_settings["FalseProfSelection"].AsInt32()) { return false; }

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }


        #endregion

        private bool Concentration(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Concentration"].AsBool() || !SufficiantHealth(fightingTarget)) { return false; }

            return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool DamageA(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return GenericTeamBuff(spell, fightingTarget, ref actionTarget);
        }

        private bool CritTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["CritTeam"].AsBool())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool ShockProc(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool DetauntProc(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcSelection.DeTaunt != (ProcSelection)_settings["ProcSelection"].AsInt32())
            {
                CancelBuffs(RelevantNanos.DetauntProcs);
                return false;
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool DamageProc(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcSelection.Damage != (ProcSelection)_settings["ProcSelection"].AsInt32())
            {
                CancelBuffs(RelevantNanos.DOTProcs);
                return false;
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Debuffs

        private bool DOTA(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["DOTA"].AsBool()) { return false; }

            return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool EvasionDecrease(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Evasion"].AsBool()) { return false; }

            return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Misc

        public static void DisbandAllCommand(string command, string[] param, ChatWindow chatWindow)
        {
            IPCChannel.Broadcast(new DisbandAllMessage());
        }

        public static void ChannelCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length > 0)
            {
                if (param.Length > 1 && param[0].ToLower() == "all")
                {
                    if (param[1] != null && Convert.ToByte(param[1]) > 0)
                    {
                        IPCChannel.Broadcast(new ChannelMessage()
                        {
                            Id = Convert.ToByte(param[1])
                        });

                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[1]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
                else if (param.Length == 1 && Convert.ToByte(param[0]) > 0)
                {
                    if (param[0] != null && Convert.ToByte(param[0]) > 0)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[0]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
            }
        }

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }

        private SimpleChar GetTargetToHeal()
        {
            if (PetHealingSelection.Team == (PetHealingSelection)_settings["PetHealingSelection"].AsInt32())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.Health > 0
                        && c.HealthPercent < 85f
                        && DynelManager.LocalPlayer.DistanceFrom(c) < 15f
                        && PetCheck(c))
                    .OrderBy(c => c.HealthPercent)
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    return dyingTeamMember;
                }
            }
            else if (PetHealingSelection.Area == (PetHealingSelection)_settings["PetHealingSelection"].AsInt32())
            {
                SimpleChar dyingPlayer = DynelManager.Characters
                    .Where(c => c.Health > 0
                        && c.HealthPercent < 85f && DynelManager.LocalPlayer.DistanceFrom(c) <= 15f
                        && (c.IsPlayer || (c.IsPet && DynelManager.LocalPlayer.Pets.Select(x => x.Identity).Contains(c.Identity))))
                    .OrderBy(c => c.HealthPercent)
                    .FirstOrDefault();

                if (dyingPlayer != null)
                {
                    return dyingPlayer;
                }
            }

            return null;
        }

        private void AssignTargetToHealPet()
        {
            if (Time.NormalTime > _petHealDelay + 5f)
            {
                _petHealDelay = Time.NormalTime;

                Pet healPet = DynelManager.LocalPlayer.Pets.Where(pet => pet.Type == PetType.Heal).FirstOrDefault();

                if (healPet != null)
                {
                    SimpleChar dyingTarget = GetTargetToHeal();

                    if (healPet.Character.Nano <= 1) { return; }

                    if (healPet.Character.DistanceFrom(DynelManager.LocalPlayer) > 20f)
                    {
                        healPet.Follow();
                        _petHealDelay = Time.NormalTime;
                        return;
                    }

                    if (dyingTarget == null) { return; }

                    healPet.Heal(dyingTarget.Identity);
                }
            }
        }

        private static bool PetNanoLow()
        {
            return (TryGetHealPet(out _pet) && (_pet.Character?.Nano / PetMaxNanoPool(_pet) * 100 <= 55
                    && _pet.Character?.Nano != 10
                    && DynelManager.LocalPlayer.DistanceFrom(_pet.Character) < 10f && _pet.Character?.IsInLineOfSight == true));
        }
        private static bool PetHindered()
        {
            return (TryGetHinderedPet(out _pet) && _pet != null
                    && DynelManager.LocalPlayer.DistanceFrom(_pet.Character) < 10f && _pet.Character?.IsInLineOfSight == true);
        }

        private void ListenerPetSit()
        {
            if (_initSit)
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                {
                    _kit?.Use(_pet?.Character, true);
                    _sitPetUsedTimer = Time.NormalTime;
                    _initSit = false;
                    MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                }
            }
            else if (!InCombat() && Time.NormalTime > _sitPetUsedTimer + 16f && CanUseSitKit(out _kit) && (PetNanoLow() || PetHindered()))
            {
                _initSit = true;
                MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            }


            //bool shouldSitKit = PetNanoLow() || PetHindered();
            //bool canSitKit = CanUseSitKit(out _kit);

            //if (_initSit)
            //{
            //    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //    {
            //        if (canSitKit && shouldSitKit)
            //        {
            //            _kit?.Use(_pet?.Character, true);
            //            _sitPetUsedTimer = Time.NormalTime;
            //        }
            //        else if (InCombat())
            //        {
            //            _initSit = false;
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //        }
            //    }
            //    else { _initSit = false; }
            //}
            //else if (!InCombat() && Time.NormalTime > _sitPetUsedTimer + 16f && canSitKit && shouldSitKit)
            //{
            //    _initSit = true;
            //    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //}
        }

        private static class RelevantNanos
        {
            public static int[] FalseProfDoc = { 117210, 117221, 32033 };
            public static int[] FalseProfEng = { 117213, 117224, 32034 };
            public static int[] FalseProfSol = { 117216, 117227, 32038 };
            public static int[] FalseProfCrat = { 117209, 117220, 32032 };
            public static int[] FalseProfTrader = { 117211, 117222, 32040 };
            public static int[] FalseProfAdv = { 117214, 117225, 32030 };
            public static int[] FalseProfMp = { 117210, 117221, 32033 };
            public static int[] FalseProfFixer = { 117212, 117223, 32039 };
            public static int[] FalseProfEnf = { 117217, 117228, 32041 };
            public static int[] FalseProfMa = { 117215, 117226, 32035 };
            public static int[] FalseProfNt = { 117207, 117218, 32037 };
            public static int[] DetauntProcs = { 226437, 226435, 226433, 226431, 226429, 226427 };
            public static int[] DOTProcs = { 226425, 226423, 226421, 226419, 226417, 226415, 226413, 226410 };
            public static int[] TeamCritBuffs = { 160791, 160789, 160787 };
            public static readonly Spell[] CritBuffs = Spell.GetSpellsForNanoline(NanoLine.CriticalIncreaseBuff).OrderByStackingOrder().Where(c => !TeamCritBuffs.Contains(c.Id)).ToArray();
            public static int CompleteHealing = 28650;
            public static int BlastOfOutFlanking = 226437;
            public const int TiredLimbs = 99578;
            public static int[] HPBuffs = new[] { 95709, 28662, 95720, 95712, 95710, 95711, 28649, 95713, 28660, 95715, 95714, 95718, 95716, 95717, 95719, 42397 };
            public static readonly Spell[] ShockProc = Spell.GetSpellsForNanoline(NanoLine.AgentProcBuff).ToArray();
            public static readonly Spell[] DamageProc = Spell.GetSpellsForNanoline(NanoLine.AgentDamageProc_DamageInflictSegment).ToArray();
            public static readonly Spell[] DetauntProc = Spell.GetSpellsForNanoline(NanoLine.AgentDetauntProc_DetauntSegment).ToArray();
            public static readonly Spell[] InitDebuffs = Spell.GetSpellsForNanoline(NanoLine.InitiativeDebuffs).OrderByStackingOrder().Where(spell => spell.Id != TiredLimbs).ToArray();
            public static int[] Healing = new[] { 223299, 223297, 223295, 223293, 223291, 223289, 223287, 223285, 223281, 43878, 43881, 43886, 43885,
                43887, 43890, 43884, 43808, 43888, 43889, 43883, 43811, 43809, 43810, 28645, 43816, 43817, 43825, 43815,
                43814, 43821, 43820, 28648, 43812, 43824, 43822, 43819, 43818, 43823, 28677, 43813, 43826, 43838, 43835,
                28672, 43836, 28676, 43827, 43834, 28681, 43837, 43833, 43830, 43828, 28654, 43831, 43829, 43832, 28665 };
            public static readonly int[] RKCalms = { 100439, 100434, 100435, 100436, 100437, 100438, 100433 };
            public static readonly int[] HealPets = { 225902, 125746, 125739, 125740, 125741, 125742, 125743, 125744, 125745, 125738 };
            public static readonly int[] AttackPets = { 254859, 225900, 254859, 225900, 225898, 225896, 225894, 43737, 43731, 43732, 43735, 43734, 43733, 43324 };
        }

        public enum FalseProfSelection
        {
            None, Metaphysicist, Soldier, Enforcer, Engineer, Doctor, Fixer, Beauracrat, MartialArtist, NanoTechnician, Trader, Adventurer
        }
        public enum FalseProfsDoctorHealSelection
        {
            None, SingleTeam, SingleArea
        }
        public enum PetHealingSelection
        {
            None, Team, Area
        }

        public enum FalseProfsDoctorInitDebuffSelection
        {
            None, Target, Area
        }
        public enum FalseProfsTraderCalmingModeSelection
        {
            None, All, Adds
        }
        public enum FalseProfsTraderDepriveSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum FalseProfsTraderRansackSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum FalseProfsTraderAAODrainSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum FalseProfsTraderAADDrainSelection
        {
            None, Target, ReplenishTarget, Area, ReplenishArea, Boss
        }
        public enum ProcType1Selection
        {
            GrimReaper, DisableCuffs, NoEscape, IntenseMetabolism, MinorNanobotEnhance, None
        }

        public enum ProcType2Selection
        {
            NotumChargedRounds, LaserAim, NanoEnhancedTargeting, PlasteelPiercingRounds, CellKiller, ImprovedFocus, BrokenAnkle, None
        }
        public enum ProcSelection
        {
            None, Damage, DeTaunt
        }

        #endregion
    }
}
