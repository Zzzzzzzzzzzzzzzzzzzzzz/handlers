﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Core.IPC;
using Handler.Generic;
using static Handler.Generic.Extensions;
using System;

namespace Handler.Soldier
{
    public class Soldierhandler : GenericHandler
    {
        private static string PluginDirectory;

        private static SimpleChar _tauntTarget;

        private static double _singleTaunt;
        private static double _mainUpdate;

        public Soldierhandler(string pluginDir) : base(pluginDir)
        {
            IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Perks, OnPerksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Buffing, OnBuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Composites, OnCompositesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Debuffing, OnDebuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sickness, OnSicknessMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Invites, OnInvitesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Level, OnLevelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Trading, OnTradingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Channel, OnChannelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.DisbandAll, OnDisbandAllMessage);

            Game.TeleportEnded += OnZoned;

            //_settings.AddVariable("DeTaunt", false);

            _settings.AddVariable("ArmorTeam", true);
            _settings.AddVariable("EvadesTeam", false);
            _settings.AddVariable("InitTeam", false);
            _settings.AddVariable("AAOTeam", false);
            _settings.AddVariable("PistolTeam", false);
            _settings.AddVariable("CompHeavyArtTeam", false);
            _settings.AddVariable("RiotControlTeam", false);

            _settings.AddVariable("SingleTauntsSelection", (int)SingleTauntsSelection.None);

            _settings.AddVariable("ReflectSelection", (int)ReflectSelection.Shadowlands);

            _settings.AddVariable("NotumGrenades", false);

            _settings.AddVariable("EncaseInStone", false);
            _settings.AddVariable("LegShot", false);

            _settings.AddVariable("ProcType1Selection", (int)ProcType1Selection.None);
            _settings.AddVariable("ProcType2Selection", (int)ProcType2Selection.None);

            RegisterSettingsWindow("Handler", "SoldierSettingsView.xml");

            //LE Proc
            RegisterPerkProcessor(PerkHash.LEProcSoldierFuriousAmmunition, FuriousAmmunition, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierTargetAcquired, TargetAcquired, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierReconditioned, Reconditioned, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierConcussiveShot, ConcussiveShot, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierEmergencyBandages, EmergencyBandages, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierSuccessfulTargeting, SuccessfulTargeting, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.LEProcSoldierFuseBodyArmor, FuseBodyArmor, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierOnTheDouble, OnTheDouble, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierGrazeJugularVein, GrazeJugularVein, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierGearAssaultAbsorption, GearAssaultAbsorption, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierDeepSixInitiative, DeepSixInitiative, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcSoldierShootArtery, ShootArtery, (CombatActionPriority)20);

            // DeTaunt
            //RegisterSpellProcessor(RelevantNanos.Detaunt, DeTaunt);

            //Spells
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ReflectShield).Where(c => c.Name.Contains("Mirror")).OrderByStackingOrder(), AMS, (CombatActionPriority)1);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ReflectShield).Where(c => !c.Name.Contains("Mirror")).OrderByStackingOrder(), RKReflects);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ShadowlandReflectBase).OrderByStackingOrder(), ShadowlandReflect);
            RegisterSpellProcessor(RelevantNanos.SolDrainHeal, DrainHeal);
            RegisterSpellProcessor(RelevantNanos.TauntBuffs, SingleTargetTaunt, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.Phalanx, Phalanx);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.HPBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SiphonBox683).OrderByStackingOrder(), NotumGrenades);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MajorEvasionBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SoldierFullAutoBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TotalFocus).OrderByStackingOrder(), GenericBuff);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SoldierShotgunBuff).OrderByStackingOrder(), Shotgun);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.HeavyWeaponsBuffs).OrderByStackingOrder(), HeavyWeapon);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ArmorBuff).OrderByStackingOrder(), ArmorTeam);

            RegisterSpellProcessor(RelevantNanos.ArBuffs, AssaultRifle);
            RegisterSpellProcessor(RelevantNanos.CompositeHeavyArtillery, CompHeavyArtTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SoldierDamageBase).OrderByStackingOrder(), GenericBuff);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.CompleteHealingLine).OrderByStackingOrder(), CHBlock);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AAOBuffs).OrderByStackingOrder(), AAOTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PistolBuff).OrderByStackingOrder(), PistolTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.BurstBuff).OrderByStackingOrder(), RiotControlTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.InitiativeBuffs).OrderByStackingOrder(), InitTeam);

            //Team Buffs
            RegisterSpellProcessor(RelevantNanos.Precognition, EvadesTeam);

            // Needs work for 2nd tanking Abmouth and Ayjous
            //if (TauntTools.CanTauntTool())
            //{
            //    Item tauntTool = TauntTools.GetBestTauntTool();
            //    RegisterItemProcessor(tauntTool.LowId, tauntTool.HighId, TauntTool);
            //}

            #region Commands

            Chat.RegisterCommand("handlerchannel", ChannelCommand);
            Chat.RegisterCommand("disbandall", DisbandAllCommand);

            #endregion

            PluginDirectory = pluginDir;
        }

        #region Callbacks

        public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || Time.NormalTime < _lastZonedTime + 2f) { return; }

            if (!DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
            SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        }

        private void OnPerksMessage(int sender, IPCMessage msg)
        {
            PerkMessage perkMsg = (PerkMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Perks"] = perkMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnBuffingMessage(int sender, IPCMessage msg)
        {
            BuffingMessage buffMsg = (BuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Buffing"] = buffMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnCompositesMessage(int sender, IPCMessage msg)
        {
            CompositesMessage compMsg = (CompositesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Composites"] = compMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnDebuffingMessage(int sender, IPCMessage msg)
        {
            DebuffingMessage debuffMsg = (DebuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Debuffing"] = debuffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnChannelMessage(int sender, IPCMessage msg)
        {
            ChannelMessage channelMsg = (ChannelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(channelMsg.Id);
            Config.Save();

            var window = SettingsController.settingsWindow;
            if (window != null && window.IsValid && window.IsVisible)
            {
                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                Config.Save();
            }

            IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
            Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
            SettingsController.RemainingNCU.Clear();
        }
        private void OnDisbandAllMessage(int sender, IPCMessage msg)
        {
            DisbandAllMessage disbandallMsg = (DisbandAllMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Team.Leave();
        }
        private void OnSicknessMessage(int sender, IPCMessage msg)
        {
            SicknessMessage sicknessMsg = (SicknessMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Sickness"] = sicknessMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnInvitesMessage(int sender, IPCMessage msg)
        {
            InvitesMessage invitesMsg = (InvitesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Invites"] = invitesMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnTradingMessage(int sender, IPCMessage msg)
        {
            TradingMessage tradingMsg = (TradingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Trading"] = tradingMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnLevelMessage(int sender, IPCMessage msg)
        {
            LevelMessage levelMsg = (LevelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].Level = levelMsg.Level;

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid && window.IsVisible) { levelInput.Text = $"{levelMsg.Level}"; }
            else
            {
                _settings[$"Level"] = levelMsg.Switch;
                Level = levelMsg.Level;
                SettingsController.CleanUp();
            }
        }

        #endregion

        #region Handles
        private void HandleGenericRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new CompositesMessage()
            {
                Switch = _settings["Composites"].AsBool()
            });
            IPCChannel.Broadcast(new BuffingMessage()
            {
                Switch = _settings["Buffing"].AsBool()
            });
            IPCChannel.Broadcast(new PerkMessage()
            {
                Switch = _settings["Perks"].AsBool()
            });
            IPCChannel.Broadcast(new DebuffingMessage()
            {
                Switch = _settings["Debuffing"].AsBool()
            });
            IPCChannel.Broadcast(new SicknessMessage()
            {
                Switch = _settings["Sickness"].AsBool()
            });
            IPCChannel.Broadcast(new InvitesMessage()
            {
                Switch = _settings["Invites"].AsBool()
            });
            IPCChannel.Broadcast(new TradingMessage()
            {
                Switch = _settings["Trading"].AsBool()
            });
            IPCChannel.Broadcast(new LevelMessage()
            {
                Switch = _settings["Level"].AsBool(),
                Level = Level
            });
        }
        private void HandleBuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_buffView)) { return; }

                _buffView = View.CreateFromXml(PluginDirectory + "\\UI\\SoldierBuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Buffs", XmlViewName = "SoldierBuffsView" }, _buffView);

                InitSettings(window);
            }
            else if (_buffWindow == null || (_buffWindow != null && !_buffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_buffWindow, PluginDir, new WindowOptions() { Name = "Buffs", XmlViewName = "SoldierBuffsView" }, _buffView, out var container);
                _buffWindow = container;

                InitSettings(container);
            }
        }
        private void HandlePerkViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_perkView)) { return; }

                _perkView = View.CreateFromXml(PluginDirectory + "\\UI\\SoldierPerksView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Perks", XmlViewName = "SoldierPerksView" }, _perkView);

                InitSettings(window);
            }
            else if (_perkWindow == null || (_perkWindow != null && !_perkWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_perkWindow, PluginDir, new WindowOptions() { Name = "Perks", XmlViewName = "SoldierPerksView" }, _perkView, out var container);
                _perkWindow = container;

                InitSettings(container);
            }
        }

        private void HandleTauntViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_tauntView)) { return; }

                _tauntView = View.CreateFromXml(PluginDirectory + "\\UI\\SoldierTauntsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Taunts", XmlViewName = "SoldierTauntsView" }, _tauntView);

                InitSettings(window);
            }
            else if (_tauntWindow == null || (_tauntWindow != null && !_tauntWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_tauntWindow, PluginDir, new WindowOptions() { Name = "Taunts", XmlViewName = "SoldierTauntsView" }, _tauntView, out var container);
                _tauntWindow = container;

                InitSettings(container);
            }
        }
        private void HandleItemViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_itemView)) { return; }

                _itemView = View.CreateFromXml(PluginDirectory + "\\UI\\SoldierItemsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Items", XmlViewName = "SoldierItemsView" }, _itemView);

                InitSettings(window);
            }
            else if (_itemWindow == null || (_itemWindow != null && !_itemWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_itemWindow, PluginDir, new WindowOptions() { Name = "Items", XmlViewName = "SoldierItemsView" }, _itemView, out var container);
                _itemWindow = container;

                InitSettings(container);
            }
        }
        private void HandleGenericViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_genericView)) { return; }

                _genericView = View.CreateFromXml(PluginDirectory + "\\UI\\SoldierGenericView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Generic", XmlViewName = "SoldierGenericView" }, _genericView);

                InitSettings(window);
            }
            else if (_genericWindow == null || (_genericWindow != null && !_genericWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_genericWindow, PluginDir, new WindowOptions() { Name = "Generic", XmlViewName = "SoldierGenericView" }, _genericView, out var container);
                _genericWindow = container;

                InitSettings(container);
            }
        }
        private void HandleProcViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_procView)) { return; }

                _procView = View.CreateFromXml(PluginDirectory + "\\UI\\SoldierProcsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Procs", XmlViewName = "SoldierProcsView" }, _procView);

                InitSettings(window);
            }
            else if (_procWindow == null || (_procWindow != null && !_procWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_procWindow, PluginDir, new WindowOptions() { Name = "Procs", XmlViewName = "SoldierProcsView" }, _procView, out var container);
                _procWindow = container;

                InitSettings(container);
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            if (Time.NormalTime > _mainUpdate + Tick)
            {
                RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

                IPCChannel.Broadcast(ncuMessage);

                SettingsController.RemainingNCU[DynelManager.LocalPlayer.Identity] = DynelManager.LocalPlayer.RemainingNCU;

                base.OnUpdate(deltaTime);
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            var window = SettingsController.FindValidWindow(_windows);

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (window != null && window.IsValid && window.IsVisible)
            {
                if (window.FindView("GenericRelay", out Button genericRelay))
                {
                    genericRelay.Tag = window;
                    genericRelay.Clicked = HandleGenericRelayClick;
                }

                if (mirrorShieldInput != null && !string.IsNullOrEmpty(mirrorShieldInput.Text))
                    if (int.TryParse(mirrorShieldInput.Text, out int mirrorShieldValue))
                        if (Config.CharSettings[Game.ClientInst].MirrorShieldPercentage != mirrorShieldValue)
                            Config.CharSettings[Game.ClientInst].MirrorShieldPercentage = mirrorShieldValue;
                if (levelInput != null && !string.IsNullOrEmpty(levelInput.Text))
                    if (int.TryParse(levelInput.Text, out int levelValue))
                        if (Config.CharSettings[Game.ClientInst].Level != levelValue)
                            Config.CharSettings[Game.ClientInst].Level = levelValue;
                if (fountainOfLifeInput != null && !string.IsNullOrEmpty(fountainOfLifeInput.Text))
                    if (int.TryParse(fountainOfLifeInput.Text, out int fountainOfLifeValue))
                        if (Config.CharSettings[Game.ClientInst].FountainOfLifePercentage != fountainOfLifeValue)
                            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = fountainOfLifeValue;
                if (singleInput != null && !string.IsNullOrEmpty(singleInput.Text))
                    if (int.TryParse(singleInput.Text, out int singleValue))
                        if (Config.CharSettings[Game.ClientInst].SingleTauntDelay != singleValue)
                            Config.CharSettings[Game.ClientInst].SingleTauntDelay = singleValue;
                if (bioCocoonInput != null && !string.IsNullOrEmpty(bioCocoonInput.Text))
                    if (int.TryParse(bioCocoonInput.Text, out int bioCocoonValue))
                        if (Config.CharSettings[Game.ClientInst].BioCocoonPercentage != bioCocoonValue)
                            Config.CharSettings[Game.ClientInst].BioCocoonPercentage = bioCocoonValue;
                if (stimTargetInput != null && !string.IsNullOrEmpty(stimTargetInput.Text))
                    if (Config.CharSettings[Game.ClientInst].StimTargetName != stimTargetInput.Text)
                        Config.CharSettings[Game.ClientInst].StimTargetName = stimTargetInput.Text;
                if (stimHealthInput != null && !string.IsNullOrEmpty(stimHealthInput.Text))
                    if (int.TryParse(stimHealthInput.Text, out int stimHealthValue))
                        if (Config.CharSettings[Game.ClientInst].StimHealthPercentage != stimHealthValue)
                            Config.CharSettings[Game.ClientInst].StimHealthPercentage = stimHealthValue;
                if (stimNanoInput != null && !string.IsNullOrEmpty(stimNanoInput.Text))
                    if (int.TryParse(stimNanoInput.Text, out int stimNanoValue))
                        if (Config.CharSettings[Game.ClientInst].StimNanoPercentage != stimNanoValue)
                            Config.CharSettings[Game.ClientInst].StimNanoPercentage = stimNanoValue;
                if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        if (Config.CharSettings[Game.ClientInst].KitHealthPercentage != kitHealthValue)
                            Config.CharSettings[Game.ClientInst].KitHealthPercentage = kitHealthValue;
                if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        if (Config.CharSettings[Game.ClientInst].KitNanoPercentage != kitNanoValue)
                            Config.CharSettings[Game.ClientInst].KitNanoPercentage = kitNanoValue;
                if (cleanseInput != null && !string.IsNullOrEmpty(cleanseInput.Text))
                    if (int.TryParse(cleanseInput.Text, out int cleanseValue))
                        if (Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay != cleanseValue)
                            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = cleanseValue;
                if (survivalInput != null && !string.IsNullOrEmpty(survivalInput.Text))
                    if (int.TryParse(survivalInput.Text, out int survivalValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay != survivalValue)
                            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = survivalValue;
                if (sphereInput != null && !string.IsNullOrEmpty(sphereInput.Text))
                    if (int.TryParse(sphereInput.Text, out int sphereValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay != sphereValue)
                            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = sphereValue;
                if (witOfTheAtroxInput != null && !string.IsNullOrEmpty(witOfTheAtroxInput.Text))
                    if (int.TryParse(witOfTheAtroxInput.Text, out int witOfTheAtroxValue))
                        if (Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay != witOfTheAtroxValue)
                            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = witOfTheAtroxValue;
                if (selfHealInput != null && !string.IsNullOrEmpty(selfHealInput.Text))
                    if (int.TryParse(selfHealInput.Text, out int selfHealValue))
                        if (Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage != selfHealValue)
                            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = selfHealValue;
                if (selfNanoInput != null && !string.IsNullOrEmpty(selfNanoInput.Text))
                    if (int.TryParse(selfNanoInput.Text, out int selfNanoValue))
                        if (Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage != selfNanoValue)
                            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = selfNanoValue;
                if (teamHealInput != null && !string.IsNullOrEmpty(teamHealInput.Text))
                    if (int.TryParse(teamHealInput.Text, out int teamHealValue))
                        if (Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage != teamHealValue)
                            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = teamHealValue;
                if (teamNanoInput != null && !string.IsNullOrEmpty(teamNanoInput.Text))
                    if (int.TryParse(teamNanoInput.Text, out int teamNanoValue))
                        if (Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage != teamNanoValue)
                            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = teamNanoValue;
                if (bodyDevInput != null && !string.IsNullOrEmpty(bodyDevInput.Text))
                    if (int.TryParse(bodyDevInput.Text, out int bodyDevValue))
                        if (Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage != bodyDevValue)
                            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = bodyDevValue;
                if (strengthInput != null && !string.IsNullOrEmpty(strengthInput.Text))
                    if (int.TryParse(strengthInput.Text, out int strengthValue))
                        if (Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage != strengthValue)
                            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = strengthValue;
                if (bioRegrowthPercentageInput != null && !string.IsNullOrEmpty(bioRegrowthPercentageInput.Text))
                    if (int.TryParse(bioRegrowthPercentageInput.Text, out int bioRegrowthPercentageValue))
                        if (Config.CharSettings[Game.ClientInst].BioRegrowthPercentage != bioRegrowthPercentageValue)
                            Config.CharSettings[Game.ClientInst].BioRegrowthPercentage = bioRegrowthPercentageValue;
                if (bioRegrowthDelayInput != null && !string.IsNullOrEmpty(bioRegrowthDelayInput.Text))
                    if (int.TryParse(bioRegrowthDelayInput.Text, out int bioRegrowthDelayValue))
                        if (Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelay != bioRegrowthDelayValue)
                            Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelay = bioRegrowthDelayValue;

                SettingsController.CleanUp();
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid && SettingsController.settingsWindow.IsVisible)
            {
                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("ItemsView", out Button itemView))
                {
                    itemView.Tag = SettingsController.settingsWindow;
                    itemView.Clicked = HandleItemViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PerksView", out Button perkView))
                {
                    perkView.Tag = SettingsController.settingsWindow;
                    perkView.Clicked = HandlePerkViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffsView", out Button buffView))
                {
                    buffView.Tag = SettingsController.settingsWindow;
                    buffView.Clicked = HandleBuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("TauntsView", out Button tauntView))
                {
                    tauntView.Tag = SettingsController.settingsWindow;
                    tauntView.Clicked = HandleTauntViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ProcsView", out Button procView))
                {
                    procView.Tag = SettingsController.settingsWindow;
                    procView.Clicked = HandleProcViewClick;
                }

                if (SettingsController.settingsWindow.FindView("GenericView", out Button genericView))
                {
                    genericView.Tag = SettingsController.settingsWindow;
                    genericView.Clicked = HandleGenericViewClick;
                }

                SettingsController.CleanUp();
            }

            #endregion
        }

        #region LE Procs

        private bool ConcussiveShot(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.ConcussiveShot != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool EmergencyBandages(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.EmergencyBandages != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool FuriousAmmunition(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.FuriousAmmunition != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool Reconditioned(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.Reconditioned != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool SuccessfulTargeting(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.SuccessfulTargeting != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool TargetAcquired(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.TargetAcquired != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool FuseBodyArmor(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.FuseBodyArmor != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool GearAssaultAbsorption(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.GearAssaultAbsorption != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool DeepSixInitiative(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.DeepSixInitiative != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool GrazeJugularVein(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.GrazeJugularVein != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool OnTheDouble(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.OnTheDouble != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool ShootArtery(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.ShootArtery != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Buffs

        private bool HeavyWeapon(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.HeavyWeapons);
        }

        private bool CHBlock(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Playfield.ModelIdentity.Instance == 9070 && DynelManager.NPCs.Any(c => c.Name == "Eumenides"))
            {
                if (fightingTarget != null && fightingTarget.Name == "Eumenides")
                    return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool Shotgun(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Shotgun);
        }

        private bool AssaultRifle(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.AssaultRifle);
        }
        protected bool ShadowlandReflect(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ReflectSelection.Shadowlands == (ReflectSelection)_settings["ReflectSelection"].AsInt32())
                return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return false;
        }

        private bool RKReflects(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ReflectSelection.Self == (ReflectSelection)_settings["ReflectSelection"].AsInt32())
                return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            else if (ReflectSelection.Team == (ReflectSelection)_settings["ReflectSelection"].AsInt32())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool Phalanx(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["ArmorTeam"].AsBool() || DynelManager.LocalPlayer.Buffs.Contains(162357)) { return false; }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool NotumGrenades(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["NotumGrenades"].AsBool()) { return false; }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }
        private bool SingleTargetTaunt(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell)) { return false; }

            if (Time.NormalTime > _singleTaunt + SingleTauntDelay)
            {
                _singleTaunt = Time.NormalTime;

                if (SingleTauntsSelection.Area == (SingleTauntsSelection)_settings["SingleTauntsSelection"].AsInt32())
                {
                    _tauntTarget = DynelManager.NPCs
                        .Where(c => c.Health > 0
                            && c.FightingTarget != null
                            && c.FightingTarget?.Profession != Profession.Soldier
                            && c.FightingTarget?.Profession != Profession.Enforcer
                            && c.FightingTarget?.Profession != Profession.MartialArtist
                            && c.IsInLineOfSight
                            && c.Buffs.Contains(NanoLine.Mezz) == false
                            && c.Buffs.Contains(NanoLine.AOEMezz) == false
                            && !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                            && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                            && c.FightingTarget?.Identity != DynelManager.LocalPlayer.Identity
                            && c.Name != "Alien Heavy Patroller"
                            && AttackingTeam(c))
                        .OrderBy(c => c.MaxHealth)
                        .FirstOrDefault();

                    if (_tauntTarget != null)
                    {
                        actionTarget.ShouldSetTarget = true;
                        actionTarget.Target = _tauntTarget;
                        return true;
                    }
                    else if (fightingTarget != null)
                    {
                        actionTarget.ShouldSetTarget = true;
                        actionTarget.Target = fightingTarget;
                        return true;
                    }
                }
                else if (SingleTauntsSelection.Target == (SingleTauntsSelection)_settings["SingleTauntsSelection"].AsInt32())
                {
                    if (fightingTarget != null)
                    {
                        actionTarget.ShouldSetTarget = true;
                        actionTarget.Target = fightingTarget;
                        return true;
                    }
                }
            }

            return false;
        }

        private bool DrainHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || fightingTarget == null || !CanCast(spell) || DynelManager.LocalPlayer.HealthPercent <= 40) { return false; }

            return false;
        }

        private bool AMS(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || fightingTarget == null || !CanCast(spell) || DynelManager.LocalPlayer.HealthPercent > MirrorShieldPercentage) { return false; }

            return true;
        }

        #endregion

        #region Team Buffs

        private bool AAOTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !_settings["AAOTeam"].AsBool() || !CanCast(spell)) { return false; }

            SimpleChar target = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0
                    && !c.Buffs.Contains(NanoLine.AAOBuffs) && !c.Buffs.Contains(NanoLine.AdventurerMorphBuff)
                    && OtherSpellChecks(spell, spell.Nanoline, c))
                .FirstOrDefault();

            if (target != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = target;
                return true;
            }

            return false;
        }

        protected bool PistolTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["PistolTeam"].AsBool())
                return TeamBuffExclusionWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);

            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
        }

        private bool RiotControlTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["RiotControlTeam"].AsBool() || !_settings["Buffing"].AsBool() || !CanCast(spell)) { return false; }

            SimpleChar target = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0
                    && c.SpecialAttacks.Contains(SpecialAttack.Burst)
                    && OtherSpellChecks(spell, spell.Nanoline, c))
                .FirstOrDefault();

            if (target != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = target;
                return true;
            }

            return false;
        }


        protected bool InitTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["InitTeam"].AsBool())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }
        protected bool ArmorTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["ArmorTeam"].AsBool())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool CompHeavyArtTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["CompHeavyArtTeam"].AsBool() || !_settings["Buffing"].AsBool() || !CanCast(spell)) { return false; }

            SimpleChar target = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0
                    && !c.Buffs.Contains(NanoLine.FixerSuppressorBuff)
                    && !c.Buffs.Contains(275027)
                    && OtherSpellChecks(spell, spell.Nanoline, c))
                .FirstOrDefault();

            if (target != null)
            {
                CharacterWieldedWeapon _targetWep = (CharacterWieldedWeapon)target.GetStat(Stat.EquippedWeapons);

                if (!_targetWep.HasFlag(CharacterWieldedWeapon.AssaultRifle)
                    || !_targetWep.HasFlag(CharacterWieldedWeapon.Smg)
                    || !_targetWep.HasFlag(CharacterWieldedWeapon.Shotgun)
                    || (_targetWep.HasFlag(CharacterWieldedWeapon.Grenade)
                        && target.Profession != Profession.Engineer)
                    || (target.Identity == DynelManager.LocalPlayer.Identity
                        && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.AssaultRifle))) { return false; }

                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = target;
                return true;
            }

            return false;
        }

        protected bool EvadesTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["EvadesTeam"].AsBool())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        #endregion

        #region Misc
        public static void DisbandAllCommand(string command, string[] param, ChatWindow chatWindow)
        {
            IPCChannel.Broadcast(new DisbandAllMessage());
        }

        public static void ChannelCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length > 0)
            {
                if (param.Length > 1 && param[0].ToLower() == "all")
                {
                    if (param[1] != null && Convert.ToByte(param[1]) > 0)
                    {
                        IPCChannel.Broadcast(new ChannelMessage()
                        {
                            Id = Convert.ToByte(param[1])
                        });

                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[1]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
                else if (param.Length == 1 && Convert.ToByte(param[0]) > 0)
                {
                    if (param[0] != null && Convert.ToByte(param[0]) > 0)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[0]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
            }
        }

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }
        public enum ReflectSelection
        {
            None, Self, Team, Shadowlands
        }

        public enum SingleTauntsSelection
        {
            None, Target, Area
        }

        public enum ProcType1Selection
        {
            FuriousAmmunition, TargetAcquired, Reconditioned, ConcussiveShot, EmergencyBandages, SuccessfulTargeting, None
        }

        public enum ProcType2Selection
        {
            FuseBodyArmor, OnTheDouble, GrazeJugularVein, GearAssaultAbsorption, DeepSixInitiative, ShootArtery, None
        }

        private static class RelevantNanos
        {
            public static readonly int[] SolDrainHeal = { 29241, 301897 };
            public static readonly int[] TauntBuffs = { 223209, 223207, 223205, 223203, 223201, 29242, 100207,
            29218, 100205, 100206, 100208, 29228};
            public static readonly int[] DeTaunt = { 223221, 223219, 223217, 223215, 223213, 223211 };
            public const int CompositeHeavyArtillery = 269482;
            public const int Phalanx = 29245;
            public const int Precognition = 29247;
            public static readonly int[] ArBuffs = { 275027, 203119, 29220, 203121 };
        }

        private static class RelevantItems
        {
            public const int DreadlochEnduranceBooster = 267168;
            public const int DreadlochEnduranceBoosterNanomageEdition = 267167;
        }

        #endregion
    }
}
