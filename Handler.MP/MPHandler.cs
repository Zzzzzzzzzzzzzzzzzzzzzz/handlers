﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Core.IPC;
using System.Collections.Generic;
using Handler.Generic;
using static Handler.Generic.Extensions;
using System;
using AOSharp.Core.Inventory;
using System.Threading.Tasks;
using AOSharp.Core.Movement;
using AOSharp.Common.Unmanaged.Imports;
using Extensions = Handler.Generic.Extensions;

namespace Handler.MP
{
    public class MPHandler : GenericHandler
    {
        private static string PluginDirectory;

        private double _petHealDelay = 0;
        private double _petMezzDelay = 0;

        private static Spell _warmUpNukeSpellDummy;
        private static Spell _singleNukeSpellDummy;

        private static double _sitPetUsedTimer;

        private static bool _initSit = false;

        private static double _mainUpdate;

        public MPHandler(string pluginDir) : base(pluginDir)
        {
            IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Perks, OnPerksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Buffing, OnBuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Composites, OnCompositesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Debuffing, OnDebuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sickness, OnSicknessMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Invites, OnInvitesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Level, OnLevelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Trading, OnTradingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Channel, OnChannelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.DisbandAll, OnDisbandAllMessage);

            Game.TeleportEnded += OnZoned;

            _warmUpNukeSpellDummy = Spell.List.FirstOrDefault(c => RelevantNanos.WarmUpfNukes.Contains(c.Id));
            _singleNukeSpellDummy = Spell.List.FirstOrDefault(c => RelevantNanos.SingleTargetNukes.Contains(c.Id));

            _settings.AddVariable("SyncPets", true);
            _settings.AddVariable("BuffPets", true);
            _settings.AddVariable("WarpPets", false);
            _settings.AddVariable("HealPets", true);

            _settings.AddVariable("AttackPet", false);
            _settings.AddVariable("HealPet", false);
            _settings.AddVariable("MezzPet", false);

            _settings.AddVariable("PetProcSelection", (int)PetProcSelection.None);
            _settings.AddVariable("CompositeNanoSkillsSelection", (int)CompositeNanoSkillsSelection.None);
            _settings.AddVariable("CostSelection", (int)CostSelection.Self);
            _settings.AddVariable("InterruptSelection", (int)InterruptSelection.None);
            _settings.AddVariable("DamageDebuffSelection", (int)DamageDebuffSelection.None);

            _settings.AddVariable("PetHealingSelection", (int)PetHealingSelection.Team);

            _settings.AddVariable("EvadesTeam", false);
            _settings.AddVariable("PistolTeam", false);

            _settings.AddVariable("NanoResistanceDebuff", (int)NanoResistanceDebuffSelection.None);
            _settings.AddVariable("NanoShutdownDebuff", (int)NanoShutdownDebuffSelection.None);

            //LE Proc
            _settings.AddVariable("ProcType1Selection", (int)ProcType1Selection.None);
            _settings.AddVariable("ProcType2Selection", (int)ProcType2Selection.None);

            _settings.AddVariable("ReplenishNanoSkills", false);

            _settings.AddVariable("Nukes", false);

            //settings.AddVariable("NanoBuffsSelection", (int)NanoBuffsSelection.SL);
            //settings.AddVariable("SummonedWeaponSelection", (int)SummonedWeaponSelection.DISABLED);

            RegisterSettingsWindow("Handler", "MPSettingsView.xml");

            //LE Proc
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistNanobotContingentArrest, NanobotContingentArrest, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistAnticipatedEvasion, AnticipatedEvasion, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistThoughtfulMeans, ThoughtfulMeans, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistRegainFocus, RegainFocus, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistEconomicNanobotUse, EconomicNanobotUse, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistSuperEgoStrike, SuperEgoStrike, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistSuppressFury, SuppressFury, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistEgoStrike, EgoStrike, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistMindWail, MindWail, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistSowDoubt, SowDoubt, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistSowDespair, SowDespair, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcMetaPhysicistDiffuseRage, DiffuseRage, (CombatActionPriority)20);

            //Self buffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MajorEvasionBuffs).OrderByStackingOrder(), Evades);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MartialArtistBowBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.Psy_IntBuff).OrderByStackingOrder(), GenericBuff);

            //Team buffs
            RegisterSpellProcessor(RelevantNanos.MPCompositeNano, CompositeNanoTeam);
            RegisterSpellProcessor(RelevantNanos.AnticipationofRetaliation, EvadesTeam);

            RegisterSpellProcessor(RelevantGenericNanos.MatMetBuffs, MatMet);
            RegisterSpellProcessor(RelevantGenericNanos.BioMetBuffs, BioMet);
            RegisterSpellProcessor(RelevantGenericNanos.PsyModBuffs, PsyMod);
            RegisterSpellProcessor(RelevantGenericNanos.SenImpBuffs, SenImp);
            RegisterSpellProcessor(RelevantGenericNanos.MatCreBuffs, MatCre);
            RegisterSpellProcessor(RelevantGenericNanos.MatLocBuffs, MatLoc);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.InterruptModifier).OrderByStackingOrder(), InterruptTeam);
            RegisterSpellProcessor(RelevantNanos.CostBuffs, CostTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PistolBuff).OrderByStackingOrder(), PistolTeam);

            //Debuffs
            RegisterSpellProcessor(RelevantNanos.WarmUpfNukes, WarmUpNuke, (CombatActionPriority)3);
            RegisterSpellProcessor(RelevantNanos.SingleTargetNukes, SingleTargetNuke, (CombatActionPriority)4);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MPDamageDebuffLineA).OrderByStackingOrder(), DamageDebuff, (CombatActionPriority)6);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MPDamageDebuffLineB).OrderByStackingOrder(), DamageDebuff, (CombatActionPriority)6);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MetaPhysicistDamageDebuff).OrderByStackingOrder(), DamageDebuff, (CombatActionPriority)5);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoResistanceDebuff_LineA).OrderByStackingOrder(), NanoResistanceDebuff, (CombatActionPriority)6);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoShutdownDebuff).OrderByStackingOrder(), NanoShutdownDebuff, (CombatActionPriority)7);

            //Pet Spawners
            RegisterSpellProcessor(RelevantNanos.AttackPets, AttackPetSpawner, (CombatActionPriority)2);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SupportPets).OrderByStackingOrder(), SupportPetSpawner, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.HealPets, HealPetSpawner, (CombatActionPriority)2);

            //Pet Buffs
            RegisterSpellProcessor(RelevantNanos.PetCleanse, PetCleanse, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.PetWarp, PetWarp, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.MastersBidding, MastersBidding);
            RegisterSpellProcessor(RelevantNanos.InducedApathy, InducedApathy);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MesmerizationConstructEmpowerment).OrderByStackingOrder(), MezzPetSeed);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.HealingConstructEmpowerment).OrderByStackingOrder(), HealPetSeed);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AggressiveConstructEmpowerment).OrderByStackingOrder(), AttackPetSeed);

            RegisterSpellProcessor(RelevantNanos.AnticipationofRetaliation, EvadesPet, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.InstillDamageBuffs, InstillDamage, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.ChantBuffs, Chant, (CombatActionPriority)5);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MPAttackPetDamageType).OrderByStackingOrder(), DamageType, (CombatActionPriority)5);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetDamageOverTimeResistNanos).OrderByStackingOrder(), NanoResistance, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.Defensive, Defensive, (CombatActionPriority)5);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetHealDelta843).OrderByStackingOrder(), HealDelta, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.ShortTermDamage, ShortTermDamage, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.CostBuffs, CostPet, (CombatActionPriority)5);

            #region Commands

            Chat.RegisterCommand("handlerchannel", ChannelCommand);
            Chat.RegisterCommand("disbandall", DisbandAllCommand);

            #endregion

            PluginDirectory = pluginDir;
        }

        #region Callbacks

        public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || Time.NormalTime < _lastZonedTime + 2f) { return; }

            if (!DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
            SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        }
        private void OnPerksMessage(int sender, IPCMessage msg)
        {
            PerkMessage perkMsg = (PerkMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Perks"] = perkMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnBuffingMessage(int sender, IPCMessage msg)
        {
            BuffingMessage buffMsg = (BuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Buffing"] = buffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnCompositesMessage(int sender, IPCMessage msg)
        {
            CompositesMessage compMsg = (CompositesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Composites"] = compMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnDebuffingMessage(int sender, IPCMessage msg)
        {
            DebuffingMessage debuffMsg = (DebuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Debuffing"] = debuffMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnChannelMessage(int sender, IPCMessage msg)
        {
            ChannelMessage channelMsg = (ChannelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].IPCChannel = channelMsg.Id;
            Config.Save();

            var window = SettingsController.settingsWindow;
            if (window != null && window.IsValid && window.IsVisible)
            {
                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                Config.Save();
            }

            IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
            Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
            SettingsController.RemainingNCU.Clear();
        }
        private void OnDisbandAllMessage(int sender, IPCMessage msg)
        {
            DisbandAllMessage disbandallMsg = (DisbandAllMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Team.Leave();
        }
        private void OnSicknessMessage(int sender, IPCMessage msg)
        {
            SicknessMessage sicknessMsg = (SicknessMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Sickness"] = sicknessMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnInvitesMessage(int sender, IPCMessage msg)
        {
            InvitesMessage invitesMsg = (InvitesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Invites"] = invitesMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnTradingMessage(int sender, IPCMessage msg)
        {
            TradingMessage tradingMsg = (TradingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Trading"] = tradingMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnLevelMessage(int sender, IPCMessage msg)
        {
            LevelMessage levelMsg = (LevelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].Level = levelMsg.Level;

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid && window.IsVisible) { levelInput.Text = $"{levelMsg.Level}"; }
            else
            {
                _settings[$"Level"] = levelMsg.Switch;
                Level = levelMsg.Level;
                SettingsController.CleanUp();
            }
        }

        #endregion

        #region Handles
        private void HandleGenericRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new CompositesMessage()
            {
                Switch = _settings["Composites"].AsBool()
            });
            IPCChannel.Broadcast(new BuffingMessage()
            {
                Switch = _settings["Buffing"].AsBool()
            });
            IPCChannel.Broadcast(new PerkMessage()
            {
                Switch = _settings["Perks"].AsBool()
            });
            IPCChannel.Broadcast(new DebuffingMessage()
            {
                Switch = _settings["Debuffing"].AsBool()
            });
            IPCChannel.Broadcast(new SicknessMessage()
            {
                Switch = _settings["Sickness"].AsBool()
            });
            IPCChannel.Broadcast(new InvitesMessage()
            {
                Switch = _settings["Invites"].AsBool()
            });
            IPCChannel.Broadcast(new TradingMessage()
            {
                Switch = _settings["Trading"].AsBool()
            });
            IPCChannel.Broadcast(new LevelMessage()
            {
                Switch = _settings["Level"].AsBool(),
                Level = Level
            });
        }
        private void HandlePetViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_petView)) { return; }

                _petView = View.CreateFromXml(PluginDirectory + "\\UI\\MPPetsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Pets", XmlViewName = "MPPetsView" }, _petView);

                InitSettings(window);
            }
            else if (_petWindow == null || (_petWindow != null && !_petWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_petWindow, PluginDir, new WindowOptions() { Name = "Pets", XmlViewName = "MPPetsView" }, _petView, out var container);
                _petWindow = container;

                InitSettings(container);
            }
        }
        private void HandlePerkViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_perkView)) { return; }

                _perkView = View.CreateFromXml(PluginDirectory + "\\UI\\MPPerksView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Perks", XmlViewName = "MPPerksView" }, _perkView);

                InitSettings(window);
            }
            else if (_perkWindow == null || (_perkWindow != null && !_perkWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_perkWindow, PluginDir, new WindowOptions() { Name = "Perks", XmlViewName = "MPPerksView" }, _perkView, out var container);
                _perkWindow = container;

                InitSettings(container);
            }
        }
        private void HandleBuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_buffView)) { return; }

                _buffView = View.CreateFromXml(PluginDirectory + "\\UI\\MPBuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Buffs", XmlViewName = "MPBuffsView" }, _buffView);

                InitSettings(window);
            }
            else if (_buffWindow == null || (_buffWindow != null && !_buffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_buffWindow, PluginDir, new WindowOptions() { Name = "Buffs", XmlViewName = "MPBuffsView" }, _buffView, out var container);
                _buffWindow = container;

                InitSettings(container);
            }
        }

        private void HandleDebuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_debuffView)) { return; }

                _debuffView = View.CreateFromXml(PluginDirectory + "\\UI\\MPDebuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Debuffs", XmlViewName = "MPDebuffsView" }, _debuffView);

                InitSettings(window);
            }
            else if (_debuffWindow == null || (_debuffWindow != null && !_debuffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_debuffWindow, PluginDir, new WindowOptions() { Name = "Debuffs", XmlViewName = "MPDebuffsView" }, _debuffView, out var container);
                _debuffWindow = container;

                InitSettings(container);
            }
        }
        private void HandleItemViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_itemView)) { return; }

                _itemView = View.CreateFromXml(PluginDirectory + "\\UI\\MPItemsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Items", XmlViewName = "MPItemsView" }, _itemView);

                InitSettings(window);
            }
            else if (_itemWindow == null || (_itemWindow != null && !_itemWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_itemWindow, PluginDir, new WindowOptions() { Name = "Items", XmlViewName = "MPItemsView" }, _itemView, out var container);
                _itemWindow = container;

                InitSettings(container);
            }
        }
        private void HandleGenericViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_genericView)) { return; }

                _genericView = View.CreateFromXml(PluginDirectory + "\\UI\\MPGenericView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Generic", XmlViewName = "MPGenericView" }, _genericView);

                InitSettings(window);
            }
            else if (_genericWindow == null || (_genericWindow != null && !_genericWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_genericWindow, PluginDir, new WindowOptions() { Name = "Generic", XmlViewName = "MPGenericView" }, _genericView, out var container);
                _genericWindow = container;

                InitSettings(container);
            }
        }
        private void HandleProcViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_procView)) { return; }

                _procView = View.CreateFromXml(PluginDirectory + "\\UI\\MPProcsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Procs", XmlViewName = "MPProcsView" }, _procView);

                InitSettings(window);
            }
            else if (_procWindow == null || (_procWindow != null && !_procWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_procWindow, PluginDir, new WindowOptions() { Name = "Procs", XmlViewName = "MPProcsView" }, _procView, out var container);
                _procWindow = container;

                InitSettings(container);
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            #region Pets

            if (Time.NormalTime > _lastPetSyncTime + 0.9f)
            {
                if (DynelManager.LocalPlayer.Pets.Count() >= 1 && CanLookupPetsAfterZone())
                {
                    if (_settings["HealPets"].AsBool())
                        ListenerPetSit();

                    AssignTargetToHealPet();
                    AssignTargetToMezzPet();

                    if (_settings["SyncPets"].AsBool())
                    {
                        foreach (Pet _pet in DynelManager.LocalPlayer.Pets.Where(c => c.Type == PetType.Attack || c.Type == PetType.Support))
                        {
                            if (!DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending
                                && (bool)_pet?.Character.IsAttacking) { _pet?.Follow(); }

                            if (DynelManager.LocalPlayer.IsAttacking)
                            {
                                if (_pet?.Character.IsAttacking == false)
                                    _pet?.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                                else if (_pet?.Character.IsAttacking == true && _pet?.Character.FightingTarget != null
                                    && _pet?.Character.FightingTarget.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                                    _pet?.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                            }
                        }
                    }
                }

                _lastPetSyncTime = Time.NormalTime;
            }

            #endregion

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

                IPCChannel.Broadcast(ncuMessage);

                SettingsController.RemainingNCU[DynelManager.LocalPlayer.Identity] = DynelManager.LocalPlayer.RemainingNCU;

                base.OnUpdate(deltaTime);
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            var window = SettingsController.FindValidWindow(_windows);

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (window != null && window.IsValid && window.IsVisible)
            {
                if (window.FindView("GenericRelay", out Button genericRelay))
                {
                    genericRelay.Tag = window;
                    genericRelay.Clicked = HandleGenericRelayClick;
                }

                if (levelInput != null && !string.IsNullOrEmpty(levelInput.Text))
                    if (int.TryParse(levelInput.Text, out int levelValue))
                        if (Config.CharSettings[Game.ClientInst].Level != levelValue)
                            Config.CharSettings[Game.ClientInst].Level = levelValue;
                if (fountainOfLifeInput != null && !string.IsNullOrEmpty(fountainOfLifeInput.Text))
                    if (int.TryParse(fountainOfLifeInput.Text, out int fountainOfLifeValue))
                        if (Config.CharSettings[Game.ClientInst].FountainOfLifePercentage != fountainOfLifeValue)
                            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = fountainOfLifeValue;
                if (stimTargetInput != null && !string.IsNullOrEmpty(stimTargetInput.Text))
                    if (Config.CharSettings[Game.ClientInst].StimTargetName != stimTargetInput.Text)
                        Config.CharSettings[Game.ClientInst].StimTargetName = stimTargetInput.Text;
                if (stimHealthInput != null && !string.IsNullOrEmpty(stimHealthInput.Text))
                    if (int.TryParse(stimHealthInput.Text, out int stimHealthValue))
                        if (Config.CharSettings[Game.ClientInst].StimHealthPercentage != stimHealthValue)
                            Config.CharSettings[Game.ClientInst].StimHealthPercentage = stimHealthValue;
                if (stimNanoInput != null && !string.IsNullOrEmpty(stimNanoInput.Text))
                    if (int.TryParse(stimNanoInput.Text, out int stimNanoValue))
                        if (Config.CharSettings[Game.ClientInst].StimNanoPercentage != stimNanoValue)
                            Config.CharSettings[Game.ClientInst].StimNanoPercentage = stimNanoValue;
                if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        if (Config.CharSettings[Game.ClientInst].KitHealthPercentage != kitHealthValue)
                            Config.CharSettings[Game.ClientInst].KitHealthPercentage = kitHealthValue;
                if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        if (Config.CharSettings[Game.ClientInst].KitNanoPercentage != kitNanoValue)
                            Config.CharSettings[Game.ClientInst].KitNanoPercentage = kitNanoValue;
                if (areaDebuffRangeInput != null && !string.IsNullOrEmpty(areaDebuffRangeInput.Text))
                    if (float.TryParse(areaDebuffRangeInput.Text, out float areaDebuffRangeValue))
                        if (Config.CharSettings[Game.ClientInst].AreaDebuffRange != areaDebuffRangeValue)
                            Config.CharSettings[Game.ClientInst].AreaDebuffRange = areaDebuffRangeValue;
                if (cleanseInput != null && !string.IsNullOrEmpty(cleanseInput.Text))
                    if (int.TryParse(cleanseInput.Text, out int cleanseValue))
                        if (Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay != cleanseValue)
                            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = cleanseValue;
                if (survivalInput != null && !string.IsNullOrEmpty(survivalInput.Text))
                    if (int.TryParse(survivalInput.Text, out int survivalValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay != survivalValue)
                            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = survivalValue;
                if (sphereInput != null && !string.IsNullOrEmpty(sphereInput.Text))
                    if (int.TryParse(sphereInput.Text, out int sphereValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay != sphereValue)
                            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = sphereValue;
                if (witOfTheAtroxInput != null && !string.IsNullOrEmpty(witOfTheAtroxInput.Text))
                    if (int.TryParse(witOfTheAtroxInput.Text, out int witOfTheAtroxValue))
                        if (Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay != witOfTheAtroxValue)
                            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = witOfTheAtroxValue;
                if (selfHealInput != null && !string.IsNullOrEmpty(selfHealInput.Text))
                    if (int.TryParse(selfHealInput.Text, out int selfHealValue))
                        if (Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage != selfHealValue)
                            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = selfHealValue;
                if (selfNanoInput != null && !string.IsNullOrEmpty(selfNanoInput.Text))
                    if (int.TryParse(selfNanoInput.Text, out int selfNanoValue))
                        if (Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage != selfNanoValue)
                            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = selfNanoValue;
                if (teamHealInput != null && !string.IsNullOrEmpty(teamHealInput.Text))
                    if (int.TryParse(teamHealInput.Text, out int teamHealValue))
                        if (Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage != teamHealValue)
                            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = teamHealValue;
                if (teamNanoInput != null && !string.IsNullOrEmpty(teamNanoInput.Text))
                    if (int.TryParse(teamNanoInput.Text, out int teamNanoValue))
                        if (Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage != teamNanoValue)
                            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = teamNanoValue;
                if (bodyDevInput != null && !string.IsNullOrEmpty(bodyDevInput.Text))
                    if (int.TryParse(bodyDevInput.Text, out int bodyDevValue))
                        if (Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage != bodyDevValue)
                            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = bodyDevValue;
                if (strengthInput != null && !string.IsNullOrEmpty(strengthInput.Text))
                    if (int.TryParse(strengthInput.Text, out int strengthValue))
                        if (Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage != strengthValue)
                            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = strengthValue;

                SettingsController.CleanUp();
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid && SettingsController.settingsWindow.IsVisible)
            {
                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("PetsView", out Button petView))
                {
                    petView.Tag = SettingsController.settingsWindow;
                    petView.Clicked = HandlePetViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffsView", out Button buffView))
                {
                    buffView.Tag = SettingsController.settingsWindow;
                    buffView.Clicked = HandleBuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DebuffsView", out Button debuffView))
                {
                    debuffView.Tag = SettingsController.settingsWindow;
                    debuffView.Clicked = HandleDebuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ProcsView", out Button procView))
                {
                    procView.Tag = SettingsController.settingsWindow;
                    procView.Clicked = HandleProcViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ItemsView", out Button itemView))
                {
                    itemView.Tag = SettingsController.settingsWindow;
                    itemView.Clicked = HandleItemViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PerksView", out Button perkView))
                {
                    perkView.Tag = SettingsController.settingsWindow;
                    perkView.Clicked = HandlePerkViewClick;
                }

                if (SettingsController.settingsWindow.FindView("GenericView", out Button genericView))
                {
                    genericView.Tag = SettingsController.settingsWindow;
                    genericView.Clicked = HandleGenericViewClick;
                }

                SettingsController.CleanUp();
            }

            #endregion
        }

        #region LE Procs

        private bool AnticipatedEvasion(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.AnticipatedEvasion != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool EconomicNanobotUse(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.EconomicNanobotUse != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool NanobotContingentArrest(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.NanobotContingentArrest != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool RegainFocus(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.RegainFocus != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool ThoughtfulMeans(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.ThoughtfulMeans != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()
                || ProcType1Selection.None == (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool DiffuseRage(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.DiffuseRage != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool EgoStrike(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.EgoStrike != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool MindWail(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.MindWail != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool SowDespair(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.SowDespair != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool SowDoubt(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.SowDoubt != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool SuperEgoStrike(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.SuperEgoStrike != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool SuppressFury(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.SuppressFury != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()
                || ProcType2Selection.None == (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Nukes

        private bool WarmUpNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || fightingTarget == null || !_settings["Nukes"].AsBool() || !CanCast(spell)) { return false; }

            if (_singleNukeSpellDummy != null)
            {
                if (fightingTarget.Buffs.Contains(NanoLine.MetaphysicistMindDamageNanoDebuffs)) { return false; }
            }

            return true;
        }

        private bool SingleTargetNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool()) { return false; }

            if (fightingTarget == null || !_settings["Nukes"].AsBool() || !CanCast(spell)) { return false; }

            if (_warmUpNukeSpellDummy != null)
            {
                if (!fightingTarget.Buffs.Contains(NanoLine.MetaphysicistMindDamageNanoDebuffs)) { return false; }
            }

            return true;
        }

        #endregion

        #region Buffs

        protected bool Evades(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["EvadesTeam"].AsBool()) { return false; }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool MatCre(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if ((CompositeNanoSkillsSelection.Self == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32()
                 || CompositeNanoSkillsSelection.Team == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32())
                && _settings["ReplenishNanoSkills"].AsBool())
            {
                _settings["ReplenishNanoSkills"] = false;

                Chat.WriteLine("Only activate one option.");
            }

            if (CompositeNanoSkillsSelection.None == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32() && _settings["ReplenishNanoSkills"].AsBool())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool PsyMod(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CompositeNanoSkillsSelection.None == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32() && _settings["ReplenishNanoSkills"].AsBool())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool MatLoc(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CompositeNanoSkillsSelection.None == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32() && _settings["ReplenishNanoSkills"].AsBool())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool SenImp(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CompositeNanoSkillsSelection.None == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32() && _settings["ReplenishNanoSkills"].AsBool())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool BioMet(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CompositeNanoSkillsSelection.None == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32() && _settings["ReplenishNanoSkills"].AsBool())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        private bool MatMet(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CompositeNanoSkillsSelection.None == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32() && _settings["ReplenishNanoSkills"].AsBool())
                return GenericCombatTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        #endregion

        #region Team Buffs

        private bool CostTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CostSelection.Team == (CostSelection)_settings["CostSelection"].AsInt32())
                return TeamBuffExclusion(spell, fightingTarget, ref actionTarget);

            if (CostSelection.Self == (CostSelection)_settings["CostSelection"].AsInt32())
                return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return false;
        }

        private bool InterruptTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (InterruptSelection.Team == (InterruptSelection)_settings["InterruptSelection"].AsInt32())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            if (InterruptSelection.Self == (InterruptSelection)_settings["InterruptSelection"].AsInt32())
                return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return false;
        }

        private bool CompositeNanoTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (CompositeNanoSkillsSelection.Team == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            if (CompositeNanoSkillsSelection.Self == (CompositeNanoSkillsSelection)_settings["CompositeNanoSkillsSelection"].AsInt32())
                return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return false;
        }


        protected bool EvadesTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["EvadesTeam"].AsBool())
                return GenericTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        protected bool PistolTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["PistolTeam"].AsBool())
                return TeamBuffExclusionWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);

            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
        }

        #endregion

        #region Debuffs

        private bool NanoShutdownDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (NanoShutdownDebuffSelection.None == (NanoShutdownDebuffSelection)_settings["NanoShutdownDebuff"].AsInt32()) { return false; }

            if (NanoShutdownDebuffSelection.Area == (NanoShutdownDebuffSelection)_settings["NanoShutdownDebuff"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);

            if (NanoShutdownDebuffSelection.Target == (NanoShutdownDebuffSelection)_settings["NanoShutdownDebuff"].AsInt32()
                && fightingTarget != null)
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name)) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            if (NanoShutdownDebuffSelection.Boss == (NanoShutdownDebuffSelection)_settings["NanoShutdownDebuff"].AsInt32()
                 && fightingTarget != null)
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name)) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool NanoResistanceDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (NanoResistanceDebuffSelection.None == (NanoResistanceDebuffSelection)_settings["NanoResistanceDebuff"].AsInt32()) { return false; }

            if (NanoResistanceDebuffSelection.Area == (NanoResistanceDebuffSelection)_settings["NanoResistanceDebuff"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);

            if (NanoResistanceDebuffSelection.Target == (NanoResistanceDebuffSelection)_settings["NanoResistanceDebuff"].AsInt32()
                && fightingTarget != null)
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name)) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            if (NanoResistanceDebuffSelection.Boss == (NanoResistanceDebuffSelection)_settings["NanoResistanceDebuff"].AsInt32()
                 && fightingTarget != null)
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name)) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        private bool DamageDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DamageDebuffSelection.None == (DamageDebuffSelection)_settings["DamageDebuffSelection"].AsInt32()) { return false; }

            if (DamageDebuffSelection.Area == (DamageDebuffSelection)_settings["DamageDebuffSelection"].AsInt32())
                return AreaDebuff(spell, spell.Nanoline, AreaDebuffRange, ref actionTarget);

            if (DamageDebuffSelection.Target == (DamageDebuffSelection)_settings["DamageDebuffSelection"].AsInt32()
                && fightingTarget != null)
            {
                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name)) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            if (DamageDebuffSelection.Boss == (DamageDebuffSelection)_settings["DamageDebuffSelection"].AsInt32()
                 && fightingTarget != null)
            {
                if (fightingTarget?.MaxHealth < 1000000) { return false; }

                if (Constants.debuffTargetsToIgnore.Contains(fightingTarget?.Name)) { return false; }

                return TargetDebuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
            }

            return false;
        }

        #endregion

        #region Pets

        #region Buffs
        private bool MezzPetSeed(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.MesmerizationConstructEmpowerment);
        }

        private bool HealPetSeed(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.HealingConstructEmpowerment);
        }

        private bool AttackPetSeed(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AggressiveConstructEmpowerment);
        }

        private bool DamageType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.MPAttackPetDamageType, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        private bool EvadesPet(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.MajorEvasionBuffs, PetType.Attack, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.MajorEvasionBuffs, PetType.Heal, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.MajorEvasionBuffs, PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        private bool Chant(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.MPPetInitiativeBuffs, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        private bool InstillDamage(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.MPPetDamageBuffs, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        private bool HealDelta(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.PetHealDelta843, PetType.Attack, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.PetHealDelta843, PetType.Heal, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.PetHealDelta843, PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        private bool Defensive(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.PetDefensiveNanos, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        private bool NanoResistance(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.PetDamageOverTimeResistNanos, PetType.Attack, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.PetDamageOverTimeResistNanos, PetType.Heal, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.PetDamageOverTimeResistNanos, PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        private bool ShortTermDamage(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.PetShortTermDamageBuffs, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        private bool CostPet(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.NPCostBuff, PetType.Heal, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(NanoLine.NPCostBuff, PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        protected bool InducedApathy(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["BuffPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            if (PetProcSelection.InducedApathy != (PetProcSelection)_settings["PetProcSelection"].AsInt32()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .Where(c => c.Character != null
                    && c.Character?.Health > 0
                    && c.Type == PetType.Attack
                    && (bool)!c.Character?.Buffs.Contains(NanoLine.SiphonBox683))
                .FirstOrDefault();

            if (_pet != null && _pet.Character != null)
            {
                if (spell.IsReady) { spell.Cast(_pet.Character, true); }
            }

            return false;
        }

        protected bool MastersBidding(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["BuffPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            if (PetProcSelection.MastersBidding != (PetProcSelection)_settings["PetProcSelection"].AsInt32()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .Where(c => c.Character != null
                    && c.Character?.Health > 0
                    && c.Type == PetType.Attack
                    && (bool)!c.Character?.Buffs.Contains(NanoLine.SiphonBox683))
                .FirstOrDefault();

            if (_pet != null && _pet.Character != null)
            {
                if (spell.IsReady) { spell.Cast(_pet.Character, true); }
            }

            return false;
        }

        #endregion

        #endregion

        #region Misc

        private static bool PetNanoLow()
        {
            return (TryGetHealPet(out _pet) && (_pet.Character?.Nano / PetMaxNanoPool(_pet) * 100 <= 55
                    && _pet.Character?.Nano != 10
                    && DynelManager.LocalPlayer.DistanceFrom(_pet.Character) < 10f && _pet.Character?.IsInLineOfSight == true));
        }

        private static bool PetHindered()
        {
            return (TryGetHinderedPet(out _pet) && _pet != null
                    && DynelManager.LocalPlayer.DistanceFrom(_pet.Character) < 10f && _pet.Character?.IsInLineOfSight == true);
        }

        private void ListenerPetSit()
        {
            if (_initSit)
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                {
                    _kit?.Use(_pet?.Character, true);
                    _sitPetUsedTimer = Time.NormalTime;
                    _initSit = false;
                    MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                }
            }
            else if (!InCombat() && Time.NormalTime > _sitPetUsedTimer + 16f && CanUseSitKit(out _kit) && (PetNanoLow() || PetHindered()))
            {
                _initSit = true;
                MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            }

            //bool shouldSitKit = PetNanoLow() || PetHindered();
            //bool canSitKit = CanUseSitKit(out _kit);

            //if (_initSit)
            //{
            //    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //    {
            //        if (canSitKit && shouldSitKit)
            //        {
            //            _kit?.Use(_pet?.Character, true);
            //            _sitPetUsedTimer = Time.NormalTime;
            //        }
            //        else if (InCombat())
            //        {
            //            _initSit = false;
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //        }
            //    }
            //    else { _initSit = false; }
            //}
            //else if (!InCombat() && Time.NormalTime > _sitPetUsedTimer + 16f && canSitKit && shouldSitKit)
            //{
            //    _initSit = true;
            //    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //}

            //if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //{
            //    if (InCombat() || (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66 && !PetNanoLow() && !PetHindered()))
            //    {
            //        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //        _initSit = false;
            //    }
            //    else if (canSitKit && shouldSitKit)
            //    {
            //        _kit?.Use(_pet?.Character, true);
            //        _sitPetUsedTimer = Time.NormalTime;
            //    }
            //    else if (_initSit)
            //    {
            //        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //        _initSit = false;
            //    }
            //}
            //else if (DynelManager.LocalPlayer.MovementState == MovementState.Run && canSitKit && shouldSitKit
            //    && Time.NormalTime > _sitPetUsedTimer + 16f && !InCombat())
            //{
            //    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //    _initSit = true;
            //}
        }

        protected bool AttackPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["AttackPet"].AsBool()) { return false; }

            if (AttackPetSpawn(spell, fightingTarget, ref actionTarget))
            {
                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }

            return false;
        }

        protected bool SupportPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["MezzPet"].AsBool()) { return false; }

            if (SupportPetSpawn(spell, fightingTarget, ref actionTarget)) 
            { 
                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }

            return false;
        }

        protected bool HealPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["HealPet"].AsBool()) { return false; }

            if (HealPetSpawn(spell, fightingTarget, ref actionTarget))
            {
                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }
            return false;
        }

        private SimpleChar GetTargetToHeal()
        {
            if (PetHealingSelection.Team == (PetHealingSelection)_settings["PetHealingSelection"].AsInt32())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.Health > 0
                        && c.HealthPercent < 85f
                        && DynelManager.LocalPlayer.DistanceFrom(c) < 15f
                        && PetCheck(c))
                    .OrderBy(c => c.HealthPercent)
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    return dyingTeamMember;
                }
            }
            else if (PetHealingSelection.Area == (PetHealingSelection)_settings["PetHealingSelection"].AsInt32())
            {
                SimpleChar dyingPlayer = DynelManager.Characters
                    .Where(c => c.Health > 0
                        && c.HealthPercent < 85f && DynelManager.LocalPlayer.DistanceFrom(c) <= 15f
                        && (c.IsPlayer || (c.IsPet && DynelManager.LocalPlayer.Pets.Select(x => x.Identity).Contains(c.Identity))))
                    .OrderBy(c => c.HealthPercent)
                    .FirstOrDefault();

                if (dyingPlayer != null)
                {
                    return dyingPlayer;
                }
            }

            return null;
        }

        private void AssignTargetToHealPet()
        {
            if (Time.NormalTime > _petHealDelay + 5f)
            {
                _petHealDelay = Time.NormalTime;

                Pet healPet = DynelManager.LocalPlayer.Pets.Where(pet => pet.Type == PetType.Heal).FirstOrDefault();

                if (healPet != null)
                {
                    SimpleChar dyingTarget = GetTargetToHeal();

                    if (healPet.Character.Nano <= 1) { return; }

                    if (healPet.Character.DistanceFrom(DynelManager.LocalPlayer) > 20f && !InYalm())
                    {
                        healPet.Follow();
                        return;
                    }

                    if (dyingTarget == null) { return; }

                    healPet.Heal(dyingTarget.Identity);
                }
            }
        }

        private SimpleChar GetTargetToMezz()
        {
            return DynelManager.Characters
                .Where(c => c.Health > 0
                    && !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                    && !c.Buffs.Contains(NanoLine.Mezz)
                    && DynelManager.LocalPlayer.FightingTarget.Identity != c.Identity
                    && !c.IsPlayer && !c.IsPet
                    && AttackingTeam(c)
                    && c.IsInLineOfSight
                    && c.DistanceFrom(DynelManager.LocalPlayer) <= 15f)
                .FirstOrDefault();
        }

        private void AssignTargetToMezzPet()
        {
            if (DynelManager.LocalPlayer.IsAttacking && Time.NormalTime > _petMezzDelay + 9)
            {
                _petMezzDelay = Time.NormalTime;

                Pet mezzPet = DynelManager.LocalPlayer.Pets.Where(pet => pet.Type == PetType.Support).FirstOrDefault();

                if (mezzPet != null)
                {
                    SimpleChar targetToMezz = GetTargetToMezz();

                    if (mezzPet.Character.Nano <= 1) { return; }

                    if (mezzPet.Character.DistanceFrom(DynelManager.LocalPlayer) > 20f && !InYalm())
                    {
                        mezzPet.Follow();
                        return;
                    }

                    mezzPet.Attack(targetToMezz.Identity);
                }
            }
        }
        public static void DisbandAllCommand(string command, string[] param, ChatWindow chatWindow)
        {
            IPCChannel.Broadcast(new DisbandAllMessage());
        }
        public static void ChannelCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length > 0)
            {
                if (param.Length > 1 && param[0].ToLower() == "all")
                {
                    if (param[1] != null && Convert.ToByte(param[1]) > 0)
                    {
                        IPCChannel.Broadcast(new ChannelMessage()
                        {
                            Id = Convert.ToByte(param[1])
                        });

                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[1]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
                else if (param.Length == 1 && Convert.ToByte(param[0]) > 0)
                {
                    if (param[0] != null && Convert.ToByte(param[0]) > 0)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[0]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
            }
        }

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }

        private static class RelevantNanos
        {
            public const int MastersBidding = 268171;
            public const int InducedApathy = 301888;
            public const int AnticipationofRetaliation = 29272;
            public const int ImprovedAnticipationofRetaliation = 302188;
            public const int PetWarp = 209488;
            public static readonly int[] CostBuffs = { 95409, 29307, 95411, 95408, 95410 };
            public static readonly int[] HealPets = { 225902, 125746, 125739, 125740, 125741, 125742, 125743, 125744, 125745, 125738 };
            public static readonly int[] AttackPets = { 254859, 225900, 254859, 225900, 225898, 225896, 225894, 43737, 43731, 43732, 43735, 43734, 43733, 43324 };
            public static readonly int[] SLAttackPets = { 254859, 225900, 254859, 225900, 225898, 225896, 225894 };
            public static readonly int[] MPCompositeNano = { 220343, 220341, 220339, 220337, 220335, 220333, 220331 };
            public static readonly int[] WarmUpfNukes = { 270355, 125761, 29297, 125762, 29298, 29114 };
            public static readonly int[] Defensive = { 267601, 267600, 267599 };
            public static readonly int[] PetCleanse = { 269870, 269869 };

            public static readonly int[] ShortTermDamage = { 267598, 205193, 151827, 205189, 205187, 151828, 205185, 151824, 
                205183,151830, 205191, 151826, 205195, 151825, 205197, 151831 };

            public static readonly int[] SingleTargetNukes = { 267878, 125763, 125760, 125765, 125764 };
            public static readonly int[] InstillDamageBuffs = { 270800, 285101, 116814, 116817, 116812, 116816, 116821, 116815, 116813 };
            public static readonly int[] ChantBuffs = { 116819, 116818, 116811, 116820 };

            //public static readonly string[] TwoHandedNames = { "Azure Cobra of Orma", "Wixel's Notum Python", "Asp of Semol", "Viper Staff" };
            //public static readonly string[] OneHandedNames = { "Asp of Titaniush", "Gold Acantophis", "Bitis Striker", "Coplan's Hand Taipan", "The Crotalus" };
            //public static readonly string[] ShieldNames = { "Shield of Zset", "Shield of Esa", "Shield of Asmodian", "Mocham's Guard", "Death Ward", "Belthior's Flame Ward", "Wave Breaker", "Living Shield of Evernan", "Solar Guard", "Notum Defender", "Vital Buckler" };
        }

        public enum PetProcSelection
        {
            None, InducedApathy, MastersBidding
        }
        public enum CompositeNanoSkillsSelection
        {
            None, Self, Team
        }
        public enum PetHealingSelection
        {
            None, Team, Area
        }

        public enum DamageDebuffSelection
        {
            None, Target, Area, Boss
        }
        public enum NanoShutdownDebuffSelection
        {
            None, Target, Area, Boss
        }
        public enum NanoResistanceDebuffSelection
        {
            None, Target, Area, Boss
        }
        public enum InterruptSelection
        {
            None, Self, Team
        }
        public enum CostSelection
        {
            Self, Team
        }
        public enum ProcType1Selection
        {
            NanobotContingentArrest, AnticipatedEvasion, ThoughtfulMeans, RegainFocus, EconomicNanobotUse, None
        }

        public enum ProcType2Selection
        {
            SuperEgoStrike, SuppressFury, EgoStrike, MindWail, SowDoubt, SowDespair, DiffuseRage, None
        }

        //private enum SummonedWeaponSelection
        //{
        //    DISABLED = 0,
        //    TWO_HANDED = 1,
        //    ONE_HANDED_PLUS_SHIELD = 2,
        //    ONE_HANDED_PLUS_ONE_HANDED = 3,
        //    ONE_HANDED = 4,
        //    SHIELD = 5
        //}

        //private SummonedWeaponSelection GetSummonedWeaponSelection()
        //{
        //    return (SummonedWeaponSelection)settings["SummonedWeaponSelection"].AsInt32();
        //}

        #endregion
    }
}
