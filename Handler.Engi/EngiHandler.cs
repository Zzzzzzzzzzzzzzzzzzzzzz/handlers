﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using Handler.Generic;
using static Handler.Generic.Extensions;
using AOSharp.Common.Unmanaged.Imports;

namespace Handler.Engi
{
    class EngiHandler : GenericHandler
    {
        private static string PluginDirectory;

        private const float DelayBetweenTrims = 1;
        private const float DelayBetweenNCUUpgrades = 1;
        private const float DelayBetweenDivertTrims = 305;

        private bool petTrimmedTaunt = false;

        private Dictionary<PetType, bool> petTrimmedAggDef = new Dictionary<PetType, bool>();
        private Dictionary<PetType, bool> petTrimmedHp = new Dictionary<PetType, bool>();
        private Dictionary<PetType, bool> petTrimmedOff = new Dictionary<PetType, bool>();
        private Dictionary<PetType, bool> petNCUUpgraded = new Dictionary<PetType, bool>();

        private Dictionary<PetType, double> _lastPetTrimOffTime = new Dictionary<PetType, double>()
        {
            { PetType.Attack, 0 },
            { PetType.Support, 0 }
        };
        private Dictionary<PetType, double> _lastPetTrimHpTime = new Dictionary<PetType, double>()
        {
            { PetType.Attack, 0 },
            { PetType.Support, 0 }
        };

        private double _lastTrimTime = 0;
        private double _lastNCUUpgradeTime = 0;

        private static double _mainUpdate;

        public EngiHandler(string pluginDir) : base(pluginDir)
        {
            IPCChannel.RegisterCallback((int)IPCOpcode.RemainingNCU, OnRemainingNCUMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Perks, OnPerksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Buffing, OnBuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Composites, OnCompositesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Debuffing, OnDebuffingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sickness, OnSicknessMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Invites, OnInvitesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Level, OnLevelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Trading, OnTradingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Channel, OnChannelMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.DisbandAll, OnDisbandAllMessage);

            Game.TeleportEnded += OnZoned;

            _settings.AddVariable("SyncPets", true);
            _settings.AddVariable("SummonPets", true);
            _settings.AddVariable("BuffPets", true);
            _settings.AddVariable("HealPets", false);
            _settings.AddVariable("WarpPets", false);

            _settings.AddVariable("NCUUpgrade", false);

            _settings.AddVariable("HpTrimmer", false);
            _settings.AddVariable("OffTrimmer", true);
            _settings.AddVariable("TauntTrimmer", true);
            _settings.AddVariable("AggDefTrimmer", true);

            _settings.AddVariable("InitBuffSelection", (int)InitBuffSelection.Self);

            _settings.AddVariable("ArmorTeam", true);
            _settings.AddVariable("PistolTeam", true);
            _settings.AddVariable("DamageTeam", false);
            _settings.AddVariable("GrenadeTeam", true);
            _settings.AddVariable("DamageShieldTeam", false);

            _settings.AddVariable("ReflectSelection", (int)ReflectSelection.Shadowlands);

            _settings.AddVariable("BuffingAuraSelection", (int)BuffingAuraSelection.Damage);
            _settings.AddVariable("DebuffingAuraSelection", (int)DebuffingAuraSelection.Blind);

            _settings.AddVariable("PetPerkSelection", (int)PetPerkSelection.ChaoticBox);
            _settings.AddVariable("PetProcSelection", (int)PetProcSelection.None);

            _settings.AddVariable("ProcType1Selection", (int)ProcType1Selection.None);
            _settings.AddVariable("ProcType2Selection", (int)ProcType2Selection.None);

            _settings.AddVariable("LegShot", false);

            RegisterSettingsWindow("Handler", "EngineerSettingsView.xml");

            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcEngineerReactiveArmor, ReactiveArmor, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerDestructiveTheorem, DestructiveTheorem, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerEnergyTransfer, EnergyTransfer, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerEndureBarrage, EndureBarrage, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerDestructiveSignal, DestructiveSignal, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerSplinterPreservation, SplinterPreservation, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerCushionBlows, CushionBlows, (CombatActionPriority)20);

            RegisterPerkProcessor(PerkHash.LEProcEngineerAssaultForceRelief, AssaultForceRelief, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerDroneMissiles, DroneMissiles, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerDroneExplosives, DroneExplosives, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerCongenialEncasement, CongenialEncasement, (CombatActionPriority)20);
            RegisterPerkProcessor(PerkHash.LEProcEngineerPersonalProtection, PersonalProtection, (CombatActionPriority)20);


            //Buffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ArmorBuff).OrderByStackingOrder(), ArmorTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PistolBuff).OrderByStackingOrder(), PistolTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.GrenadeBuffs).OrderByStackingOrder(), GrenadeTeam);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ShadowlandReflectBase).OrderByStackingOrder(), ShadowlandReflect);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ReflectShield).OrderByStackingOrder(), RKReflects);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DamageShields).OrderByStackingOrder(), DamageShieldTeam);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SpecialAttackAbsorberBase).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.EngineerSpecialAttackAbsorber).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.InitiativeBuffs).OrderByStackingOrder(), InitBuff);

            RegisterSpellProcessor(RelevantNanos.BoostedTendons, GenericBuff, (CombatActionPriority)5);
            RegisterSpellProcessor(RelevantNanos.DamageBuffLineA, DamageTeam);
            RegisterSpellProcessor(RelevantNanos.Blind, BlindAura, (CombatActionPriority)4);
            RegisterSpellProcessor(RelevantNanos.ShieldRipper, ShieldRipperAura, (CombatActionPriority)4);
            RegisterSpellProcessor(RelevantNanos.ArmorAura, ArmorAura);
            RegisterSpellProcessor(RelevantNanos.DamageAura, DamageAura);
            RegisterSpellProcessor(RelevantNanos.ReflectAura, ReflectAura);
            RegisterSpellProcessor(RelevantNanos.ShieldAura, ShieldAura);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.EngineerPetAOESnareBuff).OrderByStackingOrder(), SnareAura, (CombatActionPriority)4);
            //RegisterSpellProcessor(RelevantNanos.IntrusiveAuraCancellation, AuraCancellation);


            //Pet Spawners
            RegisterSpellProcessor(PetsList.Pets.Where(c => c.Value.PetType == PetType.Attack).Select(c => c.Key).ToArray(), AttackPetSpawner, (CombatActionPriority)3);
            RegisterSpellProcessor(PetsList.Pets.Where(c => c.Value.PetType == PetType.Support).Select(c => c.Key).ToArray(), SupportPetSpawner, (CombatActionPriority)3);

            //Pet Buffs
            RegisterSpellProcessor(RelevantNanos.PetCleanse, PetCleanse, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.PetWarp, PetWarp, (CombatActionPriority)1);
            RegisterSpellProcessor(RelevantNanos.ShieldOfObedientServant, ShieldOfTheObedientServant);
            RegisterSpellProcessor(RelevantNanos.MastersBidding, MastersBidding);
            RegisterSpellProcessor(RelevantNanos.SedativeInjectors, SedativeInjectors);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.EngineerMiniaturization).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetShortTermDamageBuffs).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetDefensiveNanos).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MPPetInitiativeBuffs).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.EngineerMiniaturization).OrderByStackingOrder(), PetTargetBuff);

            RegisterSpellProcessor(RelevantNanos.PetHealing, PetHealing, (CombatActionPriority)2);
            RegisterSpellProcessor(RelevantNanos.PetHealingGreater, PetHealingGreater, (CombatActionPriority)2);


            //Pet Perks
            RegisterPerkProcessor(PerkHash.ChaoticEnergy, ChaoticBox);
            RegisterPerkProcessor(PerkHash.SiphonBox, SiphonBox);
            RegisterPerkProcessor(PerkHash.TauntBox, TauntBox);

            //Pet NCU Upgrade
            InitNCUUpgrade();

            RegisterItemProcessor(new int[] { RelevantItems.AndroidNCUUpgradeLow, RelevantItems.AndroidNCUUpgradeHigh, RelevantItems.AndroidNCUInjector }, AndroidNCUUpgrade);

            //Pet Trimmers
            ResetTrimmers();

            RegisterItemProcessor(RelevantTrimmers.PositiveAggressiveDefensive, RelevantTrimmers.PositiveAggressiveDefensive, PetAggDefTrimmer);
            RegisterItemProcessor(new int[] { RelevantTrimmers.IncreaseAggressivenessLow, RelevantTrimmers.IncreaseAggressivenessHigh }, PetAggressiveTrimmer);
            RegisterItemProcessor(new int[] { RelevantTrimmers.DivertEnergyToOffenseLow, RelevantTrimmers.DivertEnergyToOffenseHigh }, PetOffTrimmer);
            RegisterItemProcessor(new int[] { RelevantTrimmers.DivertEnergyToHitpointsLow, RelevantTrimmers.DivertEnergyToHitpointsHigh }, PetHpTrimmer);

            #region Commands

            Chat.RegisterCommand("handlerchannel", ChannelCommand);
            Chat.RegisterCommand("disbandall", DisbandAllCommand);

            #endregion

            PluginDirectory = pluginDir;
        }

        #region Callbacks

        public static void OnRemainingNCUMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || Time.NormalTime < _lastZonedTime + 2f) { return; }

            if (!DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            RemainingNCUMessage ncuMessage = (RemainingNCUMessage)msg;
            SettingsController.RemainingNCU[ncuMessage.Character] = ncuMessage.RemainingNCU;
        }
        private void OnPerksMessage(int sender, IPCMessage msg)
        {
            PerkMessage perkMsg = (PerkMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Perks"] = perkMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnBuffingMessage(int sender, IPCMessage msg)
        {
            BuffingMessage buffMsg = (BuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Buffing"] = buffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnCompositesMessage(int sender, IPCMessage msg)
        {
            CompositesMessage compMsg = (CompositesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Composites"] = compMsg.Switch;
            SettingsController.CleanUp();
        }

        private void OnDebuffingMessage(int sender, IPCMessage msg)
        {
            DebuffingMessage debuffMsg = (DebuffingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Debuffing"] = debuffMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnChannelMessage(int sender, IPCMessage msg)
        {
            ChannelMessage channelMsg = (ChannelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(channelMsg.Id);
            Config.Save();

            var window = SettingsController.settingsWindow;
            if (window != null && window.IsValid && window.IsVisible)
            {
                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                Config.Save();
            }

            IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
            Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
            SettingsController.RemainingNCU.Clear();
        }
        private void OnDisbandAllMessage(int sender, IPCMessage msg)
        {
            DisbandAllMessage disbandallMsg = (DisbandAllMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Team.Leave();
        }
        private void OnSicknessMessage(int sender, IPCMessage msg)
        {
            SicknessMessage sicknessMsg = (SicknessMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Sickness"] = sicknessMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnInvitesMessage(int sender, IPCMessage msg)
        {
            InvitesMessage invitesMsg = (InvitesMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Invites"] = invitesMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnTradingMessage(int sender, IPCMessage msg)
        {
            TradingMessage tradingMsg = (TradingMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            _settings[$"Trading"] = tradingMsg.Switch;
            SettingsController.CleanUp();
        }
        private void OnLevelMessage(int sender, IPCMessage msg)
        {
            LevelMessage levelMsg = (LevelMessage)msg;

            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            Config.CharSettings[Game.ClientInst].Level = levelMsg.Level;

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid && window.IsVisible) { levelInput.Text = $"{levelMsg.Level}"; }
            else
            {
                _settings[$"Level"] = levelMsg.Switch;
                Level = levelMsg.Level;
                SettingsController.CleanUp();
            }
        }
        #endregion

        #region Handles
        private void HandleGenericRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new CompositesMessage()
            {
                Switch = _settings["Composites"].AsBool()
            });
            IPCChannel.Broadcast(new BuffingMessage()
            {
                Switch = _settings["Buffing"].AsBool()
            });
            IPCChannel.Broadcast(new PerkMessage()
            {
                Switch = _settings["Perks"].AsBool()
            });
            IPCChannel.Broadcast(new DebuffingMessage()
            {
                Switch = _settings["Debuffing"].AsBool()
            });
            IPCChannel.Broadcast(new SicknessMessage()
            {
                Switch = _settings["Sickness"].AsBool()
            });
            IPCChannel.Broadcast(new InvitesMessage()
            {
                Switch = _settings["Invites"].AsBool()
            });
            IPCChannel.Broadcast(new TradingMessage()
            {
                Switch = _settings["Trading"].AsBool()
            });
            IPCChannel.Broadcast(new LevelMessage()
            {
                Switch = _settings["Level"].AsBool(),
                Level = Level
            });
        }
        private void HandlePetViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_petView)) { return; }

                _petView = View.CreateFromXml(PluginDirectory + "\\UI\\EngineerPetsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Pets", XmlViewName = "EngineerPetsView" }, _petView);

                InitSettings(window);
            }
            else if (_petWindow == null || (_petWindow != null && !_petWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_petWindow, PluginDir, new WindowOptions() { Name = "Pets", XmlViewName = "EngineerPetsView" }, _petView, out var container);
                _petWindow = container;

                InitSettings(container);
            }
        }

        private void HandlePerkViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_perkView)) { return; }

                _perkView = View.CreateFromXml(PluginDirectory + "\\UI\\EngineerPerksView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Perks", XmlViewName = "EngineerPerksView" }, _perkView);

                InitSettings(window);
            }
            else if (_perkWindow == null || (_perkWindow != null && !_perkWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_perkWindow, PluginDir, new WindowOptions() { Name = "Perks", XmlViewName = "EngineerPerksView" }, _perkView, out var container);
                _perkWindow = container;

                InitSettings(container);
            }
        }
        private void HandleBuffViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_buffView)) { return; }

                _buffView = View.CreateFromXml(PluginDirectory + "\\UI\\EngineerBuffsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Buffs", XmlViewName = "EngineerBuffsView" }, _buffView);

                InitSettings(window);
            }
            else if (_buffWindow == null || (_buffWindow != null && !_buffWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_buffWindow, PluginDir, new WindowOptions() { Name = "Buffs", XmlViewName = "EngineerBuffsView" }, _buffView, out var container);
                _buffWindow = container;

                InitSettings(container);
            }
        }

        private void HandleItemViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_itemView)) { return; }

                _itemView = View.CreateFromXml(PluginDirectory + "\\UI\\EngineerItemsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Items", XmlViewName = "EngineerItemsView" }, _itemView);

                InitSettings(window);
            }
            else if (_itemWindow == null || (_itemWindow != null && !_itemWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_itemWindow, PluginDir, new WindowOptions() { Name = "Items", XmlViewName = "EngineerItemsView" }, _itemView, out var container);
                _itemWindow = container;

                InitSettings(container);
            }
        }
        private void HandleGenericViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_genericView)) { return; }

                _genericView = View.CreateFromXml(PluginDirectory + "\\UI\\EngineerGenericView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Generic", XmlViewName = "EngineerGenericView" }, _genericView);

                InitSettings(window);
            }
            else if (_genericWindow == null || (_genericWindow != null && !_genericWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_genericWindow, PluginDir, new WindowOptions() { Name = "Generic", XmlViewName = "EngineerGenericView" }, _genericView, out var container);
                _genericWindow = container;

                InitSettings(container);
            }
        }
        private void HandleProcViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_procView)) { return; }

                _procView = View.CreateFromXml(PluginDirectory + "\\UI\\EngineerProcsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Procs", XmlViewName = "EngineerProcsView" }, _procView);

                InitSettings(window);
            }
            else if (_procWindow == null || (_procWindow != null && !_procWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_procWindow, PluginDir, new WindowOptions() { Name = "Procs", XmlViewName = "EngineerProcsView" }, _procView, out var container);
                _procWindow = container;

                InitSettings(container);
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            #region Pets

            if (Time.NormalTime > _lastPetSyncTime + 0.9f)
            {
                if (DynelManager.LocalPlayer.Pets.Count() >= 1 && CanLookupPetsAfterZone())
                {
                    if (_settings["SyncPets"].AsBool())
                    {
                        foreach (Pet _pet in DynelManager.LocalPlayer.Pets.Where(c => c.Type == PetType.Attack || c.Type == PetType.Support))
                        {
                            if (!DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending
                                && (bool)_pet?.Character.IsAttacking) { _pet?.Follow(); }

                            if (DynelManager.LocalPlayer.IsAttacking)
                            {
                                if (_pet?.Character.IsAttacking == false)
                                    _pet?.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                                else if (_pet?.Character.IsAttacking == true && _pet?.Character.FightingTarget != null
                                    && _pet?.Character.FightingTarget.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                                    _pet?.Attack(DynelManager.LocalPlayer.FightingTarget.Identity);
                            }
                        }
                    }
                }

                _lastPetSyncTime = Time.NormalTime;
            }

            #endregion

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                RemainingNCUMessage ncuMessage = RemainingNCUMessage.ForLocalPlayer();

                IPCChannel.Broadcast(ncuMessage);

                SettingsController.RemainingNCU[DynelManager.LocalPlayer.Identity] = DynelManager.LocalPlayer.RemainingNCU;


                base.OnUpdate(deltaTime);
                _mainUpdate = Time.NormalTime;
            }

            

            #region UI Update

            var window = SettingsController.FindValidWindow(_windows);

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (window != null && window.IsValid && window.IsVisible)
            {
                if (window.FindView("GenericRelay", out Button genericRelay))
                {
                    genericRelay.Tag = window;
                    genericRelay.Clicked = HandleGenericRelayClick;
                }

                if (levelInput != null && !string.IsNullOrEmpty(levelInput.Text))
                    if (int.TryParse(levelInput.Text, out int levelValue))
                        if (Config.CharSettings[Game.ClientInst].Level != levelValue)
                            Config.CharSettings[Game.ClientInst].Level = levelValue;
                if (fountainOfLifeInput != null && !string.IsNullOrEmpty(fountainOfLifeInput.Text))
                    if (int.TryParse(fountainOfLifeInput.Text, out int fountainOfLifeValue))
                        if (Config.CharSettings[Game.ClientInst].FountainOfLifePercentage != fountainOfLifeValue)
                            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = fountainOfLifeValue;
                if (bioCocoonInput != null && !string.IsNullOrEmpty(bioCocoonInput.Text))
                    if (int.TryParse(bioCocoonInput.Text, out int bioCocoonValue))
                        if (Config.CharSettings[Game.ClientInst].BioCocoonPercentage != bioCocoonValue)
                            Config.CharSettings[Game.ClientInst].BioCocoonPercentage = bioCocoonValue;
                if (stimTargetInput != null && !string.IsNullOrEmpty(stimTargetInput.Text))
                    if (Config.CharSettings[Game.ClientInst].StimTargetName != stimTargetInput.Text)
                        Config.CharSettings[Game.ClientInst].StimTargetName = stimTargetInput.Text;
                if (stimHealthInput != null && !string.IsNullOrEmpty(stimHealthInput.Text))
                    if (int.TryParse(stimHealthInput.Text, out int stimHealthValue))
                        if (Config.CharSettings[Game.ClientInst].StimHealthPercentage != stimHealthValue)
                            Config.CharSettings[Game.ClientInst].StimHealthPercentage = stimHealthValue;
                if (stimNanoInput != null && !string.IsNullOrEmpty(stimNanoInput.Text))
                    if (int.TryParse(stimNanoInput.Text, out int stimNanoValue))
                        if (Config.CharSettings[Game.ClientInst].StimNanoPercentage != stimNanoValue)
                            Config.CharSettings[Game.ClientInst].StimNanoPercentage = stimNanoValue;
                if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        if (Config.CharSettings[Game.ClientInst].KitHealthPercentage != kitHealthValue)
                            Config.CharSettings[Game.ClientInst].KitHealthPercentage = kitHealthValue;
                if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        if (Config.CharSettings[Game.ClientInst].KitNanoPercentage != kitNanoValue)
                            Config.CharSettings[Game.ClientInst].KitNanoPercentage = kitNanoValue;
                if (cleanseInput != null && !string.IsNullOrEmpty(cleanseInput.Text))
                    if (int.TryParse(cleanseInput.Text, out int cleanseValue))
                        if (Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay != cleanseValue)
                            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = cleanseValue;
                if (survivalInput != null && !string.IsNullOrEmpty(survivalInput.Text))
                    if (int.TryParse(survivalInput.Text, out int survivalValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay != survivalValue)
                            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = survivalValue;
                if (sphereInput != null && !string.IsNullOrEmpty(sphereInput.Text))
                    if (int.TryParse(sphereInput.Text, out int sphereValue))
                        if (Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay != sphereValue)
                            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = sphereValue;
                if (witOfTheAtroxInput != null && !string.IsNullOrEmpty(witOfTheAtroxInput.Text))
                    if (int.TryParse(witOfTheAtroxInput.Text, out int witOfTheAtroxValue))
                        if (Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay != witOfTheAtroxValue)
                            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = witOfTheAtroxValue;
                if (selfHealInput != null && !string.IsNullOrEmpty(selfHealInput.Text))
                    if (int.TryParse(selfHealInput.Text, out int selfHealValue))
                        if (Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage != selfHealValue)
                            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = selfHealValue;
                if (selfNanoInput != null && !string.IsNullOrEmpty(selfNanoInput.Text))
                    if (int.TryParse(selfNanoInput.Text, out int selfNanoValue))
                        if (Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage != selfNanoValue)
                            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = selfNanoValue;
                if (teamHealInput != null && !string.IsNullOrEmpty(teamHealInput.Text))
                    if (int.TryParse(teamHealInput.Text, out int teamHealValue))
                        if (Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage != teamHealValue)
                            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = teamHealValue;
                if (teamNanoInput != null && !string.IsNullOrEmpty(teamNanoInput.Text))
                    if (int.TryParse(teamNanoInput.Text, out int teamNanoValue))
                        if (Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage != teamNanoValue)
                            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = teamNanoValue;
                if (bodyDevInput != null && !string.IsNullOrEmpty(bodyDevInput.Text))
                    if (int.TryParse(bodyDevInput.Text, out int bodyDevValue))
                        if (Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage != bodyDevValue)
                            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = bodyDevValue;
                if (strengthInput != null && !string.IsNullOrEmpty(strengthInput.Text))
                    if (int.TryParse(strengthInput.Text, out int strengthValue))
                        if (Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage != strengthValue)
                            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = strengthValue;
                if (bioRegrowthPercentageInput != null && !string.IsNullOrEmpty(bioRegrowthPercentageInput.Text))
                    if (int.TryParse(bioRegrowthPercentageInput.Text, out int bioRegrowthPercentageValue))
                        if (Config.CharSettings[Game.ClientInst].BioRegrowthPercentage != bioRegrowthPercentageValue)
                            Config.CharSettings[Game.ClientInst].BioRegrowthPercentage = bioRegrowthPercentageValue;
                if (bioRegrowthDelayInput != null && !string.IsNullOrEmpty(bioRegrowthDelayInput.Text))
                    if (int.TryParse(bioRegrowthDelayInput.Text, out int bioRegrowthDelayValue))
                        if (Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelay != bioRegrowthDelayValue)
                            Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelay = bioRegrowthDelayValue;

                SettingsController.CleanUp();
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid && SettingsController.settingsWindow.IsVisible)
            {
                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("ItemsView", out Button itemView))
                {
                    itemView.Tag = SettingsController.settingsWindow;
                    itemView.Clicked = HandleItemViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PerksView", out Button perkView))
                {
                    perkView.Tag = SettingsController.settingsWindow;
                    perkView.Clicked = HandlePerkViewClick;
                }

                if (SettingsController.settingsWindow.FindView("PetsView", out Button petView))
                {
                    petView.Tag = SettingsController.settingsWindow;
                    petView.Clicked = HandlePetViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffsView", out Button buffView))
                {
                    buffView.Tag = SettingsController.settingsWindow;
                    buffView.Clicked = HandleBuffViewClick;
                }

                if (SettingsController.settingsWindow.FindView("ProcsView", out Button procView))
                {
                    procView.Tag = SettingsController.settingsWindow;
                    procView.Clicked = HandleProcViewClick;
                }

                if (SettingsController.settingsWindow.FindView("GenericView", out Button genericView))
                {
                    genericView.Tag = SettingsController.settingsWindow;
                    genericView.Clicked = HandleGenericViewClick;
                }

                SettingsController.CleanUp();
            }

            #endregion
        }

        #region LE Procs

        private bool CushionBlows(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.CushionBlows != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool DestructiveSignal(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.DestructiveSignal != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool DestructiveTheorem(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.DestructiveTheorem != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool EndureBarrage(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.EndureBarrage != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool EnergyTransfer(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.EnergyTransfer != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool ReactiveArmor(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.ReactiveArmor != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool SplinterPreservation(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType1Selection.SplinterPreservation != (ProcType1Selection)_settings["ProcType1Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }


        private bool AssaultForceRelief(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.AssaultForceRelief != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool CongenialEncasement(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.CongenialEncasement != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool DroneExplosives(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.DroneExplosives != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        private bool DroneMissiles(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.DroneMissiles != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }
        private bool PersonalProtection(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ProcType2Selection.PersonalProtection != (ProcType2Selection)_settings["ProcType2Selection"].AsInt32()) { return false; }

            return LEProc(perk, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Buffs

        #region Auras

        private bool ArmorAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (BuffingAuraSelection.Armor != (BuffingAuraSelection)_settings["BuffingAuraSelection"].AsInt32())
            {
                CancelBuffs(RelevantNanos.ArmorAura);
                return false;
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool DamageAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (BuffingAuraSelection.Damage != (BuffingAuraSelection)_settings["BuffingAuraSelection"].AsInt32())
            {
                CancelBuffs(RelevantNanos.DamageAura);
                return false;
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool ReflectAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (BuffingAuraSelection.Reflect != (BuffingAuraSelection)_settings["BuffingAuraSelection"].AsInt32())
            {
                CancelBuffs(RelevantNanos.ReflectAura);
                return false;
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool ShieldAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (BuffingAuraSelection.Shield != (BuffingAuraSelection)_settings["BuffingAuraSelection"].AsInt32())
            {
                CancelBuffs(RelevantNanos.ShieldAura);
                return false;
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }
        private bool SnareAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || DebuffingAuraSelection.PetSnare != (DebuffingAuraSelection)_settings["DebuffingAuraSelection"].AsInt32()) { return false; }

            if (SnareMobExists() && CanCast(spell))
            {
                Pet _pet = DynelManager.LocalPlayer.Pets
                    .Where(c => c.Character != null && c.Character?.Health > 0)
                    .FirstOrDefault();

                if (_pet != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _pet.Character;
                    return true;
                }
            }

            return PetTargetBuff(spell, fightingTarget, ref actionTarget);
        }

        private bool ShieldRipperAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DebuffingAuraSelection.ShieldRipper != (DebuffingAuraSelection)_settings["DebuffingAuraSelection"].AsInt32()
                || (DebuffingAuraSelection.ShieldRipper == (DebuffingAuraSelection)_settings["DebuffingAuraSelection"].AsInt32()
                    && (!InCombatTeam()
                        || !DynelManager.Characters.Any(c => c.Health > 0
                            && !c.IsPlayer
                            && !c.IsPet
                            && c.FightingTarget != null
                            && c.FightingTarget?.IsPet == false
                            && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 9f))))
            {
                CancelBuffs(RelevantNanos.ShieldRipper);
                return false;
            }

            if (DynelManager.Characters.Any(c => c.Health > 0
                && !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                && !c.IsPlayer
                && !c.IsPet
                && !c.Buffs.Contains(RelevantNanos.ShieldRipper)
                && c.FightingTarget != null
                && c.FightingTarget?.IsPet == false
                && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 9f)) 
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        private bool BlindAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell)) { return false; }

            if (DebuffingAuraSelection.Blind != (DebuffingAuraSelection)_settings["DebuffingAuraSelection"].AsInt32()
                || (DebuffingAuraSelection.Blind == (DebuffingAuraSelection)_settings["DebuffingAuraSelection"].AsInt32()
                    && !InCombatTeam()
                    && !DynelManager.Characters.Any(c => c.Health > 0
                        && c.Identity != DynelManager.LocalPlayer.Identity
                        && c.IsValid
                        && !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && !c.IsPlayer
                        && !c.IsPet
                        && !c.Buffs.Contains(RelevantNanos.Blind)
                        && c.FightingTarget != null
                        && c.FightingTarget?.IsPet == false
                        && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 9f)))
            {
                CancelBuffs(RelevantNanos.Blind);
                return false;
            }

            if (DebuffingAuraSelection.Blind == (DebuffingAuraSelection)_settings["DebuffingAuraSelection"].AsInt32())
            {
                if (!DynelManager.Characters.Any(c => c.Health > 0
                    && c.Identity != DynelManager.LocalPlayer.Identity
                    && c.IsValid
                    && !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                    && !c.IsPlayer
                    && !c.IsPet
                    && !c.Buffs.Contains(RelevantNanos.Blind)
                    && c.FightingTarget != null
                    && c.FightingTarget?.IsPet == false
                    && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 9f))
                {
                    return false;
                }

                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        #endregion

        protected bool PistolTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["PistolTeam"].AsBool())
                return TeamBuffExclusionWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);

            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
        }
        protected bool GrenadeTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["GrenadeTeam"].AsBool())
                return TeamBuffExclusionWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Grenade)
                        || TeamBuffExclusionWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);

            return BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Grenade)
                    || BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Pistol);
        }

        protected bool ShadowlandReflect(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ReflectSelection.Shadowlands == (ReflectSelection)_settings["ReflectSelection"].AsInt32())
                return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return false;
        }
        protected bool DamageTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["DamageTeam"].AsBool())
                return AllTeamBuff(spell, fightingTarget, ref actionTarget);

            return AllBuff(spell, fightingTarget, ref actionTarget);
        }

        private bool RKReflects(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (ReflectSelection.Self == (ReflectSelection)_settings["ReflectSelection"].AsInt32())
                return AllBuff(spell, fightingTarget, ref actionTarget);
            else if (ReflectSelection.Team == (ReflectSelection)_settings["ReflectSelection"].AsInt32())
                return AllTeamBuff(spell, fightingTarget, ref actionTarget);

            return false;
        }

        protected bool DamageShieldTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["DamageShieldTeam"].AsBool())
                return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected bool ArmorTeam(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["ArmorTeam"].AsBool())
                return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected bool InitBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() 
                || InitBuffSelection.None == (InitBuffSelection)_settings["InitBuffSelection"].AsInt32()) { return false; }

            SimpleChar target = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0
                    && c.Profession != Profession.Doctor && c.Profession != Profession.NanoTechnician
                    && OtherSpellChecks(spell, spell.Nanoline, c))
                .FirstOrDefault();

            if (target != null && GetWieldedWeapons(target).HasFlag(CharacterWieldedWeapon.Ranged))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = target;
                return true;
            }

            return false;
        }

        #endregion

        #region Pets

        #region Healing

        private bool PetHealing(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["HealPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && c.Character?.HealthPercent <= 90);

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        private bool PetHealingGreater(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["HealPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && c.Character?.HealthPercent <= 90);

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        #endregion

        #region NCU Upgrade

        protected bool AndroidNCUUpgrade(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["NCUUpgrade"].AsBool() || !CanLookupPetsAfterZone() || !NCUUpgradeDelay() 
                || (petNCUUpgraded[PetType.Attack] && petNCUUpgraded[PetType.Support])) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets.FirstOrDefault(c => c.Character != null);

            if (_pet != null && CanNCUUpgrade(_pet))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                petNCUUpgraded[_pet.Type] = true;
                _lastNCUUpgradeTime = Time.NormalTime;
                return true;
            }

            return false;
        }

        #endregion

        #region Trimmers

        protected bool PetHpTrimmer(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["HpTrimmer"].AsBool() || !CanLookupPetsAfterZone() || !TrimDelay() 
                || petTrimmedHp[PetType.Support]) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && c.Type == PetType.Support && CanHpTrim(c));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                petTrimmedHp[PetType.Support] = true;
                _lastPetTrimHpTime[PetType.Support] = Time.NormalTime;
                _lastTrimTime = Time.NormalTime;
                return true;
            }

            return false;
        }

        protected bool PetOffTrimmer(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["OffTrimmer"].AsBool() || !CanLookupPetsAfterZone() || !TrimDelay()) { return false; }

            if (_settings["HpTrimmer"].AsBool())
            {
                if (!petTrimmedOff[PetType.Attack]) { return false; }

                Pet _pet = DynelManager.LocalPlayer.Pets
                    .FirstOrDefault(c => c.Character != null && c.Type == PetType.Attack && CanOffTrim(c));

                if (_pet != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _pet.Character;
                    petTrimmedOff[PetType.Attack] = true;
                    _lastPetTrimOffTime[PetType.Attack] = Time.NormalTime;
                    _lastTrimTime = Time.NormalTime;
                    return true;
                }
            }
            else if (!petTrimmedHp[PetType.Attack] && !petTrimmedHp[PetType.Support])
            {
                Pet _pet = DynelManager.LocalPlayer.Pets
                    .FirstOrDefault(c => c.Character != null && CanHpTrim(c));

                if (_pet?.Type == PetType.Attack)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _pet.Character;
                    petTrimmedHp[PetType.Attack] = true;
                    _lastPetTrimHpTime[PetType.Attack] = Time.NormalTime;
                    _lastTrimTime = Time.NormalTime;
                    return true;
                }
                else if (_pet?.Type == PetType.Support)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _pet.Character;
                    petTrimmedHp[PetType.Support] = true;
                    _lastPetTrimHpTime[PetType.Support] = Time.NormalTime;
                    _lastTrimTime = Time.NormalTime;
                    return true;
                }
            }

            return false;
        }

        protected bool PetAggDefTrimmer(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["AggDefTrimmer"].AsBool() || !CanLookupPetsAfterZone() || !TrimDelay()
                || (petTrimmedAggDef[PetType.Attack] && petTrimmedAggDef[PetType.Support])) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && CanAggDefTrim(c));

            if (_pet?.Type == PetType.Attack)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                petTrimmedAggDef[PetType.Attack] = true;
                _lastTrimTime = Time.NormalTime;
                return true;
            }
            else if (_pet?.Type == PetType.Support)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                petTrimmedAggDef[PetType.Support] = true;
                _lastTrimTime = Time.NormalTime;
                return true;
            }

            return false;
        }

        protected bool PetAggressiveTrimmer(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["TauntTrimmer"].AsBool() || !CanLookupPetsAfterZone() || !TrimDelay()
                || petTrimmedTaunt) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                    .FirstOrDefault(c => c.Character != null && c.Type == PetType.Attack && CanTauntTrim());

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                petTrimmedTaunt = true;
                _lastTrimTime = Time.NormalTime;
                return true;
            }

            return false;
        }

        #endregion

        #region Perks

        private bool SiphonBox(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PetPerkSelection.SiphonBox != (PetPerkSelection)_settings["PetPerkSelection"].AsInt32()
                || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                    .FirstOrDefault(c => c.Character != null && (bool)!c.Character?.Buffs.Contains(RelevantNanos.PerkSiphonBox));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        private bool ChaoticBox(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PetPerkSelection.ChaoticBox != (PetPerkSelection)_settings["PetPerkSelection"].AsInt32()
                || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                    .FirstOrDefault(c => c.Character != null && (bool)!c.Character?.Buffs.Contains(RelevantNanos.PerkChaoticBox));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        private bool TauntBox(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PetPerkSelection.TauntBox != (PetPerkSelection)_settings["PetPerkSelection"].AsInt32()
                || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                    .FirstOrDefault(c => c.Character != null && (bool)!c.Character?.Buffs.Contains(RelevantNanos.PerkTauntBox));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        #endregion

        #region Buffs

        protected bool ShieldOfTheObedientServant(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["BuffPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && (bool)!c.Character?.Buffs.Contains(NanoLine.ShieldoftheObedientServant));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        private bool MastersBidding(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PetProcSelection.MastersBidding != (PetProcSelection)_settings["PetProcSelection"].AsInt32()) { return false; }

            if (!_settings["BuffPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && (bool)!c.Character?.Buffs.Contains(NanoLine.SiphonBox683)
                        && (c.Type == PetType.Attack || c.Type == PetType.Support));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        private bool SedativeInjectors(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PetProcSelection.SedativeInjectors != (PetProcSelection)_settings["PetProcSelection"].AsInt32()) { return false; }

            if (!_settings["BuffPets"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character != null && (bool)!c.Character?.Buffs.Contains(NanoLine.SiphonBox683)
                        && (c.Type == PetType.Attack || c.Type == PetType.Support));

            if (_pet != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Misc

        protected bool AttackPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["SummonPets"].AsBool()) { return false; }

            if (AttackPetSpawn(spell, fightingTarget, ref actionTarget))
            {
                ResetTrimmers();

                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }

            return false;
        }

        protected bool SupportPetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["SummonPets"].AsBool()) { return false; }

            if (SupportPetSpawn(spell, fightingTarget, ref actionTarget))
            {
                ResetTrimmers();

                if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 2)
                    return true;
            }

            return false;
        }

        private bool SnareMobExists()
        {
            return DynelManager.NPCs
                .Where(c => !c.Buffs.Contains(NanoLine.AOESnare)
                    && !c.Buffs.Contains(NanoLine.EngineerPetAOESnareBuff)
                    && !c.Buffs.Contains(NanoLine.Snare)
                    && (c.Name == "Flaming Vengeance" ||
                        c.Name == "Hand of the Colonel"))
                .Any();
        }

        protected bool NCUUpgradeDelay()
        {
            return Time.NormalTime > _lastNCUUpgradeTime + DelayBetweenNCUUpgrades;
        }

        protected bool CanNCUUpgrade(Pet pet)
        {
            if (pet?.Character?.GetStat(Stat.QuantumFT) > 1)
                return !petNCUUpgraded[pet.Type];

            return false;
        }

        protected bool TrimDelay()
        {
            return Time.NormalTime > _lastTrimTime + DelayBetweenTrims;
        }

        protected bool CanOffTrim(Pet pet)
        {
            return Time.NormalTime > _lastPetTrimOffTime[pet.Type] + DelayBetweenDivertTrims;
        }

        protected bool CanHpTrim(Pet pet)
        {
            return (Time.NormalTime > _lastPetTrimHpTime[pet.Type] + DelayBetweenDivertTrims) || !petTrimmedHp[pet.Type];
        }


        protected bool CanAggDefTrim(Pet pet)
        {
            return !petTrimmedAggDef[pet.Type];
        }

        protected bool CanTauntTrim()
        {
            return !petTrimmedTaunt;
        }

        public void ResetTrimmers()
        {
            petTrimmedTaunt = false;
            petTrimmedOff[PetType.Attack] = false;
            petTrimmedOff[PetType.Support] = false;
            petTrimmedHp[PetType.Attack] = false;
            petTrimmedHp[PetType.Support] = false;
            petTrimmedAggDef[PetType.Attack] = false;
            petTrimmedAggDef[PetType.Support] = false;
        }

        public void InitNCUUpgrade()
        {
            petNCUUpgraded[PetType.Attack] = false;
            petNCUUpgraded[PetType.Support] = false;
        }
        public static void DisbandAllCommand(string command, string[] param, ChatWindow chatWindow)
        {
            IPCChannel.Broadcast(new DisbandAllMessage());
        }
        public static void ChannelCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length > 0)
            {
                if (param.Length > 1 && param[0].ToLower() == "all")
                {
                    if (param[1] != null && Convert.ToByte(param[1]) > 0)
                    {
                        IPCChannel.Broadcast(new ChannelMessage()
                        {
                            Id = Convert.ToByte(param[1])
                        });

                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[1]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
                else if (param.Length == 1 && Convert.ToByte(param[0]) > 0)
                {
                    if (param[0] != null && Convert.ToByte(param[0]) > 0)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToInt32(param[0]);
                        Config.Save();

                        var window = SettingsController.settingsWindow;
                        if (window != null && window.IsValid && window.IsVisible)
                        {
                            channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
                            Config.Save();
                        }

                        IPCChannel.SetChannelId(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                        Chat.WriteLine($"Handler Channel : {Config.CharSettings[Game.ClientInst].IPCChannel}");
                        SettingsController.RemainingNCU.Clear();
                    }
                }
            }
        }

        private void OnZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;

            ResetTrimmers();
        }

        private static class RelevantNanos
        {
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int MastersBidding = 268171;
            public const int SedativeInjectors = 302254;
            public const int CompositeUtility = 287046;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpec = 223364;
            public const int SympatheticReactiveCocoon = 154550;
            public const int IntrusiveAuraCancellation = 204372;
            public const int BoostedTendons = 269463;
            public const int PetHealingGreater = 270351;
            public const int PetWarp = 209488;

            public static readonly Spell[] DamageBuffLineA = Spell.GetSpellsForNanoline(NanoLine.DamageBuffs_LineA)
                .Where(spell => spell.Id != RelevantNanos.BoostedTendons).OrderByStackingOrder().ToArray();

            public static readonly int[] PerkTauntBox = { 229131, 229130, 229129, 229128, 229127, 229126 };
            public static readonly int[] PerkSiphonBox = { 229657, 229656, 229655, 229654 };
            public static readonly int[] PerkChaoticBox = { 227787 };
            public static readonly int[] PetCleanse = { 269870, 269869 };
            public static readonly int[] CompositeTradeskills = { 99999, 999999 };
            public static readonly int[] ShieldRipper = { 154725, 154726, 154727, 154728 };
            public static readonly int[] Blind = { 154715, 154716, 154717, 154718, 154719 };
            public static readonly int[] ShieldAura = { 154550, 154551, 154552, 154553 };
            public static readonly int[] DamageAura = { 154560, 154561 };
            public static readonly int[] ArmorAura = { 154562, 154563, 154564, 154565, 154566, 154567 };
            public static readonly int[] ReflectAura = { 154557, 154558, 154559 };
            public static readonly int[] PetHealing = { 116791, 116795, 116796, 116792, 116797, 116794, 116793 };
            public static readonly int[] ShieldOfObedientServant = { 270790, 202260 };
        }
        private static class RelevantItems
        {
            //public static readonly int[] AndroidNCUUpgrades = { 269840, 150307, 150306 };
            public const int AndroidNCUUpgradeLow = 150306;
            public const int AndroidNCUUpgradeHigh = 150307;
            public const int AndroidNCUInjector = 269840;
        }

        private static class RelevantTrimmers
        {
            public const int IncreaseAggressivenessLow = 154939;
            public const int IncreaseAggressivenessHigh = 154940;
            public const int DivertEnergyToOffenseLow = 88377;
            public const int DivertEnergyToOffenseHigh = 88378;
            public const int PositiveAggressiveDefensive = 88384;
            public const int DivertEnergyToHitpointsLow = 88381;
            public const int DivertEnergyToHitpointsHigh = 88382;
        }

        public enum PetPerkSelection
        {
            TauntBox, ChaoticBox, SiphonBox
        }
        public enum PetProcSelection
        {
            None, MastersBidding, SedativeInjectors
        }

        public enum BuffingAuraSelection
        {
            None, Armor, Reflect, Damage, Shield
        }
        public enum DebuffingAuraSelection
        {
            None, Blind, PetSnare, ShieldRipper
        }
        public enum ProcType1Selection
        {
            ReactiveArmor, DestructiveTheorem, EnergyTransfer, EndureBarrage, DestructiveSignal, SplinterPreservation, CushionBlows, None
        }
        public enum ProcType2Selection
        {
            AssaultForceRelief, DroneMissiles, DroneExplosives, CongenialEncasement, PersonalProtection, None
        }
        public enum InitBuffSelection
        {
            None, Self, Team
        }
        public enum ReflectSelection
        {
            None, Self, Team, Shadowlands
        }

        #endregion
    }
}
