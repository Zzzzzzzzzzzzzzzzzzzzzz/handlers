﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Handler.Engi
{
    public enum IPCOpcode
    {
        RemainingNCU = 2000,
        Buffing = 2001,
        Composites = 2002,
        Debuffing = 2003,
        Perks = 2004,
        Sickness = 2005,
        Invites = 2006,
        Level = 2007,
        Trading = 2008,
        Channel = 2009,
        DisbandAll = 2010
    }
}
