﻿using AOSharp.Core;
using System;
using AOSharp.Core.UI;
using AOSharp.Core.Combat;

namespace Handler.Shade
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Shade Handler Loaded! Version: 0.9.9.96");
                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new ShadeHandler(pluginDir));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
