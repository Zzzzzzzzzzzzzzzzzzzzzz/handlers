﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace Handler.Adv
{
    [AoContract((int)IPCOpcode.Buffing)]
    public class BuffingMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Buffing;

        [AoMember(0)]
        public bool Switch { get; set; }
    }
}
