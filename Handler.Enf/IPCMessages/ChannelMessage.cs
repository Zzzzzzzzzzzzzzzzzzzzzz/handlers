﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace Handler.Enf
{
    [AoContract((int)IPCOpcode.Channel)]
    public class ChannelMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Channel;

        [AoMember(0)]
        public int Id { get; set; }
    }
}
