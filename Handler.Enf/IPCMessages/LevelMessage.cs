﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace Handler.Enf
{
    [AoContract((int)IPCOpcode.Level)]
    public class LevelMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Level;

        [AoMember(0)]
        public bool Switch { get; set; }

        [AoMember(1)]
        public int Level { get; set; }
    }
}
