﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using static Handler.Generic.GenericHandler;

namespace Handler.Generic
{
    public static class Extensions
    {
        public static void AddRandomness(this ref Vector3 pos, int entropy)
        {
            pos.X += Next(-entropy, entropy);
            pos.Z += Next(-entropy, entropy);
        }

        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }
        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }
        public static void InitSettings(Window _window)
        {
            _window.FindView("MirrorShieldPercentageBox", out mirrorShieldInput);
            _window.FindView("LevelBox", out levelInput);
            _window.FindView("FountainOfLifePercentageBox", out fountainOfLifeInput);
            _window.FindView("StimTargetBox", out stimTargetInput);
            _window.FindView("StimHealthPercentageBox", out stimHealthInput);
            _window.FindView("StimNanoPercentageBox", out stimNanoInput);
            _window.FindView("KitHealthPercentageBox", out kitHealthInput);
            _window.FindView("KitNanoPercentageBox", out kitNanoInput);
            _window.FindView("AreaDebuffRangeBox", out areaDebuffRangeInput);
            _window.FindView("CalmRangeBox", out calmRangeInput);
            _window.FindView("FalseProfsDoctorRangeBox", out falseProfsDoctorAreaRangeInput);
            _window.FindView("FalseProfsTraderRangeBox", out falseProfsTraderAreaRangeInput);
            _window.FindView("AbsorbsDelayBox", out absorbsInput);
            _window.FindView("CleanseDelayBox", out cleanseInput);
            _window.FindView("SphereDelayBox", out sphereInput);
            _window.FindView("WitDelayBox", out witOfTheAtroxInput);
            _window.FindView("SurvivalDelayBox", out survivalInput);
            _window.FindView("SelfHealPercentageBox", out selfHealInput);
            _window.FindView("SelfNanoPercentageBox", out selfNanoInput);
            _window.FindView("TeamHealPercentageBox", out teamHealInput);
            _window.FindView("TeamNanoPercentageBox", out teamNanoInput);
            _window.FindView("BodyDevAbsorbsItemPercentageBox", out bodyDevInput);
            _window.FindView("StrengthAbsorbsItemPercentageBox", out strengthInput);
            _window.FindView("NanoAegisPercentageBox", out nanoAegisInput);
            _window.FindView("NullitySpherePercentageBox", out nullSphereInput);
            _window.FindView("IzgimmersWealthPercentageBox", out izWealthInput);
            _window.FindView("XpPerksDelayBox", out xpPerksInput);
            _window.FindView("HealPercentageBox", out healInput);
            _window.FindView("HealthDrainPercentageBox", out healthDrainInput);
            _window.FindView("CompleteHealPercentageBox", out completeHealInput);
            _window.FindView("HealingBookItemPercentageBox", out healingBookInput);
            _window.FindView("BattleGroupHeal1PercentageBox", out bg1Input);
            _window.FindView("BattleGroupHeal2PercentageBox", out bg2Input);
            _window.FindView("BattleGroupHeal3PercentageBox", out bg3Input);
            _window.FindView("BattleGroupHeal4PercentageBox", out bg4Input);
            _window.FindView("DuckAbsorbsItemPercentageBox", out duckInput);
            _window.FindView("BioRegrowthPercentageBox", out bioRegrowthPercentageInput);
            _window.FindView("BioRegrowthDelayBox", out bioRegrowthDelayInput);
            _window.FindView("BioCocoonPercentageBox", out bioCocoonInput);
            _window.FindView("SingleTauntDelayBox", out singleInput);
            _window.FindView("MongoDelayBox", out mongoInput);

            InitSetting(falseProfsTraderAreaRangeInput, FalseProfsTraderAreaRange);
            InitSetting(falseProfsDoctorAreaRangeInput, FalseProfsDoctorAreaRange);
            InitSetting(mirrorShieldInput, MirrorShieldPercentage);
            InitSetting(levelInput, Level);
            InitSetting(fountainOfLifeInput, FountainOfLifePercentage);
            InitSetting(stimTargetInput, StimTargetName);
            InitSetting(stimHealthInput, StimHealthPercentage);
            InitSetting(stimNanoInput, StimNanoPercentage);
            InitSetting(kitHealthInput, KitHealthPercentage);
            InitSetting(kitNanoInput, KitNanoPercentage);
            InitSetting(areaDebuffRangeInput, AreaDebuffRange);
            InitSetting(calmRangeInput, CalmRange);
            InitSetting(absorbsInput, CycleAbsorbsDelay);
            InitSetting(cleanseInput, CycleCleansePerksDelay);
            InitSetting(survivalInput, CycleSurvivalPerkDelay);
            InitSetting(sphereInput, CycleSpherePerkDelay);
            InitSetting(witOfTheAtroxInput, CycleWitOfTheAtroxPerkDelay);
            InitSetting(selfHealInput, SelfHealPerkPercentage);
            InitSetting(selfNanoInput, SelfNanoPerkPercentage);
            InitSetting(teamHealInput, TeamHealPerkPercentage);
            InitSetting(teamNanoInput, TeamNanoPerkPercentage);
            InitSetting(bodyDevInput, BodyDevAbsorbsItemPercentage);
            InitSetting(strengthInput, StrengthAbsorbsItemPercentage);
            InitSetting(nanoAegisInput, NanoAegisPercentage);
            InitSetting(nullSphereInput, NullitySpherePercentage);
            InitSetting(izWealthInput, IzgimmersWealthPercentage);
            InitSetting(xpPerksInput, CycleXpPerksDelay);
            InitSetting(healInput, HealPercentage);
            InitSetting(healthDrainInput, HealthDrainPercentage);
            InitSetting(completeHealInput, CompleteHealPercentage);
            InitSetting(healingBookInput, HealingBookItemPercentage);
            InitSetting(bg1Input, BattleGroupHeal1Percentage);
            InitSetting(bg2Input, BattleGroupHeal2Percentage);
            InitSetting(bg3Input, BattleGroupHeal3Percentage);
            InitSetting(bg4Input, BattleGroupHeal4Percentage);
            InitSetting(duckInput, DuckAbsorbsItemPercentage);
            InitSetting(bioRegrowthPercentageInput, BioCocoonPercentage);
            InitSetting(bioRegrowthDelayInput, CycleBioRegrowthPerkDelay);
            InitSetting(bioCocoonInput, BioCocoonPercentage);
            InitSetting(singleInput, SingleTauntDelay);
            InitSetting(mongoInput, MongoDelay);
        }

        public static bool InYalm()
        {
            return DynelManager.LocalPlayer.Buffs.Any(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"))
                || Inventory.Items.Any(c => c.Name.Contains("Yalm") && c.Slot.Instance == 0x01);
        }

        public static void InitSetting(TextInputView _textInputView, string _default)
        {
            if (_textInputView == null) { return; }

            _textInputView.Text = _default;
        }
        public static void InitSetting(TextInputView _textInputView, float _default)
        {
            if (_textInputView == null) { return; }

            _textInputView.Text = $"{_default}";
        }

        public static void InitSetting(TextInputView _textInputView, int _default)
        {
            if (_textInputView == null) { return; }

            _textInputView.Text = $"{_default}";
        }

        public static bool IsCasting()
        {
            if (_casting)
            {
                if (Time.NormalTime > _castTimer + _castRecharge)
                {
                    _casting = false;
                }
                else { return true; }
            }

            return false;
        }

        public static bool OtherExceptions(SimpleChar target, Spell spell, NanoLine nanoline)
        {
            if (nanoline == NanoLine.StrengthBuff && target.Buffs.Contains(NanoLine.KeeperStr_Stam_AgiBuff)) { return true; }

            if (spell.Nanoline == NanoLine.CriticalIncreaseBuff)
            {
                foreach (int id in RelevantGenericNanos.TraderSabotageBuffs) if (target.Buffs.Select(c => c.Id).Any(c => c == id)) { return true; }
            }

            return false;
        }

        public static bool SelfExceptions(Spell spell, NanoLine nanoline)
        {
            if (RelevantGenericNanos.HpBuffs.Contains(spell.Id) && DynelManager.LocalPlayer.Buffs.Contains(NanoLine.DoctorHPBuffs)) { return true; }

            if (spell.Id == RelevantGenericNanos.TeamDeathlessBlessing && DynelManager.LocalPlayer.Buffs.Contains(RelevantGenericNanos.IndividualShortHoTs))
            { CancelBuffs(RelevantGenericNanos.IndividualShortHoTs); }

            if (nanoline == NanoLine.GrenadeBuffs && DynelManager.LocalPlayer.Buffs.Contains(269482)) { return true; }

            if (nanoline == NanoLine.RifleBuffs)
            {
                if (DynelManager.LocalPlayer.Buffs.Find(RelevantGenericNanos.AssassinsAimedShot, out Buff AAS)
                    || DynelManager.LocalPlayer.Buffs.Find(RelevantGenericNanos.SteadyNerves, out Buff SN)) { return true; }
            }

            return false;
        }

        public static bool CanUseSitKit(out Item _kit)
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(280488)
                || IsCasting()
                || DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment))
            {
                _kit = null;
                return false;
            }

            if (DynelManager.LocalPlayer.Health > 0 && !InCombatTeam()
                && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning)
            {
                if (Inventory.Find(297274, out _kit)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if ((bool)!sitKits?.Any()) { return false; }

                foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel).Take(1))
                {
                    //int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

                    //if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
                    //{
                    //    _kit = sitKit;
                    //    return true;
                    //}

                    _kit = sitKit;
                    return true;
                }
            }

            _kit = null;
            return false;
        }

        public static bool SufficiantHealth(SimpleChar target)
        {
            return target != null && target.HealthPercent >= 45 && target.MaxHealth < 999999;
        }

        public static bool IsNanoSkill(int id)
        {
            if (RelevantGenericNanos.MatMetBuffs.Contains(id)
                || RelevantGenericNanos.MatLocBuffs.Contains(id)
                || RelevantGenericNanos.PsyModBuffs.Contains(id)
                || RelevantGenericNanos.SenImpBuffs.Contains(id)
                || RelevantGenericNanos.MatCreBuffs.Contains(id)
                || RelevantGenericNanos.BioMetBuffs.Contains(id)) { return true; }

            return false;
        }

        public static bool PetCheck(SimpleChar target)
        {
            if (target.IsPet && DynelManager.LocalPlayer.Pets.Select(c => c.Identity).Contains(target.Identity))
                return true;

            if (Team.IsInTeam)
                return Team.Members.Select(t => t.Identity.Instance).Contains(target.Identity.Instance);

            return DynelManager.LocalPlayer.Identity == target.Identity;
        }

        public static bool TeamCheck(SimpleChar target)
        {
            if (target.IsPet && DynelManager.LocalPlayer.Pets.Select(c => c.Identity).Contains(target.Identity))
                return true;

            if (Team.IsInTeam)
                return Team.Members.Select(t => t.Identity.Instance).Contains(target.Identity.Instance);

            return DynelManager.LocalPlayer.Identity == target.Identity;
        }

        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }

        public static bool TryGetHealPet(out Pet healPet)
        {
            return (healPet = DynelManager.LocalPlayer.Pets.FirstOrDefault(c => c.Type == PetType.Heal)) != null;
        }

        public static bool TryGetHinderedPet(out Pet hinderedPet)
        {
            return (hinderedPet = DynelManager.LocalPlayer.Pets
                .FirstOrDefault(c => c.Character?.Buffs.Contains(NanoLine.Root) == true
                     || c.Character?.Buffs.Contains(NanoLine.AOERoot) == true
                     || c.Character?.Buffs.Contains(NanoLine.Snare) == true
                     || c.Character?.Buffs.Contains(NanoLine.AOESnare) == true
                     || c.Character?.Buffs.Contains(NanoLine.Mezz) == true
                     || c.Character?.Buffs.Contains(NanoLine.AOEMezz) == true)) != null;
        }

        public static bool CanCleanse(SimpleChar member)
        {
            return member.Buffs.Contains(NanoLine.Root)
                     || member.Buffs.Contains(NanoLine.AOERoot)
                     || member.Buffs.Contains(NanoLine.Snare)
                     || member.Buffs.Contains(NanoLine.AOESnare)
                     || member.Buffs.Contains(NanoLine.Mezz)
                     || member.Buffs.Contains(NanoLine.AOEMezz)
                     || member.Buffs.Contains(NanoLine.TraderSkillTransferTargetDebuff_Deprive)
                     || member.Buffs.Contains(NanoLine.TraderSkillTransferTargetDebuff_Ransack)
                     || member.Buffs.Contains(NanoLine.TraderSkillTransferTargetDebuff_Deprive)
                     || member.Buffs.Contains(NanoLine.TraderSkillTransferTargetDebuff_Ransack)
                     || member.Buffs.Contains(NanoLine.AAODebuffs)
                     || member.Buffs.Contains(NanoLine.TraderAAODrain)
                     || member.Buffs.Contains(NanoLine.DOT_LineA)
                     || member.Buffs.Contains(NanoLine.DOT_LineB)
                     || member.Buffs.Contains(NanoLine.DOTNanotechnicianStrainA)
                     || member.Buffs.Contains(NanoLine.DOTAgentStrainA)
                     || member.Buffs.Contains(NanoLine.DOTNanotechnicianStrainB)
                     || member.Buffs.Contains(NanoLine.DOTStrainC)
                     || member.Buffs.Contains(NanoLine.PainLanceDoT)
                     || member.Buffs.Contains(NanoLine.MINIDoT)
                     || member.Buffs.Contains(NanoLine.InitiativeDebuffs);
        }

        public static float PetMaxNanoPool(Pet _healPet)
        {
            if (_healPet.Character.Level == 215)
                return 5803;
            else if (_healPet.Character.Level == 192)
                return 13310;
            else if (_healPet.Character.Level == 169)
                return 11231;
            else if (_healPet.Character.Level == 146)
                return 9153;
            else if (_healPet.Character.Level == 123)
                return 7169;
            else if (_healPet.Character.Level == 99)
                return 5327;
            else if (_healPet.Character.Level == 77)
                return 3807;
            else if (_healPet.Character.Level == 55)
                return 2404;
            else if (_healPet.Character.Level == 33)
                return 1234;
            else if (_healPet.Character.Level == 14)
                return 414;

            return 0;
        }

        public static bool UBTCheck(Spell spell, SimpleChar target)
        {
            if (spell.Name.Contains("Malaise") && target.Buffs.Contains(301844)) { return false; }

            return true;
        }

        public static bool MezzCheck(SimpleChar target)
        {
            if (target.Buffs.Contains(NanoLine.Mezz) || target.Buffs.Contains(NanoLine.AOEMezz))
            {
                if (DynelManager.Characters.Any(c => (c.IsPet || c.IsPlayer) && c.FightingTarget != null && c.FightingTarget?.Identity == target.Identity))
                { return true; }
                else { return false; }
            }

            return true;
        }

        public static bool AttackingTeam(SimpleChar mob)
        {
            if (mob?.FightingTarget == null) { return true; }

            if (mob?.FightingTarget?.Name == "Guardian Spirit of Purification"
                || mob?.FightingTarget?.Name == "Rookie Alien Hunter"
                || mob?.FightingTarget?.Name == "Unicorn Service Tower Alpha"
                || mob?.FightingTarget?.Name == "Unicorn Service Tower Delta"
                || mob?.FightingTarget?.Name == "Unicorn Service Tower Gamma") { return true; }

            if (Team.IsInTeam) { return Team.Members.Select(m => m.Name).Contains(mob.FightingTarget?.Name); }

            return mob.FightingTarget?.Name == DynelManager.LocalPlayer.Name;
        }


        public static bool InCombatTeam()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }

        public static bool InCombatSelf()
        {
            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }

        public static bool IsInsideInnerSanctum()
        {
            return DynelManager.LocalPlayer.Buffs.Any(buff => buff.Id == 206387);
        }

        public static bool CanCast(Spell spell)
        {
            if (InYalm()) { return false; }

            return spell.Cost < DynelManager.LocalPlayer.Nano;
        }

        public static bool HasNCU(Spell spell, SimpleChar target)
        {
            return SettingsController.GetRemainingNCU(target.Identity) > spell.NCU;
        }

        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);

        [Flags]
        public enum CharacterWieldedWeapon
        {
            Fists = 0x0,              // 0x00000000000000000000b Fists / invalid
            MartialArts = 0x01,       // 0x00000000000000000001b martialarts / fists
            Melee = 0x02,             // 0x00000000000000000010b
            Ranged = 0x04,            // 0x00000000000000000100b
            Bow = 0x08,               // 0x00000000000000001000b
            Smg = 0x10,               // 0x00000000000000010000b
            Edged1H = 0x20,           // 0x00000000000000100000b
            Blunt1H = 0x40,           // 0x00000000000001000000b
            Edged2H = 0x80,           // 0x00000000000010000000b
            Blunt2H = 0x100,          // 0x00000000000100000000b
            Piercing = 0x200,         // 0x00000000001000000000b
            Pistol = 0x400,           // 0x00000000010000000000b
            AssaultRifle = 0x800,     // 0x00000000100000000000b
            Rifle = 0x1000,           // 0x00000001000000000000b
            Shotgun = 0x2000,         // 0x00000010000000000000b
            Grenade = 0x8000,         // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
            MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
            RangedEnergy = 0x10000,   // 0x00010000000000000000b
            Grenade2 = 0x20000,       // 0x00100000000000000000b
            HeavyWeapons = 0x40000,   // 0x01000000000000000000b
        }
    }
}
